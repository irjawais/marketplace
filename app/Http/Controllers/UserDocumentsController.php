<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;  
use Response;

class UserDocumentsController extends Controller
{
    public function profileattachment(Request $request)
    {
        $data = $request->all();
        
        $path = public_path() . '/attachments/profile/'.Auth::user()->id.".pdf";
        if(file_exists($path)){
            unlink($path);
        }
            $path = public_path() . '/attachments/profile/'.Auth::user()->id.".xlsx";
        if(file_exists($path)){
            unlink($path);
        }
        $path = public_path() . '/attachments/profile/'.Auth::user()->id.".xls";
        if(file_exists($path)){
            unlink($path);
        }
        $path = public_path() . '/attachments/profile/'.Auth::user()->id.".doc";
        if(file_exists($path)){
            unlink($path);
        }
        $path = public_path() . '/attachments/profile/'.Auth::user()->id.".docx";
        if(file_exists($path)){
            unlink($path);
        }

        if($request->file('documentfile')){
           $file = $request->file('documentfile');
             $filename= $file->getClientOriginalName();
            $file->getClientOriginalExtension();
            $file->getRealPath();
            $file->getSize();
            $file->getMimeType();
            $destinationPath  = public_path() . '/attachments/profile';
        $file = $file->move($destinationPath,Auth::user()->id.'.'.$file->getClientOriginalExtension());
        }
        return redirect()->back(); 
    }
    public function download($id,Request $request){
        
        $path = public_path() . '/attachments/profile/'.$id.".pdf";
        if(file_exists($path)){
            return Response::download($path);
        }
        $path = public_path() . '/attachments/profile/'.$id.".xlsx";
        if(file_exists($path)){
            return Response::download($path);
        }
        $path = public_path() . '/attachments/profile/'.$id.".xls";
        if(file_exists($path)){
            return Response::download($path);
        }
        $path = public_path() . '/attachments/profile/'.$id.".doc";
        if(file_exists($path)){
            return Response::download($path);
        }
        $path = public_path() . '/attachments/profile/'.$id.".docx";
        if(file_exists($path)){
            return Response::download($path);
        }
        
        $request->session()->flash('error', 'You Dont uploaded any file for this broker'); 
        return redirect()->back();
    }
    public function downloadProfileAttachment($id,Request $request){
        $path = public_path() . '/attachments/profile/'.$id.".pdf";
        if(file_exists($path)){
            return Response::download($path);
        }
        $path = public_path() . '/attachments/profile/'.$id.".xlsx";
        if(file_exists($path)){
            return Response::download($path);
        }
        $path = public_path() . '/attachments/profile/'.$id.".xls";
        if(file_exists($path)){
            return Response::download($path);
        }
        $path = public_path() . '/attachments/profile/'.$id.".doc";
        if(file_exists($path)){
            return Response::download($path);
        }
        $path = public_path() . '/attachments/profile/'.$id.".docx";
        if(file_exists($path)){
            return Response::download($path);
        }
        
        return redirect()->back();
    }
}
