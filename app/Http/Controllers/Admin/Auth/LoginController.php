<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin/home';
    protected $guard = 'admin';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
        $this->middleware('admin:admin', ['except' => 'logout'])->except('logout');
        /* if(Auth::guard('admin')->check()){
            return redirect('/admin/home');
        }
        if(!Auth::guard('admin')->check()){
            return redirect('/admin/login');
        } */
    }
    public function showLoginForm()
    {
        
        return view('admin.auth.login');
    }
    
    protected function attemptLogin(Request $request)
    {
       
        return $this->guard('admin:admin')->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }
   /*  protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard('admin:admin')->user())
                ?: redirect()->intended($this->redirectPath());
    } */
    protected function guard() {
        return Auth::guard('admin');
    }
    public function logout(Request $request)
    {
        $this->guard('admin')->logout();

        $request->session()->invalidate();

        return redirect('/admin/login');
    }
    protected function unauthenticated($request, AuthenticationException $exception)
    {   
        return redirect('/admin/login');
    }
    protected function authenticated(Request $request, $user)
    {
        //
        return redirect('/admin/home');
    }
}
