<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use File;
use Redirect;
use Illuminate\Support\Facades\Validator;
use BlueM\Tree;
use Illuminate\Support\Facades\Input;
use App\Model\Post;

use App\Model\Language;
use App\Model\Category;
use App\Model\SubCategory;
use DB;
use App\Model\Proposal;
class PostController extends Controller
{
     public function makePost(){
        
       return view('post.makePost');
    }
    public function submitPost(Request $request){
      $data = $request->all();
     
      $post = new Post;
      $post->typepost = $data['typepost'];
      $post->subject = $data['subject'];
      $post->category_id = $data['category_id'];
        if($data['amount_field'])
        {
            $post->amount = $data['amount_field'];      
        }
      $post->zone = $data['zone'];
      $post->notes = $data['notes'];
      $post->user_id = Auth::user()->id;
      $post->save();   
      return redirect()->route('timeline');
   }
   public function getPostData(Request $request){
    $data = $request->all();
   
    $post = Post::where('id',$data['id'])->where('deleted',0)->first();
    return $post;   
      
  }
  public function timeline($type="all"){
      
    $user  = User::where('id',Auth::user()->id)->first();
        $profile_ratio=0;
        $user =User::where('id', Auth::user()->id)->first();
        if($user->phoneno){
            $profile_ratio+=20;
        }
        if($user->abouttitle){
            $profile_ratio+=20;
        }
        if($user->aboutdescr){
            $profile_ratio+=60;
        }
        if($user->email_verify!=1){
            
            $request->session()->flash('error', 'Your Email is not verified');
            return view('main.emailverification');
        }
        $languages =Language::all();
        $selected_language = DB::table('language_user')
            ->select('users.id','users.name','language_user.*','language.*','language_user.id as my_language_id','language.id as lngid')
            ->join('users', 'users.id', '=', 'language_user.user_id')
            ->join('language', 'language.id', '=', 'language_user.language_id')
            ->where('language_user.user_id', '=', Auth::user()->id)
            ->get();
        $main_categories = Category::all();
       
        if(isset($main_categories[0])){
            $first_category = $main_categories[0]->toArray();
        }else{  
            $first_category = [
                'id'=>0
            ];
        }

        
        if($type == "all"){
        
            $posts = Post::select('post.*','proposal.id as proposal_id')
                ->leftJoin('proposal', 'proposal.post_id', '=', 'post.id')
                ->where('post.user_id',Auth::user()->id)
                ->where('post.deleted',0)
                ->orderBy('post.created_at', 'asc')
                ->take(10)
                ->get();
        }
        else if($type == "sell" || $type == "buy" || $type == "articale") {
            switch ($type) {
                case "buy":
                    $post_type = 1;
                    break;
                case "sell":
                    $post_type =2;
                    break;
                case "articale":
                    $post_type =3;
                    break;
            }
            $posts = Post::select('post.*','proposal.id as proposal_id')
                ->leftJoin('proposal', 'proposal.post_id', '=', 'post.id')
                ->where('post.user_id',Auth::user()->id)
                ->where('post.deleted',0)
                ->where('post.typepost',$post_type)
                ->orderBy('post.created_at', 'asc')
                ->take(10)
                ->get();
        }else{
            abort(404);
        }
        
        return view('timeline.index',compact(
            'user',
            'profile_ratio',
            'languages',
            'selected_language',
            'main_categories',
            'first_category',
            'posts',
            'type'
        ));
    
    }
   public function editPost(Request $request){
      $data = $request->all();
      $post = Post::where('user_id',Auth::user()->id)->where('deleted',0)->where('id',$data['edit_post_id'])->first();
      $post->zone = $data['zone'];
      $post->notes = $data['notes'];
      $post->save();
      return redirect()->back();
   }
   public function userTimeline($id,$type="all",Request $request){
        $data = $request->all();
        $user = User::where('id', $id)->first();
        $profile_ratio=0;
        if($user->phoneno){
            $profile_ratio+=20;
        }
        if($user->abouttitle){
            $profile_ratio+=20;
        }
        if($user->aboutdescr){
            $profile_ratio+=60;
        }

            
        if($type == "all"){
        
            $posts = Post::where('user_id',$id)
                ->where('deleted',0)
                ->orderBy('created_at', 'asc')
                ->take(10)
                ->get();
        }
        else if($type == "sell" || $type == "buy" || $type == "articale") {
            switch ($type) {
                case "buy":
                    $post_type = 1;
                    break;
                case "sell":
                    $post_type =2;
                    break;
                case "articale":
                    $post_type =3;
                    break;
            }
            $posts = Post::where('user_id',$id)
                ->where('deleted',0)
                ->where('typepost',$post_type)
                ->orderBy('created_at', 'asc')
                ->take(10)
                ->get();
        }else{
            abort(404);
        }
        /* $posts = Post::where('user_id',$id)->where('deleted',0)->orderBy('created_at', 'asc')
        ->take(10)
        ->get(); */
        return view('timeline.user_timeline',compact('user','profile_ratio','posts','type'));
   }
   public function deleteRoute($id){
        $posts = Post::where('id',$id)->first();
        $posts->deleted = 1;
        $posts->save();
        return redirect()->back();
   } 
   public function submitProposal(Request $request){
        $data = $request->all();
        $proposal = new Proposal;
        $proposal->post_id = $data['post_id'];
        $proposal->amount = $data['amount'];
        $proposal->note = $data['notes'];
        $proposal->user_id =Auth::user()->id;
        $proposal->save();
        return redirect()->back();
   }   
   public function getProposal(Request $request){
        $data = $request->all();
        $post_id = $data['post_id'];
        $proposal = Proposal::where('post_id',$post_id)->get();
        return $proposal;
   }
   public function processProposal(Request $request){
        $data = $request->all();
        $post_id = $data['proposal_id'];
        $proposal = Proposal::where('id',$post_id)->first();
        if( $proposal->user_id!=Auth::user()->id){
            abort(404);
        }
        $proposal->status = $data['status'];
        $proposal->save();
        return $proposal;
    }
    public function addArcheivePost($id,Request $request){
        $posts = Post::where('id',$id)->first();
        $posts->is_archive = 1;
        $posts->save();
        return redirect()->back();
    }
}
