<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;   
use App\Model\Language;
use App\Model\Category;
use App\Model\SubCategory;
use DB;
use App\Model\Post;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user  = User::where('id',Auth::user()->id)->first();
        $profile_ratio=0;
        $user =User::where('id', Auth::user()->id)->first();
        if($user->phoneno){
            $profile_ratio+=20;
        }
        if($user->abouttitle){
            $profile_ratio+=20;
        }
        if($user->aboutdescr){
            $profile_ratio+=60;
        }
        if($user->email_verify!=1){
            
            $request->session()->flash('error', 'Your Email is not verified');
            return view('main.emailverification');
        }
        $languages =Language::all();
        $selected_language = DB::table('language_user')
            ->select('users.id','users.name','language_user.*','language.*','language_user.id as my_language_id','language.id as lngid')
            ->join('users', 'users.id', '=', 'language_user.user_id')
            ->join('language', 'language.id', '=', 'language_user.language_id')
            ->where('language_user.user_id', '=', Auth::user()->id)
            ->get();
        $main_categories = Category::all();
       
        if(isset($main_categories[0])){
            $first_category = $main_categories[0]->toArray();
        }else{  
            $first_category = [
                'id'=>0
            ];
        }
        $posts = Post::orderBy('created_at', 'asc')
        ->take(10)
        ->get();
        
        return view('main.main',compact(
            'user',
            'profile_ratio',
            'languages',
            'selected_language',
            'main_categories',
            'first_category',
            'posts'
        ));
    }
}
