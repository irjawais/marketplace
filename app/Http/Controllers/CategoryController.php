<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use File;
use Redirect;
use App\Model\Contact;
use App\Model\Blocked;
use App\Model\Note;
use App\Model\Language;
use App\Model\Category;
use App\Model\SubCategory;
use App\Model\Language_User;
use App\Model\Question;
use DB;
use App\Model\policy;
use Illuminate\Support\Facades\Validator;
use BlueM\Tree;
class CategoryController extends Controller
{
     public function ajaxAddCategory(Request $request){
        $data = $request->all();
        $category = Category::where('name','=',strtolower($data['category_name']))->first();
        if($category){
            return "already_created";
        }
        if ($request->hasFile('category_image')) {
           
         }else{
            return "upload_image";
         }
        $category = new Category;
        $category->name = strtolower($data['category_name']);
        $category->parent = strtolower($data['parent']);
        $category->save();
        if ($request->hasFile('category_image')) {
            if($request->file('category_image')->isValid()) {
                    $name = "category_" .$category->id. '.png';
                    $path = public_path() . '/category/main_category/';
                    $usersImage = public_path("/category/main_category/{$name}");
                    if (File::exists($usersImage)) { // unlink or remove previous image from folder
                        unlink($usersImage);
                    }
                    $request->file('category_image')->move($path, $name);
             } 
         }
        $data['id']=$category->id;
        return $data;
    }
    /*
    public function ajaxSubAddCategory(Request $request){
        $data = $request->all();
        
        $category_id = SubCategory::where('id',$data['category_id'])->first();
        $category = SubCategory::where('name','=',strtolower($data['category_name']))->where('category_id',$category_id->category_id)->first();
        if($category){
            return "already_created";
        }
        
        
        $category = new SubCategory;
        $category->user_id = Auth::user()->id;
        $category->category_id = $category_id->category_id; 
        $category->name = strtolower($data['category_name']);
        $category->save();
        if ($request->hasFile('category_image')) {
            if($request->file('category_image')->isValid()) {
                    $name = "sub_category_" .$category->id. '.png';
                    $path = public_path() . '/category/sub_category/';
                    $usersImage = public_path("/category/sub_category/{$name}");
                    if (File::exists($usersImage)) { // unlink or remove previous image from folder
                        unlink($usersImage);
                    }
                    $request->file('category_image')->move($path, $name);
             } 
         }
        $data['id']=$category->id;
        return $data;
   }
   public function ajaxSubAddCategoryIfEmpty(Request $request){
    $data = $request->all();
    
    $category = SubCategory::where('name','=',strtolower($data['category_name']))->where('category_id',$data['category_id'])->first();
    if($category){
        return "already_created";
    }
    
    
    $category = new SubCategory;
    $category->user_id = Auth::user()->id;
    $category->category_id = $data['category_id']; 
    $category->name = strtolower($data['category_name']);
    $category->save();
    if ($request->hasFile('category_image')) {
        if($request->file('category_image')->isValid()) {
                $name = "sub_category_" .$category->id. '.png';
                $path = public_path() . '/category/sub_category/';
                $usersImage = public_path("/category/sub_category/{$name}");
                if (File::exists($usersImage)) { // unlink or remove previous image from folder
                    unlink($usersImage);
                }
                $request->file('category_image')->move($path, $name);
         } 
     }
    $data['id']=$category->id;
    return $data;
    } */
   public function ajaxGetCategory(Request $request){
    $data = $request->all();
     $main_categories = Category::select('id','name','parent')
     ->where('parent',$data['parent_id'])->get();
        foreach($main_categories as $main_category){
            $question = Question::where('category_id',$main_category->id)->first();
            $main_category->question = $question['id'];
        }
     return $main_categories;
   }
   public function ajaxSubCategoryEdit(Request $request){
    $data = $request->all();
    $category = Category::where('id',$data['category_id'])->first();
    if($category){
        $category->name = strtolower($data['category_name']);
        $category->save();
    }
    if ($request->hasFile('category_image')) {
        if($request->file('category_image')->isValid()) {
                $name = "sub_category_" .$category->id. '.png';
                $path = public_path() . '/category/sub_category/';
                $usersImage = public_path("/category/sub_category/{$name}");
                if (File::exists($usersImage)) { // unlink or remove previous image from folder
                    unlink($usersImage);
                }
                $request->file('category_image')->move($path, $name);
         } 
     }
    $data['id']=$category->id;
    return $data;
} 
   public function ajaxSubCategoryDelete(Request $request){
    $data = $request->all();
    $main_categories = Category::where('parent',$data['id'])->first();
    if($main_categories){
       return "can_not_delete"; 
    }
    $main_categories = Category::where('id',$data['id'])->delete();
    return "done";
    }
    public function ajaxMoveCategory(Request $request){
        $data = $request->all();
        
        $moveable_category = Category::where('id',$data['target'])->first();
        if($data['target']==$data['dragable']){
            $moveable_category->parent = 0;
            $moveable_category->save();
            return "ok";
        }
        if($moveable_category->parent==$data['dragable']){
            $moveable_category->parent = 0;
            $moveable_category->save();
            return "ok";
        }
        //$parent_id = $moveable_category->parent;
        //$main_categories = Category::where('id',$parent_id)->first();
        $moveable_category->parent = $data['dragable'];

        $moveable_category->save();
       return 'ok';
        }
    
   function buildTree($flat, $pidKey, $idKey = null)
   {
       $grouped = array();
       foreach ($flat as $sub){
           $grouped[$sub[$pidKey]][] = $sub;
       }

       $fnBuilder = function($siblings) use (&$fnBuilder, $grouped, $idKey) {
           foreach ($siblings as $k => $sibling) {
               $id = $sibling[$idKey];
               if(isset($grouped[$id])) {
                   $sibling['children'] = $fnBuilder($grouped[$id]);
               }
               $siblings[$k] = $sibling;
           }

           return $siblings;
       };

       $tree = $fnBuilder($grouped[0]);

       return $tree;
       }
}
