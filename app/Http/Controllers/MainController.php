<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use File;
use Redirect;
use App\Model\Contact;
use App\Model\Blocked;
use App\Model\Note;
use App\Model\Language;
use App\Model\Language_User;
use DB;
use App\Model\policy;
use Illuminate\Support\Facades\Validator;
use App\Model\Post;
class MainController extends Controller
{
    
    
    public function main()
    {
        $profile_ratio=0;
        if(Auth::user()){
            $user =User::where('id', Auth::user()->id)->first();
        if($user->phoneno){
            $profile_ratio+=20;
        }
        if($user->abouttitle){
            $profile_ratio+=20;
        }
        if($user->aboutdescr){
            $profile_ratio+=60;
        }
        }
        $posts = Post::select('users.name','post.*')
            ->where('deleted',0)->orderBy('created_at', 'asc')
            ->join('users', 'users.id', '=', 'post.user_id')
            ->take(10)
            ->get();
        
        return view('main.home',compact('user','profile_ratio','languages','posts'));
    }
    public function emailverification()
    {
        return view('main.emailverification');
    }
    public function changestatustofree(Request $request)
    {
       $data = $request->all();
        
        $user =User::where('id', Auth::user()->id)->first();
        $user->visiblity=$data['status'];
        $user->save();
        return "ok";
    }
    public function emailverificationpost(Request $request)
    {
       $data = $request->all();
       $user =User::where('id', Auth::user()->id)->first();
        if($user->email_code==$data['email_code']){
            $user->email_verify=1;
            $user->save();
            return redirect("/home");
        }
        else 
        {
            $request->session()->flash('error', 'Wrong Verificaton Code'); 
            return view('main.emailverification');
        }
             
    }
    public function updateprofile(Request $request)
    {
        $user =User::where('id', Auth::user()->id)->first();
        return view('main.updateprofile',compact('user'));
       /*  $data = $request->all();
        $user =User::where('id', Auth::user()->id)->first();
        $user->username=$data['username'];
        $user->address=$data['address'];
        $user->phoneno=$data['phoneno'];
        $user->save();
        $request->session()->flash('msg', 'Profile Updated Successfully'); 
        return redirect("/home"); */
    }
    public function updateprofilePost(Request $request)
    {
        
        $data = $request->all();
        $user =User::where('id', Auth::user()->id)->first();
        $user->username=$data['username'];
        $user->address=$data['address'];
        $user->phoneno=$data['phoneno'];
        $user->abouttitle=$data['abouttitle'];
        $user->aboutdescr=$data['aboutdescr'];
        $user->save();
        $request->session()->flash('msg', 'Profile Updated Successfully'); 
        return redirect("/home");
    }
    
    public function changeprofile(Request $request){
        $user =User::where('id', Auth::user()->id)->first();
        if ($request->hasFile('profileimg')) {
            if($request->file('profileimg')->isValid()) {
                
                
                     $file = $request->file('profileimg');
                     $name = "img_" .$user->id. '.' . $file->getClientOriginalExtension();
                     $path = public_path() . '/profileimages/images';
                     $usersImage = public_path("/profileimages/images/{$name}");
                     if (File::exists($usersImage)) { // unlink or remove previous image from folder
                        unlink($usersImage);
                    }
                    $name = "img_" .$user->id. '.jpg';
                    $usersImage = public_path("/profileimages/images/{$name}");
                    if (File::exists($usersImage)) { // unlink or remove previous image from folder
                        unlink($usersImage);
                    }
                    $name = "img_" .$user->id. '.gif';
                    $usersImage = public_path("/profileimages/images/{$name}");
                    if (File::exists($usersImage)) { // unlink or remove previous image from folder
                        unlink($usersImage);
                    }
                    $name = "img_" .$user->id. '.jpeg';
                    $usersImage = public_path("/profileimages/images/{$name}");
                    if (File::exists($usersImage)) { // unlink or remove previous image from folder
                        unlink($usersImage);
                    }
                    $name = "img_" .$user->id. '.png';
                    $usersImage = public_path("/profileimages/images/{$name}");
                    if (File::exists($usersImage)) { // unlink or remove previous image from folder
                        unlink($usersImage);
                    }
                     $request->file('profileimg')->move($path, $name);
             } 
         }
        return redirect("/home");
    }
    public function changecover(Request $request){
        $user =User::where('id', Auth::user()->id)->first();
        if ($request->hasFile('profileimg')) {
            if($request->file('profileimg')->isValid()) {
                
                
                     $file = $request->file('profileimg');
                     $name = "img_" .$user->id. '.' . $file->getClientOriginalExtension();
                     $path = public_path() . '/profileimages/cover';
                     $usersImage = public_path("/profileimages/cover/{$name}");
                     if (File::exists($usersImage)) { // unlink or remove previous image from folder
                        unlink($usersImage);
                    }
                    $name = "img_" .$user->id. '.jpg';
                    $usersImage = public_path("/profileimages/cover/{$name}");
                    if (File::exists($usersImage)) { // unlink or remove previous image from folder
                        unlink($usersImage);
                    }
                    $name = "img_" .$user->id. '.png';
                    $usersImage = public_path("/profileimages/cover/{$name}");
                    if (File::exists($usersImage)) { // unlink or remove previous image from folder
                        unlink($usersImage);
                    }
                    $name = "img_" .$user->id. '.gif';
                    $usersImage = public_path("/profileimages/cover/{$name}");
                    if (File::exists($usersImage)) { // unlink or remove previous image from folder
                        unlink($usersImage);
                    }
                    $name = "img_" .$user->id. '.jpeg';
                    $usersImage = public_path("/profileimages/cover/{$name}");
                    if (File::exists($usersImage)) { // unlink or remove previous image from folder
                        unlink($usersImage);
                    }
                     $request->file('profileimg')->move($path, $name);
             } 
         }
        return redirect("/home");
    }
    public function searching(Request $request){
        $data = $request->all();
        $user = User::where('id','!=',Auth::user()->id)->where('name', 'LIKE', '%'.$data['data'].'%')->get();
        return $user;
    }
    public function ajaxUpdateHoursRate(Request $request){
        $data = $request->all();
        $user = User::where('id','=',Auth::user()->id)->first();
        $user->hr = $data['hr'];
        $user = $user->save();
        return $data['hr'];
    }
    public function ajaxUpdatelanguage(Request $request){
        $data = $request->all();
        $user = User::where('id','=',Auth::user()->id)->first();
        $user->language = $data['language'];
        $user = $user->save();
        return $data['language'];
    }
    public function ajaxUpdateWebsite(Request $request){
        $data = $request->all();
        $user = User::where('id','=',Auth::user()->id)->first();
        $user->website = $data['website'];
        $user = $user->save();
        return $data['website'];
    }
    public function ajaxUpdateParagraphTitle(Request $request){
        $data = $request->all();
        $user = User::where('id','=',Auth::user()->id)->first();
        $user->paragraph_title = $data['paragraph_title'];
        $user = $user->save();
        return $data['paragraph_title'];
    }
    public function ajaxUpdateParagraph(Request $request){
        $data = $request->all();
        $user = User::where('id','=',Auth::user()->id)->first();
        $user->paragraph = $data['paragraph_title'];
        $user = $user->save();
        return $data['paragraph_title'];
    }
    public function ajaxAddlanguage(Request $request){
        $data = $request->all();
        $language = Language::where('language','=',strtolower($data['language']))->first();
        if($language){
            return "already_created";
        }
        $language = new Language;
        $language->user_id = Auth::user()->id;
        $language->language = $data['language'];
        $language->save();
        
        $data['id']=$language->id;
        return $data;
    }
    public function ajaxRemovelanguage(Request $request){
        $data = $request->all();
        $language = Language::where('id','=',$data['id'])->first();
        
        $language = $language->delete();
        return "deleted";
    }
    public function ajaxSaveUserlanguage(Request $request){
        $data = $request->all();
        $lng = Language_User::where('user_id', Auth::user()->id)->first();
        if($lng){
            Language_User::where('user_id', Auth::user()->id)->delete();
        }
        if(isset($data['id'])){
        foreach ($data['id'] as $key => $value) {
            $user = Language_User::where('language_id', $value)->where('user_id', Auth::user()->id)->first();
            if($user)
                continue;
            $language_user = new Language_User;
            $language_user->user_id = Auth::user()->id;
            $language_user->language_id = $value;
            $language_user->save();
        }
        }
        return "ok";
    }
    public function ajaxGetUserlanguage(Request $request){
        $language = DB::table('language_user')
            ->select('users.id','users.name','language_user.*','language.*','language_user.id as my_language_id')
            ->join('users', 'users.id', '=', 'language_user.user_id')
            ->join('language', 'language.id', '=', 'language_user.language_id')
            ->where('language_user.user_id', '=', Auth::user()->id)
            ->get();
        return $language;
    }
    
    public function profile($id,Request $request){
        $data = $request->all();
        $user = User::where('id', $id)->first();
        $profile_ratio=0;
        if($user->phoneno){
            $profile_ratio+=20;
        }
        if($user->abouttitle){
            $profile_ratio+=20;
        }
        if($user->aboutdescr){
            $profile_ratio+=60;
        }
        return view('profile.profile',compact('user','profile_ratio'));
    }
    public function addcontact($id){
         $user = Contact::where('my_id', Auth::user()->id)->where('my_contact_id', $id)->first();
         if($user){
            return Redirect::back();
         }
         else {
            $contact = new Contact;
            $contact->my_id = Auth::user()->id;
            $contact->my_contact_id = $id;
            $contact->save();
         }
         return Redirect::back();
    }
    public function getcontactlist(Request $request)
    {
        $data = $request->all();
        $users = Contact::join('users', function ($join) {
            $join->on('users.id', '=', 'contact.my_contact_id' );
        })->where('my_id',Auth::User()->id)->get();

        return $users;
    }
    public function getblockcontactlist(Request $request)
    {
        $data = $request->all();
        $users = Blocked::join('users', function ($join) {
            $join->on('users.id', '=', 'blocked.my_contact_id' );
        })->where('my_id',Auth::User()->id)->get();

        return $users;
    }
    
    public function removecontact($id){
        $user = Contact::where('my_id', Auth::user()->id)->where('my_contact_id', $id)->first();
        $user->delete();
        return Redirect::back();
   }
   public function addnote(Request $request){
    $data = $request->all();
    $data['notes'];
    $user = Contact::where('my_id', Auth::user()->id)->where('my_contact_id', $data['id'])->first();
    if($user){
        $note->note=$data['notes'];
        $note->save();
        return Redirect::back();
    }
    $note = new Note;

    $note->my_id=Auth::user()->id;
    $note->my_contact_id=$data['id'];
    $note->note=$data['notes'];
    $note->save();
    return Redirect::back();
    }
    public function updatepolicy(Request $request){
        $data = $request->all();
        
        $user = User::where('id', Auth::user()->id)->first();
        $user->note=$data['notes'];
        $user->save();
        return Redirect::back();
        }
    
   public function blockcontact($id)
    {
        $user = Blocked::where('my_id', Auth::user()->id)->where('my_contact_id', $id)->first();
         if($user){
            return Redirect::back();
         }
         else {

            $contact = new Blocked;
            $contact->my_id = Auth::user()->id;
            $contact->my_contact_id = $id;
            $contact->save();
         }
         return Redirect::back();
    }
    public function unblockcontact($id){
        $user = Blocked::where('my_id', Auth::user()->id)->where('my_contact_id', $id)->first();
        if($user){
            $user->delete();
         }
        
        return Redirect::back();
   }
   public function uploadvideo(Request $request){
    $data = $request->all();
   
    if($data['videocheck']=="file"){
       
       
        if($request->file('videofile')){
                $file = $request->file('videofile');
                 $destinationPath  = public_path() . '/video/';
                 $file = $file->move($destinationPath,Auth::user()->id.'.'.$file->getClientOriginalExtension());
                 $user = User::where('id', Auth::user()->id)->first();
                 $user->video="file";
                 $user->save();
               
         }
    }else if($data['videocheck']=="link"){
        $user = User::where('id', Auth::user()->id)->first();
                 $user->video=$data['videolink'];;
                 $user->save();
    }
    return Redirect::back();
    }
    public function loadmap(){
        return view('profile.map');
    }
    public function savelatlng(Request $request){
        $data = $request->all();
        $user = User::where('id', Auth::user()->id)->first();
                 $user->lat = $data['lat'];
                 $user->lng = $data['lng'];
                 $user->save();
        return "ok";
    }
    public function getlatlng(Request $request){
        $data = $request->all();
        $user = User::where('id', Auth::user()->id)->first();
                 $user->lat;
                 $user->lng;
        $latlng = array(
            'lat'=>$user->lat,
            'lng'=>$user->lng
        );
        return $latlng;
    }
    public function setloginlocation(Request $request){
        $data = $request->all();
        $user = User::where('id', Auth::user()->id)->first();
                 $user->live_lat = $data['lat'];
                 $user->live_lng = $data['lng'];
                 $user->save();
        return "ok";
    }
    
}
