<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use File;
use Redirect;
use App\Model\Contact;
use App\Model\Blocked;
use App\Model\Note;
use App\Model\Language;
use App\Model\Category;
use App\Model\SubCategory;
use App\Model\Language_User;
use App\Model\Question;
use App\Model\Exam;
use App\Model\QuizResult;
use DB;
use App\Model\policy;
use Illuminate\Support\Facades\Validator;
use BlueM\Tree;
use Illuminate\Support\Facades\Input;
class TrainingController extends Controller
{
     public function addTraining(){
        $user  = User::where('id',Auth::user()->id)->first();
        $profile_ratio=0;
        $user =User::where('id', Auth::user()->id)->first();
        if($user->phoneno){
            $profile_ratio+=20;
        }
        if($user->abouttitle){
            $profile_ratio+=20;
        }
        if($user->aboutdescr){
            $profile_ratio+=60;
        }
        if($user->email_verify!=1){
            
            $request->session()->flash('error', 'Your Email is not verified');
            return view('main.emailverification');
        }
        $languages =Language::all();
        $selected_language = DB::table('language_user')
            ->select('users.id','users.name','language_user.*','language.*','language_user.id as my_language_id','language.id as lngid')
            ->join('users', 'users.id', '=', 'language_user.user_id')
            ->join('language', 'language.id', '=', 'language_user.language_id')
            ->where('language_user.user_id', '=', Auth::user()->id)
            ->get();
        $main_categories = Category::all();
       
        return view('training.addTraining',compact(
            'user',
            'profile_ratio',
            'languages',
            'selected_language',
            'main_categories'
        ));
    }
        public function addTrainings($id,$question_id,Request $request){
            
            $user  = User::where('id',Auth::user()->id)->first();
            $profile_ratio=0;
            $user =User::where('id', Auth::user()->id)->first();
            if($user->phoneno){
                $profile_ratio+=20;
            }
            if($user->abouttitle){
                $profile_ratio+=20;
            }
            if($user->aboutdescr){
                $profile_ratio+=60;
            }
            if($user->email_verify!=1){
                
                $request->session()->flash('error', 'Your Email is not verified');
                return view('main.emailverification');
            }
            $languages =Language::all();
            $selected_language = DB::table('language_user')
                ->select('users.id','users.name','language_user.*','language.*','language_user.id as my_language_id','language.id as lngid')
                ->join('users', 'users.id', '=', 'language_user.user_id')
                ->join('language', 'language.id', '=', 'language_user.language_id')
                ->where('language_user.user_id', '=', Auth::user()->id)
                ->get();
            $main_categories = Category::all();
            $category = Category::where('id',$id)->first();
            $question = Question::where('category_id',$id)->where('id',$question_id)->first();
            $ques = Question::select("questions.id")->where('category_id',$id)->get();
            $question_count = count($ques);
            foreach($ques as $key=> $que){
                if($que->id==$question_id){
                    $position  =  $key+1;
                    $location  =  $key;
                }
            }
            
            
            if(isset($position)){
                $percentage = $position."0";
            }
            if(isset($location)){
                
                if(isset($ques[$location-1])){
                    $previous = $ques[$location-1]->toArray();
                }else{
                    $previous = $ques[0]->toArray();
                    
                }
                
                if(isset($ques[$location+1])){
                    $next = $ques[$location+1]->toArray();
                }else{
                    $next = $ques[$location]->toArray();
                    
                    $next['id'] = $next['id']+1;
                    
                }
            }else{
                if(isset($ques[sizeof($ques) - 1]))
                    $previous = $ques[sizeof($ques) - 1]->toArray();
                else{
                    $previous = [
                        'id'=>0
                    ];
                    $next = [
                        'id'=>0
                    ];
                }
                    
            }
            if(isset($ques[sizeof($ques) - 1])){
                $last = $ques[sizeof($ques) - 1]->toArray();
                $last['id'] = $last['id']+1;
            }
            if(isset($ques[0])){
                $start = $ques[0]->toArray();
            }else{
                $start = [
                    'id'=>0
                ];
            }
                   
            
            return view('training.addTrainings',compact(
                'user',
                'profile_ratio',
                'languages',
                'selected_language',
                'main_categories',
                'category',
                'question',
                'position',
                'percentage',
                'previous',
                'next',
                'last',
                'start',
                'question_count'
            ));
        }
        public function addTrainingPost(Request $request){
            
            $data = $request->all();
            $parsed_url = parse_url($data['youtube_url']);
            
            if( isset($parsed_url['query']) ){
                parse_str($parsed_url['query'],$query_params);
                if( isset($query_params['v']) ){
                    $data['youtube_url'] = $query_params['v'];
                }
            }
            $question = Question::create($data);
            return \Redirect::route('addTrainingPage', [$data['category_id'],$question->id]);
        }
        public function updateTrainingPost(Request $request){
            
            $data = $request->all();
            
            $parsed_url = parse_url($data['youtube_url']);
            if( isset($parsed_url['query']) ){
                parse_str($parsed_url['query'],$query_params);
                if( isset($query_params['v']) ){
                    $data['youtube_url'] = $query_params['v'];
                }
            }

            $question = Question::where('id',$data['question_id'])->first();
            $question->youtube_url = $data['youtube_url'];
            $question->question = $data['question'];
            $question->opt_1 = $data['opt_1'];
            $question->opt_2 = $data['opt_2'];
            $question->opt_3 = $data['opt_3'];
            $question->opt_4 = $data['opt_4'];
            $question->msg_wrong = $data['msg_wrong'];
            $question->msg_correct = $data['msg_correct'];
            $question->answer = $data['answer'];
            $question->save();
            return redirect()->back(); 
        }
        public function deleteTraining($id){
            $question = Question::where('id',$id)->first();
            $question->delete();
            return redirect()->back(); 
        }
        public function quiz_reqult(){
            $profile_ratio=0;
            $user =User::where('id', Auth::user()->id)->first();
            if($user->phoneno){
                $profile_ratio+=20;
            }
            if($user->abouttitle){
                $profile_ratio+=20;
            }
            if($user->aboutdescr){
                $profile_ratio+=60;
            }
            $languages =Language::all();
            $quizresult = QuizResult::select("category.id as cat_id","category.name","quiz_result.*")
                ->where("quiz_result.user_id",Auth::user()->id)
                ->join('category', function ($join) {
                    $join->on('category.id', '=', 'quiz_result.category_id');
                });
            $quizresult = $quizresult->get();
           
            $selected_language = DB::table('language_user')
                ->select('users.id','users.name','language_user.*','language.*','language_user.id as my_language_id','language.id as lngid')
                ->join('users', 'users.id', '=', 'language_user.user_id')
                ->join('language', 'language.id', '=', 'language_user.language_id')
                ->where('language_user.user_id', '=', Auth::user()->id)
                ->get();
            return view("training.quiz_reqult",compact(
                'profile_ratio',
                'languages',
                'selected_language',
                'user',
                'quizresult'
            ));
        }
        public function initExamMode(){
            $profile_ratio=0;
            $user =User::where('id', Auth::user()->id)->first();
            if($user->phoneno){
                $profile_ratio+=20;
            }
            if($user->abouttitle){
                $profile_ratio+=20;
            }
            if($user->aboutdescr){
                $profile_ratio+=60;
            }
            $languages =Language::all();
            $selected_language = DB::table('language_user')
                ->select('users.id','users.name','language_user.*','language.*','language_user.id as my_language_id','language.id as lngid')
                ->join('users', 'users.id', '=', 'language_user.user_id')
                ->join('language', 'language.id', '=', 'language_user.language_id')
                ->where('language_user.user_id', '=', Auth::user()->id)
                ->get();
            return view("training.init_exam_mode",compact(
                'profile_ratio',
                'languages',
                'selected_language',
                'user'
            ));
        }  
        public function questionExamMode($id,Request $request){
            
            $category = Category::where('id',$id)->first();
            
            $question = Question::where('category_id',$id)->get();
            if(isset($question[sizeof($question) - 1])){
                $last = $question[sizeof($question) - 1]->toArray();
                $count = $last['id']+1;
            }
           if(!isset($count)){
               return redirect()->route("quiz");
           }
            while(1){
                $rand_number = rand(1,$count);
                
                $question = Question::where('category_id',$id)->where('id',$rand_number)->first();
                if($question){
                    break;
                }
            }
            $question_id = $question->id;
            //random question end
            $user  = User::where('id',Auth::user()->id)->first();
            $profile_ratio=0;
            $user =User::where('id', Auth::user()->id)->first();
            if($user->phoneno){
                $profile_ratio+=20;
            }
            if($user->abouttitle){
                $profile_ratio+=20;
            }
            if($user->aboutdescr){
                $profile_ratio+=60;
            }
            if($user->email_verify!=1){
                
                $request->session()->flash('error', 'Your Email is not verified');
                return view('main.emailverification');
            }
            $languages =Language::all();
            $selected_language = DB::table('language_user')
                ->select('users.id','users.name','language_user.*','language.*','language_user.id as my_language_id','language.id as lngid')
                ->join('users', 'users.id', '=', 'language_user.user_id')
                ->join('language', 'language.id', '=', 'language_user.language_id')
                ->where('language_user.user_id', '=', Auth::user()->id)
                ->get();
            $main_categories = Category::all();
            //   $category = Category::where('id',$id)->first();
                 $category = Category::select("id")->where('parent',$id)->get();
                 //dd($category->toArray());
                $question = Question::where('category_id',$id)->where('id',$question_id)->first();
            // $ques = Question::select("questions.id")->where('category_id',$id)->get();
            $ques = array();
            $ques = Question::select("questions.id")->whereIn('category_id',$category->toArray())->get();
            $category = Category::where('id',$id)->first();
           
            $question_count = count($ques);
            
            foreach($ques as $key=> $que){
                
                if($que->id==$question_id){
                    $position  =  $key+1;
                    $location  =  $key;
                }
            }
            
            
            if(isset($position)){
                $percentage = $position."0";
            }
            if(isset($location)){
                
                if(isset($ques[$location-1])){
                    $previous = $ques[$location-1]->toArray();
                }else{
                    $previous = $ques[0]->toArray();
                    
                }
                
                if(isset($ques[$location+1])){
                    $next = $ques[$location+1]->toArray();
                }else{
                    $next = $ques[$location]->toArray();
                    
                    $next['id'] = $next['id']+1;
                    
                }
            }else{
                if(isset($ques[sizeof($ques) - 1]))
                    $previous = $ques[sizeof($ques) - 1]->toArray();
                else{
                    $previous = [
                        'id'=>0
                    ];
                    $next = [
                        'id'=>0
                    ];
                }
                    
            }
            if(isset($ques[sizeof($ques) - 1])){
                $last = $ques[sizeof($ques) - 1]->toArray();
                $last['id'] = $last['id']+1;
            }
            if(isset($ques[0])){
                $start = $ques[0]->toArray();
            }else{
                $start = [
                    'id'=>0
                ];
            }
                   
            
            return view('training.exam_mode',compact(
                'user',
                'profile_ratio',
                'languages',
                'selected_language',
                'main_categories',
                'category',
                'question',
                'position',
                'percentage',
                'previous',
                'next',
                'last',
                'start',
                'question_count'
            ));
        }
        public function submitExamAnswer(Request $request){
            
            $data = $request->all();
            $id = $data['category_id'];
            $category = Category::where('id',$id)->first();
            $question = Question::where('category_id',$id)->get();
            $total_question = count($question);
            if(isset($question[sizeof($question) - 1])){
                $last = $question[sizeof($question) - 1]->toArray();
                $count = $last['id']+1;
            }
            
            /* $exams = Exam::where('category_id',$data['category_id'])->where('mode','exam')->where('user_id',Auth::user()->id)->get();
            if($total_question==count($exams)){
                return redirect()->route('examFinished', [$data['category_id']]);
            } */
               
            
            $this_question = Question::where('id',$data['question_id'])->first();
            if($this_question->answer == $data['answer'])
                $question_res = 1;
            else 
                $question_res = 0;
            $exam =new  Exam;
            $exam->question_id = $data['question_id'];
            $exam->category_id = $data['category_id'];
            $exam->user_id = Auth::user()->id;
            $exam->answer = $question_res;
            $exam->mode = "exam";
            $exam->save();

            $exams = Exam::where('category_id',$data['category_id'])->where('mode','exam')->where('user_id',Auth::user()->id)->get();
            //dd($total_question ."-".count($exams));
            if($total_question==count($exams)){
                return redirect()->route('examFinished', [$data['category_id']]);
            } 
            /* $exams = Exam::where('category_id',$data['category_id'])->where('mode','exam')->where('user_id',Auth::user()->id)->get();
            if($total_question==count($exams)){
                $total_marks = 0;
                foreach($exams as $ex){
                    if($ex->answer==1)
                        $total_marks =  $total_marks+1;
                }
                $total_marks = round(($total_marks/count($exams)*100)/20,2);
                $quiz_result = new QuizResult;
                $quiz_result->category_id = $data['category_id'];
                $quiz_result->user_id = Auth::user()->id;
                $quiz_result->score	= $total_marks;
                $quiz_result->time_to_complete = 2;
                $quiz_result->parent = 0;
                $quiz_result->save();
                return redirect()->route('examFinished', [$quiz_result->id]);
            } */
                

            while(1){
                $rand_number = rand(1,$count);
                
                $question = Question::where('category_id',$id)->where('id',$rand_number)->first();
                
                if($question){
                    $exam = Exam::where('question_id',$question->id)->where('mode','exam')->where('user_id',Auth::user()->id)->first();
                   
                    if($exam && count($exam)!=1 && count($exam)!=0){
                     
                    }else{
                        
                        break;
                    }
                }
            }
            
            $question_id = $question->id;

            $user  = User::where('id',Auth::user()->id)->first();
            $profile_ratio=0;
            $user =User::where('id', Auth::user()->id)->first();
            if($user->phoneno){
                $profile_ratio+=20;
            }
            if($user->abouttitle){
                $profile_ratio+=20;
            }
            if($user->aboutdescr){
                $profile_ratio+=60;
            }
            if($user->email_verify!=1){
                
                $request->session()->flash('error', 'Your Email is not verified');
                return view('main.emailverification');
            }
            $languages =Language::all();
            $selected_language = DB::table('language_user')
                ->select('users.id','users.name','language_user.*','language.*','language_user.id as my_language_id','language.id as lngid')
                ->join('users', 'users.id', '=', 'language_user.user_id')
                ->join('language', 'language.id', '=', 'language_user.language_id')
                ->where('language_user.user_id', '=', Auth::user()->id)
                ->get();
            $main_categories = Category::all();
            $category = Category::where('id',$id)->first();
            $question = Question::where('category_id',$id)->where('id',$question_id)->first();
            $ques = Question::select("questions.id")->where('category_id',$id)->get();
            $question_count = count($ques);
            foreach($ques as $key=> $que){
                if($que->id==$question_id){
                    $position  =  $key+1;
                    $location  =  $key;
                }
            }
            
            
            if(isset($position)){
                $percentage = $position."0";
            }
            if(isset($location)){
                
                if(isset($ques[$location-1])){
                    $previous = $ques[$location-1]->toArray();
                }else{
                    $previous = $ques[0]->toArray();
                    
                }
                
                if(isset($ques[$location+1])){
                    $next = $ques[$location+1]->toArray();
                }else{
                    $next = $ques[$location]->toArray();
                    
                    $next['id'] = $next['id']+1;
                    
                }
            }else{
                if(isset($ques[sizeof($ques) - 1]))
                    $previous = $ques[sizeof($ques) - 1]->toArray();
                else{
                    $previous = [
                        'id'=>0
                    ];
                    $next = [
                        'id'=>0
                    ];
                }
                    
            }
            if(isset($ques[sizeof($ques) - 1])){
                $last = $ques[sizeof($ques) - 1]->toArray();
                $last['id'] = $last['id']+1;
            }
            if(isset($ques[0])){
                $start = $ques[0]->toArray();
            }else{
                $start = [
                    'id'=>0
                ];
            }
                   
            
            return view('training.exam_mode',compact(
                'user',
                'profile_ratio',
                'languages',
                'selected_language',
                'main_categories',
                'category',
                'question',
                'position',
                'percentage',
                'previous',
                'next',
                'last',
                'start',
                'question_count'
            ));
        }
        public function examFinished($id,Request $request){
                $exams = Exam::where('category_id',$id)->where('mode','exam')->where('user_id',Auth::user()->id)->get();
                $total_marks = 0;
                if(count($exams)==0){
                   
                    return redirect()->route("quiz");
                }
                foreach($exams as $ex){
                    if($ex->answer==1)
                        $total_marks =  $total_marks+1;
                }
                $total_marks = round(($total_marks/count($exams)*100)/20,2);
                $quiz_result = new QuizResult;
                $quiz_result->category_id = $id;
                $quiz_result->user_id = Auth::user()->id;
                $quiz_result->score	= $total_marks;
                $quiz_result->time_to_complete = 2;
                $quiz_result->parent = 0;
                $quiz_result->save();
                $exams = Exam::where('category_id',$id)->where('mode','exam')->where('user_id',Auth::user()->id)->delete();
               
            /* $quiz_result = QuizResult::where('id',$id)->first(); */
            $profile_ratio=0;
            $user =User::where('id', Auth::user()->id)->first();
            if($user->phoneno){
                $profile_ratio+=20;
            }
            if($user->abouttitle){
                $profile_ratio+=20;
            }
            if($user->aboutdescr){
                $profile_ratio+=60;
            }
            $languages =Language::all();
            $selected_language = DB::table('language_user')
                ->select('users.id','users.name','language_user.*','language.*','language_user.id as my_language_id','language.id as lngid')
                ->join('users', 'users.id', '=', 'language_user.user_id')
                ->join('language', 'language.id', '=', 'language_user.language_id')
                ->where('language_user.user_id', '=', Auth::user()->id)
                ->get();
            return view("training.scoreExam",compact(
                "profile_ratio",
                "user",
                "selected_language",
                "languages",
                "quiz_result"
            ));
        }  
        
        public function initTrainingMode(){
            $profile_ratio=0;
            $user =User::where('id', Auth::user()->id)->first();
            if($user->phoneno){
                $profile_ratio+=20;
            }
            if($user->abouttitle){
                $profile_ratio+=20;
            }
            if($user->aboutdescr){
                $profile_ratio+=60;
            }
            $languages =Language::all();
            $selected_language = DB::table('language_user')
                ->select('users.id','users.name','language_user.*','language.*','language_user.id as my_language_id','language.id as lngid')
                ->join('users', 'users.id', '=', 'language_user.user_id')
                ->join('language', 'language.id', '=', 'language_user.language_id')
                ->where('language_user.user_id', '=', Auth::user()->id)
                ->get();
            return view("training.init_training_mode",compact(
                'profile_ratio',
                'languages',
                'selected_language',
                'user'
            ));
        }
        public function questionTrainingMode($id,Request $request){
            
            $category = Category::where('id',$id)->first();
            $question = Question::where('category_id',$id)->get();
            if(isset($question[sizeof($question) - 1])){
                $last = $question[sizeof($question) - 1]->toArray();
                $count = $last['id']+1;
            }
           if(!isset($count)){
               return redirect()->route("quiz");
           }
            while(1){
                $rand_number = rand(1,$count);
                
                $question = Question::where('category_id',$id)->where('id',$rand_number)->first();
                if($question){
                    break;
                }
            }
            $question_id = $question->id;
            //random question end
            $user  = User::where('id',Auth::user()->id)->first();
            $profile_ratio=0;
            $user =User::where('id', Auth::user()->id)->first();
            if($user->phoneno){
                $profile_ratio+=20;
            }
            if($user->abouttitle){
                $profile_ratio+=20;
            }
            if($user->aboutdescr){
                $profile_ratio+=60;
            }
            if($user->email_verify!=1){
                
                $request->session()->flash('error', 'Your Email is not verified');
                return view('main.emailverification');
            }
            $languages =Language::all();
            $selected_language = DB::table('language_user')
                ->select('users.id','users.name','language_user.*','language.*','language_user.id as my_language_id','language.id as lngid')
                ->join('users', 'users.id', '=', 'language_user.user_id')
                ->join('language', 'language.id', '=', 'language_user.language_id')
                ->where('language_user.user_id', '=', Auth::user()->id)
                ->get();
            $main_categories = Category::all();
            $category = Category::where('id',$id)->first();
            $question = Question::where('category_id',$id)->where('id',$question_id)->first();
            $ques = Question::select("questions.id")->where('category_id',$id)->get();
            $question_count = count($ques);
            foreach($ques as $key=> $que){
                if($que->id==$question_id){
                    $position  =  $key+1;
                    $location  =  $key;
                }
            }
            
            
            if(isset($position)){
                $percentage = $position."0";
            }
            if(isset($location)){
                
                if(isset($ques[$location-1])){
                    $previous = $ques[$location-1]->toArray();
                }else{
                    $previous = $ques[0]->toArray();
                    
                }
                
                if(isset($ques[$location+1])){
                    $next = $ques[$location+1]->toArray();
                }else{
                    $next = $ques[$location]->toArray();
                    
                    $next['id'] = $next['id']+1;
                    
                }
            }else{
                if(isset($ques[sizeof($ques) - 1]))
                    $previous = $ques[sizeof($ques) - 1]->toArray();
                else{
                    $previous = [
                        'id'=>0
                    ];
                    $next = [
                        'id'=>0
                    ];
                }
                    
            }
            if(isset($ques[sizeof($ques) - 1])){
                $last = $ques[sizeof($ques) - 1]->toArray();
                $last['id'] = $last['id']+1;
            }
            if(isset($ques[0])){
                $start = $ques[0]->toArray();
            }else{
                $start = [
                    'id'=>0
                ];
            }
                   
            
            return view('training.training_mode',compact(
                'user',
                'profile_ratio',
                'languages',
                'selected_language',
                'main_categories',
                'category',
                'question',
                'position',
                'percentage',
                'previous',
                'next',
                'last',
                'start',
                'question_count'
            ));
        } 
        public function submitTrainingAnswer(Request $request){
            
            $data = $request->all();
            $id = $data['category_id'];
            $category = Category::where('id',$id)->first();
            $question = Question::where('category_id',$id)->get();
            $total_question = count($question);
            if(isset($question[sizeof($question) - 1])){
                $last = $question[sizeof($question) - 1]->toArray();
                $count = $last['id']+1;
            }
            
            /* $exams = Exam::where('category_id',$data['category_id'])->where('mode','exam')->where('user_id',Auth::user()->id)->get();
            if($total_question==count($exams)){
                
                return redirect()->route('trainingFinished', [$data['category_id']]);
            } */
               
            
            $this_question = Question::where('id',$data['question_id'])->first();
            if($this_question->answer == $data['answer'])
                $question_res = 1;
            else 
                $question_res = 0;
            $exam =new  Exam;
            $exam->question_id = $data['question_id'];
            $exam->category_id = $data['category_id'];
            $exam->user_id = Auth::user()->id;
            $exam->answer = $question_res;
            $exam->mode = "training";
            $exam->save();

            $exams = Exam::where('category_id',$data['category_id'])->where('mode','training')->where('user_id',Auth::user()->id)->get();
            if($total_question==count($exams)){
                return redirect()->route('trainingFinished', [$data['category_id']]);
               // return redirect()->route('trainingFinished', [$quiz_result->id]);
            }
                

            while(1){
                $rand_number = rand(1,$count);
                
                $question = Question::where('category_id',$id)->where('id',$rand_number)->first();
                
                if($question){
                    $exam = Exam::where('question_id',$question->id)->where('mode','training')->where('user_id',Auth::user()->id)->first();
                   
                    if($exam && count($exam)!=1 && count($exam)!=0){
                     
                    }else{
                        
                        break;
                    }
                }
            }
            $question_id = $question->id;

            $user  = User::where('id',Auth::user()->id)->first();
            $profile_ratio=0;
            $user =User::where('id', Auth::user()->id)->first();
            if($user->phoneno){
                $profile_ratio+=20;
            }
            if($user->abouttitle){
                $profile_ratio+=20;
            }
            if($user->aboutdescr){
                $profile_ratio+=60;
            }
            if($user->email_verify!=1){
                
                $request->session()->flash('error', 'Your Email is not verified');
                return view('main.emailverification');
            }
            $languages =Language::all();
            $selected_language = DB::table('language_user')
                ->select('users.id','users.name','language_user.*','language.*','language_user.id as my_language_id','language.id as lngid')
                ->join('users', 'users.id', '=', 'language_user.user_id')
                ->join('language', 'language.id', '=', 'language_user.language_id')
                ->where('language_user.user_id', '=', Auth::user()->id)
                ->get();
            $main_categories = Category::all();
            $category = Category::where('id',$id)->first();
            $question = Question::where('category_id',$id)->where('id',$question_id)->first();
            $ques = Question::select("questions.id")->where('category_id',$id)->get();
            $question_count = count($ques);
            foreach($ques as $key=> $que){
                if($que->id==$question_id){
                    $position  =  $key+1;
                    $location  =  $key;
                }
            }
            
            
            if(isset($position)){
                $percentage = $position."0";
            }
            if(isset($location)){
                
                if(isset($ques[$location-1])){
                    $previous = $ques[$location-1]->toArray();
                }else{
                    $previous = $ques[0]->toArray();
                    
                }
                
                if(isset($ques[$location+1])){
                    $next = $ques[$location+1]->toArray();
                }else{
                    $next = $ques[$location]->toArray();
                    
                    $next['id'] = $next['id']+1;
                    
                }
            }else{
                if(isset($ques[sizeof($ques) - 1]))
                    $previous = $ques[sizeof($ques) - 1]->toArray();
                else{
                    $previous = [
                        'id'=>0
                    ];
                    $next = [
                        'id'=>0
                    ];
                }
                    
            }
            if(isset($ques[sizeof($ques) - 1])){
                $last = $ques[sizeof($ques) - 1]->toArray();
                $last['id'] = $last['id']+1;
            }
            if(isset($ques[0])){
                $start = $ques[0]->toArray();
            }else{
                $start = [
                    'id'=>0
                ];
            }
                   
            
            return view('training.training_mode',compact(
                'user',
                'profile_ratio',
                'languages',
                'selected_language',
                'main_categories',
                'category',
                'question',
                'position',
                'percentage',
                'previous',
                'next',
                'last',
                'start',
                'question_count'
            ));
        }
        public function trainingFinished($id,Request $request){
            $exams = Exam::where('category_id',$id)->where('mode','training')->where('user_id',Auth::user()->id)->get();
            $total_marks = 0;
            if(count($exams)==0){
                return redirect()->route("quiz");
            }
            foreach($exams as $ex){
                if($ex->answer==1)
                    $total_marks =  $total_marks+1;
            }
            $total_marks = round(($total_marks/count($exams)*100)/20,2);
            $quiz_result = new QuizResult;
            $quiz_result->category_id = $id;
            $quiz_result->user_id = Auth::user()->id;
            $quiz_result->score	= $total_marks;
            $quiz_result->time_to_complete = 2;
            $quiz_result->parent = 0;
            $quiz_result->save();
            $exams = Exam::where('category_id',$id)->where('mode','training')->where('user_id',Auth::user()->id)->delete();
           
            /* $quiz_result = QuizResult::where('id',$id)->first(); */
            $profile_ratio=0;
            $user =User::where('id', Auth::user()->id)->first();
            if($user->phoneno){
                $profile_ratio+=20;
            }
            if($user->abouttitle){
                $profile_ratio+=20;
            }
            if($user->aboutdescr){
                $profile_ratio+=60;
            }
            $languages =Language::all();
            $selected_language = DB::table('language_user')
                ->select('users.id','users.name','language_user.*','language.*','language_user.id as my_language_id','language.id as lngid')
                ->join('users', 'users.id', '=', 'language_user.user_id')
                ->join('language', 'language.id', '=', 'language_user.language_id')
                ->where('language_user.user_id', '=', Auth::user()->id)
                ->get();
            return view("training.score",compact(
                "profile_ratio",
                "user",
                "selected_language",
                "languages",
                "quiz_result"
            ));
        }
        public function publishExam($id){
            $quizresult = QuizResult::where("id",$id)->first();
            if($quizresult->parent == 0){
                $quizresult->parent = 1;
            }else{
                $quizresult->parent = 0;
            }
            
            $quizresult->save();
            return redirect()->route("quiz");
        } 
        public function quizView($id){
            $profile_ratio=0;
            $user =User::where('id', $id)->first();
            if($user->phoneno){
                $profile_ratio+=20;
            }
            if($user->abouttitle){
                $profile_ratio+=20;
            }
            if($user->aboutdescr){
                $profile_ratio+=60;
            }
            $languages =Language::all();
            $quizresult = QuizResult::select("category.id as cat_id","category.name","quiz_result.*")
                ->where("quiz_result.user_id",$id)
                ->where("quiz_result.parent",1)
                ->join('category', function ($join) {
                    $join->on('category.id', '=', 'quiz_result.category_id');
                });
            $quizresult = $quizresult->get();
           
            $selected_language = DB::table('language_user')
                ->select('users.id','users.name','language_user.*','language.*','language_user.id as my_language_id','language.id as lngid')
                ->join('users', 'users.id', '=', 'language_user.user_id')
                ->join('language', 'language.id', '=', 'language_user.language_id')
                ->where('language_user.user_id', '=', $id)
                ->get();
            return view("training.quiz_reqult_view",compact(
                'profile_ratio',
                'languages',
                'selected_language',
                'user',
                'quizresult'
            ));
        }
        public function showAnswer($id){
            $quizresult = QuizResult::where("id",$id)->first();
            $category_id = $quizresult->category_id;

            $questions = Question::where("category_id",$category_id)->get();
            
            $profile_ratio=0;
            $user =User::where('id', Auth::user()->id)->first();
            if($user->phoneno){
                $profile_ratio+=20;
            }
            if($user->abouttitle){
                $profile_ratio+=20;
            }
            if($user->aboutdescr){
                $profile_ratio+=60;
            }
            $languages =Language::all();
            $selected_language = DB::table('language_user')
                ->select('users.id','users.name','language_user.*','language.*','language_user.id as my_language_id','language.id as lngid')
                ->join('users', 'users.id', '=', 'language_user.user_id')
                ->join('language', 'language.id', '=', 'language_user.language_id')
                ->where('language_user.user_id', '=', Auth::user()->id)
                ->get();
            return view("training.showAnswer",compact(
                "profile_ratio",
                "user",
                "selected_language",
                "languages",
                "questions"
            ));
        }
}
