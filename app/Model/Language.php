<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Language extends Model{
    
    protected $fillable = [
         'language', 'user_id'
    ];
    
    protected $table = 'language';
}



