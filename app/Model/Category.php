<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Category extends Model{
    
    protected $fillable = [
         'name',
         'parent_id'
    ];
    /* public function sub_category(){
        return $this->hasMany('App\Model\SubCategory');
    } */
    protected $table = 'category';
}



