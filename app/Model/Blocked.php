<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Blocked extends Model{
    
    protected $fillable = [
         'my_contact_id', 'my_id'
    ];
    
    protected $table = 'blocked';
}



