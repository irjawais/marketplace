<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Question extends Model{
    
    protected $fillable = [
         'category_id',
         'youtube_url',
         'question',
         'opt_1',
         'opt_2',
         'opt_3',
         'opt_4',
         'msg_wrong',
         'msg_correct',
         'answer'
    ];
    /* public function sub_category(){
        return $this->hasMany('App\Model\SubCategory');
    } */
    protected $table = 'questions';
}



