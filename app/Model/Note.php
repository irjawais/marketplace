<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Note extends Model{
    
    protected $fillable = [
         'my_contact_id', 'my_id','note'
    ];
    
    protected $table = 'note';
}



