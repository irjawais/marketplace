<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Language_User extends Model{
    
    protected $fillable = [
         'language_id', 'user_id'
    ];
    
    protected $table = 'language_user';
}



