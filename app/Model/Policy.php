<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Policy extends Model{
    
    protected $fillable = [
         'my_contact_id', 'my_id','note'
    ];
    
    protected $table = 'policy';
}



