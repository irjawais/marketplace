<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class SubCategory extends Model{
    
    protected $fillable = [
         'name', 'user_id'
    ];
    public function category(){
        return $this->belongsTo('App\Model\Category');
    }
    protected $table = 'sub_category';
}



