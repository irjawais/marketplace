<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */



Auth::routes();
Route::get('/redirect', 'SocialAuthFacebookController@redirect');
Route::get('/callback', 'SocialAuthFacebookController@callback');

Route::get('/', 'MainController@main')->name('main');
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['auth']], function () {
    Route::get('/emailverification', 'MainController@emailverification')->name('emailverification');
    Route::post('/emailverificationpost', 'MainController@emailverificationpost')->name('emailverificationpost');
    Route::post('/updateprofile', 'MainController@updateprofilePost')->name('update.profilePost');
    Route::get('/updateprofile', 'MainController@updateprofile')->name('update.profile');
    Route::post('/changeprofile', 'MainController@changeprofile')->name('changeprofile');
    Route::post('/changecover', 'MainController@changecover')->name('changecover');
    Route::post('/changestatustofree', 'MainController@changestatustofree')->name('changestatustofree');
    Route::post('/searching', 'MainController@searching')->name('searching');
    Route::get('/profile/{id}', 'MainController@profile')->name('profile');
    Route::get('/addcontact/{id}', 'MainController@addcontact')->name('addcontact');
    Route::post('/getcontactlist', 'MainController@getcontactlist')->name('getcontactlist');
    Route::get('/removecontact/{id}', 'MainController@removecontact')->name('removecontact');
    Route::get('/blockcontact/{id}', 'MainController@blockcontact')->name('blockcontact');
    Route::get('/unblockcontact/{id}', 'MainController@unblockcontact')->name('unblockcontact');
    Route::post('/getblockcontactlist', 'MainController@getblockcontactlist')->name('getblockcontactlist');
    Route::post('/addnote', 'MainController@addnote')->name('addnote');
    Route::post('/updatepolicy', 'MainController@updatepolicy')->name('updatepolicy');
    Route::post('/profileattachment', 'UserDocumentsController@profileattachment')->name('profileattachment');
    Route::get('/download/{id}', 'UserDocumentsController@download')->name('download');
    Route::post('/uploadvideo', 'MainController@uploadvideo')->name('upload.video');
    Route::get('/loadmap', 'MainController@loadmap')->name('loadmap');
    Route::post('/savelatlng', 'MainController@savelatlng')->name('savelatlng');
    Route::post('/getlatlng', 'MainController@getlatlng')->name('getlatlng');
    Route::post('/ajax-update-hours-rate', 'MainController@ajaxUpdateHoursRate')->name('ajaxUpdateHoursRate');
    Route::post('/ajax-update-language', 'MainController@ajaxUpdatelanguage')->name('ajaxUpdatelanguage');
    Route::post('/ajax-update-website', 'MainController@ajaxUpdateWebsite')->name('ajaxUpdateWebsite');
    Route::post('/ajax-update-paragraph-title', 'MainController@ajaxUpdateParagraphTitle')->name('ajaxUpdateParagraphTitle');
    Route::post('/ajax-update-paragraph', 'MainController@ajaxUpdateParagraph')->name('ajaxUpdateParagraph');
    Route::get('/setting', 'MainController@setting')->name('setting');
    Route::post('/ajax-add-language', 'MainController@ajaxAddlanguage')->name('ajaxAddlanguage');
    Route::post('/ajax-remove-language', 'MainController@ajaxRemovelanguage')->name('ajaxRemovelanguage');
    Route::post('/ajax-save-user-language', 'MainController@ajaxSaveUserlanguage')->name('ajaxSaveUserlanguage');

    Route::post('/ajax-get-user-language', 'MainController@ajaxGetUserlanguage')->name('ajaxGetUserlanguage');


    Route::post('/setloginlocation', 'MainController@setloginlocation')->name('setloginlocation');
    Route::get('/download-profile-attachment-/{id}', 'UserDocumentsController@downloadProfileAttachment')->name('download.profile.detail.attachments');
    Route::post('/ajax-add-category', 'CategoryController@ajaxAddCategory')->name('ajaxAddCategory');
    Route::post('/ajax-add-category', 'CategoryController@ajaxAddCategory')->name('ajaxAddCategory');
    Route::post('/ajax-add-sub-category', 'CategoryController@ajaxSubAddCategory')->name('ajaxSubAddCategory');
    Route::post('/ajax-get-category', 'CategoryController@ajaxGetCategory')->name('ajaxGetCategory');
    Route::post('/ajax-sub-category-delete', 'CategoryController@ajaxSubCategoryDelete')->name('ajaxSubCategoryDelete');
    Route::post('/ajax-sub-category-edit', 'CategoryController@ajaxSubCategoryEdit')->name('ajaxSubCategoryEdit');
    Route::post('/ajax-add-sub-category-new', 'CategoryController@ajaxSubAddCategoryIfEmpty')->name('ajaxSubAddCategoryIfEmpty');
    Route::post('/ajax-move-category', 'CategoryController@ajaxMoveCategory')->name('ajaxMoveCategory');

    Route::get('/add-training', 'TrainingController@addTraining')->name('addTraining');
    Route::get('/add-training/{id}/{question?}', 'TrainingController@addTrainings')->name('addTrainingPage');
    Route::post('/add-training', 'TrainingController@addTrainingPost')->name('addTrainingPost');
    Route::post('/update-training', 'TrainingController@updateTrainingPost')->name('updateTrainingPost');
    Route::get('/delete-training/{id}', 'TrainingController@deleteTraining')->name('deleteTraining');

    Route::get('/quiz', 'TrainingController@quiz_reqult')->name('quiz');
    Route::get('/exam', 'TrainingController@initExamMode')->name('initExamMode');
    Route::get('/exam/{id}/{question?}', 'TrainingController@questionExamMode')->name('questionExamMode');
    Route::post('/exam-mode/', 'TrainingController@submitExamAnswer')->name('submitExamAnswer');
    Route::get('/exam-finished/{id}', 'TrainingController@examFinished')->name('examFinished');

    Route::get('/training', 'TrainingController@initTrainingMode')->name('initTrainingMode');
    Route::get('/training/{id}/{question?}', 'TrainingController@questionTrainingMode')->name('questionTrainingMode');
    Route::post('/training-mode/', 'TrainingController@submitTrainingAnswer')->name('submitTrainingAnswer');
    Route::get('/training-finished/{id}', 'TrainingController@trainingFinished')->name('trainingFinished');
    Route::get('/publish/{id}', 'TrainingController@publishExam')->name('publishExam');
    

    Route::get('/quiz/{id}', 'TrainingController@quizView')->name('quizView');
    Route::get('/answer/{id}', 'TrainingController@showAnswer')->name('showAnswer');
    
    Route::get('make-post', 'PostController@makePost')->name('makePost');
    Route::post('register-post', 'PostController@submitPost')->name('submitPost');
    

    Route::get('timeline', 'PostController@timeline')->name('timeline');
    Route::get('timeline/sortby/{type?}', 'PostController@timeline')->name('timeline');
    
    Route::post('edit-post', 'PostController@editPost')->name('editPost');
    Route::get('timeline/{id}', 'PostController@userTimeline')->name('userTimeline');
    Route::get('timeline/{id}/sortby/{type?}', 'PostController@userTimeline')->name('userTimeline');
    Route::get('add-archeive-post/{id}', 'PostController@addArcheivePost')->name('addArcheivePost');
    
    Route::get('delete-post/{id}', 'PostController@deleteRoute')->name('deleteRoute');
    Route::post('submit-proposal', 'PostController@submitProposal')->name('submitProposal');
    Route::post('get-proposal', 'PostController@getProposal')->name('getProposal');
    Route::post('process-proposal', 'PostController@processProposal')->name('processProposal');
});


Route::get('logout','Auth\LoginController@logout')->name('logout');
Route::group(['prefix' =>'admin','namespace'=>'Admin','as' => 'admin.'], function () {

    Route::get('/', function () {
        return redirect(route("admin.login"));
    });
    
    //Login Routes...
    Route::get('login','Auth\LoginController@showLoginForm')->name('login');
    Route::post('login','Auth\LoginController@login')->name('login');
    Route::post('logout','Auth\LoginController@logout')->name('logout');

    // Registration Routes...
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register')->name('register');

    Route::group(['middleware' => ['auth:admin']], function () {
        Route::get('/home', 'HomeController@index')->name('home');
    });
});
Route::group(['prefix' =>'agent','namespace'=>'Agent','as' => 'agent.','middleware' => ['agent:agent']], function () {

    Route::get('/', function () {
        return redirect(route("agent.login"));
    });
    //Login Routes...
    Route::get('login','Auth\LoginController@showLoginForm')->name('login');
    Route::post('login','Auth\LoginController@login')->name('login');
    Route::post('logout','Auth\LoginController@logout')->name('logout');

    // Registration Routes...
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register')->name('register');
    Route::group(['middleware' => ['auth:agent']], function () {
        Route::get('/home', 'HomeController@index');
    });
});

Route::post('get-post', 'PostController@getPostData')->name('getPostData');