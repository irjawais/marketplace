var draggedID = null;
var your_helper = null;
$(function() {
    $('#category_list').delegate('li li.ulli', 'mouseover', function() {
        $(this).draggable({
            revert: true,
            revertDuration: 0,

            connectWith: "li.ulli",
            stop: function(event, ui) {
                var id = this.value;
                /* console.log("fire"+this.value)
                console.log("target"+draggedID);
                console.log(ui); */
                /* var _token = $('input[name="_token"]').val();
                $.ajax({
                    url: "{{route('ajaxMoveCategory')}}",
                    type: 'POST',
                    data:{
                        _token:_token,id:id
                    },
                    success: function (data) {
                        load_category(0);
                        }
                        }) */
            }
        });
        //console.log(this.id)
    });
});

function dragdrop(dragable) {
    console.log("new2" + draggedID)
    console.log("new1" + dragable)
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: "{{route('ajaxMoveCategory')}}",
        type: 'POST',
        data: {
            _token: _token,
            target: dragable,
            dragable: draggedID
        },
        success: function(data) {
            load_category(0);
        }
    })
}
$("#category_list").sortable({
    connectWith: ".ulli",


});
$("#category_list").droppable({
    accept: 'li.ulli',
    cursor: "move",
    drop: function(e, ui) {
        var your_draggable_element = $(ui.draggable);
        your_helper = $(ui.helper);
        console.log("fire" + your_helper[0].value);
        dragable = your_helper[0].value;
        console.log("target" + draggedID);

        setTimeout(function() { dragdrop(dragable); }, 500);

    },
    over: function() {
        //console.log(this);
        var drop_id = event.target.id;
        console.log("dropable" + drop_id);
        //console.log("fire"+your_helper[0].value);
        //dragable = your_helper[0].value;
        var _token = $('input[name="_token"]').val();
        /*  $.ajax({
         url: "{{route('ajaxMoveCategory')}}",
         type: 'POST',
          data:{
               _token:_token,target:dragable,dragable
            },
           success: function (data) {
                load_category(0);
            }
            }) */
    }
});
$('body').on('mouseenter', 'li.ulli', function() {
    draggedID = $(this).attr("value");
    // console.log("hoverd"+draggedID);
});
/* $(function(){
    $('#category_list').delegate('li li', 'mouseover', function () {
            $(this).draggable({
                revert: true,
                revertDuration: 0,
                stop: function( event, ui ) {
                   var id =  this.value;
                    console.log(this.value)
                    var id =  this.value;
                    console.log(this.value)
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: "{{route('ajaxMoveCategory')}}",
                        type: 'POST',
                        data:{
                            _token:_token,id:id
                        },
                        success: function (data) {
                            load_category(0);
                            }
                            })
                }
            });
            //console.log(this.id)
    }); 
}); */



jQuery(document).ready(function() {
    jQuery(".chosen").data("placeholder", "Select Frameworks...").chosen();
});

var easyTree;
$(document).ready(function() {
    load_category(0);
    //$('#category_model').modal('show');
});
$('#category_btn').on('click', function() {
    $('#category_model').modal('show'); //
});

function load_category(parent_id) {
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: "{{route('ajaxGetCategory')}}",
        type: 'POST',
        data: {
            _token: _token,
            parent_id: parent_id
        },
        success: function(data) {
            $("#category_list").empty();
            data.forEach(function(v, i, a) {
                $("#category_list").append("<li class='glyphicon glyphicon-chevron-right list-group-item ulli ui-state-default' id='list" + v.id + "' value='" + v.id + "'>" + v.name.toUpperCase() + " <img id='category_icon" + v.id + "' class='imgimg-responsive' style='width:20px;height:20px;' src='' style='float:left;' id='img3' /></li>");

                $.ajax({
                    url: '/category/main_category/category_' + v.id + '.png',
                    type: 'HEAD',
                    success: function() {
                        $('#category_icon' + v.id).attr("src", '/category/sub_category/sub_category_' + v.id + '.png');
                    }
                })

            })


        }
    })
}
var parent_id = 0;
var category_id = 0;

function load_category_after(parent_id) {
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: "{{route('ajaxGetCategory')}}",
        type: 'POST',
        data: {
            _token: _token,
            parent_id: parent_id
        },
        success: function(data) {
            data.forEach(function(v, i, a) {

                $("#list" + parent_id).append("<li class='list-group-item ulli ' id='list" + v.id + "' value='" + v.id + "'>" + v.name.toUpperCase() + " <img id='category_icon" + v.id + "' class='imgimg-responsive' style='width:20px;height:20px;' src='' style='float:left;' id='img3' /></li>");
                $.ajax({
                    url: '/category/main_category/category_' + v.id + '.png',
                    type: 'HEAD',
                    success: function() {
                        $('#category_icon' + v.id).attr("src", '/category/sub_category/sub_category_' + v.id + '.png');
                    }
                })
            })
        }
    })
}
$('body').on('mouseenter', '.ulli', function() {

    //console.log(this.value);
    //$(this) .parent().trigger('mouseleave');
    $(this).prepend('<span style="float:right;"><span class="glyphicon glyphicon-plus add_sub_category_btn"></span><span   class="glyphicon glyphicon-pencil sub_category_control_edit"></span> <span class="glyphicon glyphicon-trash sub_category_control_delete"></span></span>');
});
$('body').on('mouseleave', '.ulli', function() {
    $(this).find("span").remove();
});
$('body').on('click', '.ulli', function(e) {

    if (e.target !== e.currentTarget) return;

    if ($("#list" + this.value).children().length > 2) {
        $("#list" + this.value).children().remove()
        return;
    }
    /* else{
                $("#list"+this.value).children().show()
               
            } */
    load_category_after(this.value);
});

$('#add_category_btn').on('click', function() {
    $('#category_form_model').modal('show');
});
$("#new_category_data").on('submit', (function(e) {
    var formData = new FormData(this)
    var _token = $('input[name="_token"]').val();
    var category_image = $('input[name="category_image"]').val();
    var category_name = $('input[name="category_name"]').val();
    formData.append("_token", _token);
    formData.append("parent", parent_id);


    $.ajax({
        url: "{{route('ajaxAddCategory')}}",
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,

        data: formData,
        success: function(data) {
            if (data == "already_created") {
                swal("Sorry!", "This Category Name already exist!", "error");
            } else {
                swal("Good job!", "Category created successfully!", "success");
                load_category(0);
            }
        }
    });
    e.preventDefault();

}));
$('body').on('click', '.add_sub_category_btn', function() {
    //category_id = this.value;
    parent_id = $(this).parent().parent().attr('value');
    $('#category_form_model').modal('show');
    console.log(parent_id);
});
$('body').on('click', '.sub_category_control_edit', function() {
    category_id = $(this).parent().parent().attr('value');
    $('#sub_category_edit_form_model').modal('show');
    var value = $(this).parent().parent().text(); //
    $('#category_name_field').val(value);
});
$("#edit_sub_category_data").on('submit', (function(e) {
    var formData = new FormData(this)
    formData.append("category_id", category_id);
    var _token = $('input[name="_token"]').val();
    var category_image = $('input[name="category_image"]').val();
    var category_name = $('input[name="category_name"]').val();
    formData.append("_token", _token);

    $.ajax({
        url: "{{route('ajaxSubCategoryEdit')}}",
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        success: function(data) {
            if (data == "already_created") {
                swal("Sorry!", "This Category Name already exist!", "error");
            } else {
                swal("Good Job!", "Sub Category edit successfully!", "success");
                load_category(0);
            }
        }

    });
    e.preventDefault();

}));
$('body').on('click', '.sub_category_control_delete', function() {
    var id = $(this).parent().parent().attr('value');
    swal({
            title: "Are you sure?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {

                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url: "{{route('ajaxSubCategoryDelete')}}",
                    type: 'POST',
                    data: {
                        _token: _token,
                        id: id
                    },
                    success: function(data) {
                        if (data == "can_not_delete") {
                            swal("Sorry!", "Sorry This category can not delete!", "error");
                        } else {
                            swal("Good job!", "Sub Category Deleted successfully!", "success");

                        }
                        load_category(0);
                    }

                });
                swal("Poof! Sub Category has been deleted!", {
                    icon: "success",
                });
            } else {
                swal("Your sub category is safe now!");
            }
        });

});
/* 
var edit_category_value;
 
    
    
    
function load_category(){
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: "{{route('ajaxGetCategory')}}",
        type: 'POST',
        data:{
            _token:_token
        },
        success: function (data) {
                console.log(data);
                $('#category_container').empty();
                $.each(data, function (key, val) {
            $('#category_container').append(`
                <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse`+val['id']+`">
                                `+val['name'][0].toUpperCase() + val['name'].substring(1)+` <img id="category_icon`+val['id']+`" class="imgimg-responsive" style="width:20px;height:20px;" src="" style="float:left;" id="img3" /></a>
                              
                            </h4>
                          </div>
                        <div id="collapse`+val['id']+`" class="panel-collapse collapse ">
                            <div class="panel-body">
                                    <ul  class="list-group" id="category_list`+val['id']+`">
                                    
                                    </ul>
                        </div>
                          </div>
                        </div>
                </div>
                `);
                 $.each(val['sub_category'], function (key, value) {
                    $('#category_list'+val['id']).append(`
                        <li value="`+value['id']+`" class="list-group-item sub_category_control">`+value['name'][0].toUpperCase() + value['name'].substring(1)+`
                           <img id="sub_category_icon`+value['id']+`" class="imgimg-responsive" style="width:20px;height:20px;" src="" style="float:left;" id="img3" />
                        </li>
                    `)
                    $.ajax({
                    url:'/category/sub_category/sub_category_'+value['id']+'.png',
                    type:'HEAD',
                    success: function(){
                        $('#sub_category_icon'+value['id']).attr("src",'/category/sub_category/sub_category_'+value['id']+'.png');
                    }
                    })
                    })
                    if(val['sub_category'].length ==0){
                        $('#category_list'+val['id']).append(`
                        <li value="`+val['id']+`" class="list-group-item">
                                <span  class="glyphicon glyphicon-plus add_sub_category_btn_for_empty"></span>
                        </li>
                    `)
                        
                    }
                
                $.ajax({
                    url:'/category/main_category/category_'+val['id']+'.png',
                    type:'HEAD',
                    success: function(){
                        $('#category_icon'+val['id']).attr("src",'/category/main_category/category_'+val['id']+'.png');
                }
                })
            });
            }
        });
}
$('body').on('mouseenter', '.sub_category_control', function() {
    
    $(this).append('<span style="float:right;"><span class="glyphicon glyphicon-plus add_sub_category_btn"></span><span   class="glyphicon glyphicon-pencil sub_category_control_edit"></span> <span class="glyphicon glyphicon-trash sub_category_control_delete"></span></span>')

});
$('body').on('mouseleave', '.sub_category_control', function() {
    $(this).find("span").remove();
});
    
    
var category_id;
$('body').on('click', '.add_sub_category_btn', function() { 
    //category_id = this.value;
    category_id = $(this).parent().parent().attr('value');
    $('#sub_category_form_model').modal('show');
    console.log(category_id);
});
$('body').on('click', '.add_sub_category_btn_for_empty', function() { 
    //category_id = this.value;
    category_id = $(this).parent().attr('value');
    $('#sub_category_form_model_for_empty').modal('show');
    console.log(category_id);
});
    
    
    
$("#new_sub_category_data").on('submit',(function(e) {
        var formData = new FormData(this)
        formData.append("category_id",category_id);
        var _token = $('input[name="_token"]').val();
        var category_image = $('input[name="category_image"]').val();
        var category_name = $('input[name="category_name"]').val();
    formData.append("_token", _token);
    
        $.ajax({
        url: "{{route('ajaxSubAddCategory')}}",
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        success: function (data) {
            if(data == "already_created"){
                swal("Sorry!", "This Category Name already exist!", "error");   
            }else{ 
            swal("Good job!", "Sub Category added successfully!", "success");
               
                load_category();
            }
            }
           
        });
    e.preventDefault();
 
}));
$("#new_sub_category_data_for_empty").on('submit',(function(e) {
        var formData = new FormData(this)
        formData.append("category_id",category_id);
        var _token = $('input[name="_token"]').val();
        var category_image = $('input[name="category_image"]').val();
        var category_name = $('input[name="category_name"]').val();
    formData.append("_token", _token);
    
        $.ajax({
        url: "{{route('ajaxSubAddCategoryIfEmpty')}}",
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        success: function (data) {
            if(data == "already_created"){
                swal("Sorry!", "This Category Name already exist!", "error");   
            }else{ 
            swal("Good job!", "Sub Category added successfully!", "success");
               
                load_category();
            }
            }
           
        });
    e.preventDefault();
 
}));
    
    */

var loc = window.location.href,
    index = loc.indexOf('#');

if (index > 0) {
    window.location = loc.substring(0, index);
}
$(document).ready(function() {
    ajaxGetUserlanguage()
});

function ajaxGetUserlanguage() {
    $("#languag_list").empty()
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: "{{route('ajaxGetUserlanguage')}}",
        type: 'POST',
        data: {
            _token: _token
        },
        success: function(data) {

            console.log(data)
            data.forEach(function(v, i, a) {
                $("#languag_list").append("<li>" + v.language.toUpperCase() + "</li>");

                // console.log(v, i, a);
            })


        }
    });
}
$('body').on('click', '#save_language', function() {


    var language = $('#save_value_language').val();
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: "{{route('ajaxSaveUserlanguage')}}",
        type: 'POST',
        data: {
            _token: _token,
            id: language
        },
        success: function(data) {
            ajaxGetUserlanguage()
            $('#select_language_model').modal('hide');
        }
    });
});
//$('.child_e').mouseenter(function() {
$('body').on('mouseenter', '.child_e', function() {
    console.log(this.id);
    $(this).append("<span   class='glyphicon glyphicon-trash trash_e'></span>")
});
//$('.child_e').mouseleave(function() {
$('body').on('mouseleave', '.child_e', function() {
    console.log(this.id);
    $(this).find("span").remove();
});
$('body').on('click', '.child_e', function() {
    // $('.child_e').click(function() {
    console.log(this.id);
    var element = this;
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: "{{route('ajaxRemovelanguage')}}",
        type: 'POST',
        data: {
            _token: _token,
            id: this.id
        },
        success: function(data) {
            $(element).remove();
            //$(element).parent().remove();
            localStorage.setItem('popupFlag', 1);
            window.location.reload();
        }
    });
});
/* $(document).ready(function(){
//Get and Check item in localStorage
    if(localStorage.getItem('popupFlag') == 1)
    {
        $('#language_model_box').modal('show');
        localStorage.removeItem('popupFlag')
       
    }
}); */
$(document).ready(function() {
    if (localStorage.getItem('popupFlag') == 1) {
        $('#language_model_box').modal('show'); // Do something after 1 second 
        localStorage.removeItem('popupFlag')
    }
});

$("#language_form").submit(function(e) {
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: "{{route('ajaxAddlanguage')}}",
        type: 'POST',
        data: {
            _token: _token,
            language: $('#settings_language_input').val()
        },
        success: function(data) {

            console.log();
            //if($("#language_cantainer:last-child div:last-child ").children().length<=6){
            if (data != "already_created") {
                console.log($("#language_cantainer:last-child div:last-child div").length)
                if ($("#language_cantainer:last-child div:last-child div").length < 6) {

                    // if($("#language_cantainer:last-child div:last-child ").children().length<=6){
                    $("#language_cantainer:last-child div:last-child div:last-child").after('<div class="col-sm-2 child_e" id=' + data.id + '>' + data.language + '</div>')

                    // }
                } else {
                    $("#language_cantainer:last-child").append('<div class="row"><div class="col-sm-2 child_e" id=' + data.id + '>' + data.language + '</div></div>')
                }
                $('#settings_language_input').empty()
            }

        }
    });
    e.preventDefault();
});
$('#settings').on('click', function() {
    $('#settings_model').modal('show');
});
$('#language_btn').on('click', function() {
    $('#settings_model').modal('hide');

}, function() {

    $('#language_model_box').modal('show');
});

$("#hour_rate_edit_box").hover(function() {
    $('#hour_rate_edit').show();
}, function() {
    $('#hour_rate_edit').hide();
});
$('body').on('click', '#hour_rate_edit', function() {
    //$('#hour_rate_edit').click(function() {
    $("#hour_rate_edit_box").empty();
    $("#hour_rate_edit_box").append(`
            <input type="text" id="input_hr" placeholder="HR-USD">
            <button type="button" id="saave_input_hr" class="btn">save</button>
        `);
    $('#input_hr').focus();
});
$('body').on('click', '#saave_input_hr', function() {
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: "{{route('ajaxUpdateHoursRate')}}",
        type: 'POST',
        data: {
            _token: _token,
            hr: $('#input_hr').val()
        },
        success: function(data) {
            $("#hour_rate_edit_box").empty();
            $("#hour_rate_edit_box").append(`
                    <h5 id="value_hr">$` + data + ` USD/hr <span id="hour_rate_edit" style="display:none" class="glyphicon glyphicon-pencil"></span></h5><h5>Hoursly Rate </h5>
                `);
            console.log(data)
        }
    });
})
$("#language_value_box").hover(function() {
    $('#language_value_icon').show();
}, function() {
    $('#language_value_icon').hide();
});
/*  $('body').on('click', '#language_value_icon', function() {
 ("#language_value_box").empty();
     $("#language_value_box").append(`
     
     <input type="text" id="input_language" placeholder="Language">
         <button type="button" id="saave_input_language" class="btn">save</button>
     `);
     $('#input_language').focus();
 }); */
$('body').on('click', '#language_value_icon', function() {
    $('#select_language_model').modal('show');
});

$('body').on('click', '#saave_input_language', function() {
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: "{{route('ajaxUpdatelanguage')}}",
        type: 'POST',
        data: {
            _token: _token,
            language: $('#input_language').val()
        },
        success: function(data) {
            $("#language_value_box").empty();
            $("#language_value_box").append(`
               
                    Language : <span>` + data + `</span> <span id="language_value_icon" style="display:none" class="glyphicon glyphicon-pencil">
                `);
            console.log(data)
        }
    });
})
$("#website_box").hover(function() {
    $('#website_icon').show();
}, function() {
    $('#website_icon').hide();
});
$('body').on('click', '#website_icon', function() {
    //$('#hour_rate_edit').click(function() {
    $("#website_box").empty();
    $("#website_box").append(`
            <input type="text" id="input_website" placeholder="Website">
            <button type="button" id="saave_input_website" class="btn">save</button>
        `);
    $('#saave_input_website').focus();
});
$('body').on('click', '#saave_input_website', function() {
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: "{{route('ajaxUpdateWebsite')}}",
        type: 'POST',
        data: {
            _token: _token,
            website: $('#input_website').val()
        },
        success: function(data) {
            $("#website_box").empty();
            $("#website_box").append(`
                Website : <a href="https://` + data + `"> ` + data + ` </a><span id="website_icon" style="display:none" class="glyphicon glyphicon-pencil">
                `);
            console.log(data)
        }
    });
})
$("#paragraph_title_box").hover(function() {
    $('#paragraph_title_icon').show();
}, function() {
    $('#paragraph_title_icon').hide();
});
$('body').on('click', '#paragraph_title_icon', function() {
    //$('#hour_rate_edit').click(function() {
    $("#paragraph_title_box").empty();
    $("#paragraph_title_box").append(`
            <input type="text" id="input_input_title" placeholder="Title">
            <button type="button" id="saave_input_btn" class="btn">save</button>
        `);
    $('#input_input_title').focus();
});
$('body').on('click', '#saave_input_btn', function() {
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: "{{route('ajaxUpdateParagraphTitle')}}",
        type: 'POST',
        data: {
            _token: _token,
            paragraph_title: $('#input_input_title').val()
        },
        success: function(data) {
            $("#paragraph_title_box").empty();
            $("#paragraph_title_box").append(`
                    <h3  >` + data + ` <span id="paragraph_title_icon" style="display:none" class="glyphicon glyphicon-pencil"></h3>    
                `);
            console.log(data)
        }
    });
})
$("#paragraph_box").hover(function() {
    $('#paragraph_icon').show();
}, function() {
    $('#paragraph_icon').hide();
});
$('body').on('click', '#paragraph_icon', function() {
    //$('#hour_rate_edit').click(function() {
    $("#paragraph_box").empty();
    $("#paragraph_box").append(`
            <textarea type="text" class="form-control" rows="5" id="input_paragraph" placeholder="Title"></textarea>
            <button type="button" id="input_paragraph_btn" class="btn">save</button>
        `);
    $('#input_paragraph').focus();
});
$('body').on('click', '#input_paragraph_btn', function() {
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url: "{{route('ajaxUpdateParagraph')}}",
            type: 'POST',
            data: {
                _token: _token,
                paragraph_title: $('#input_paragraph').val()
            },
            success: function(data) {
                $("#paragraph_box").empty();
                $("#paragraph_box").append(`
                    <p>` + data + ` <span id="paragraph_icon" style="display:none" class="glyphicon glyphicon-pencil"></span></p>
                `);
                console.log(data)
            }
        });
    })
    //
$("#phone").intlTelInput({
    // allowDropdown: false,
    // autoHideDialCode: false,
    // autoPlaceholder: "off",
    // dropdownContainer: "body",
    // excludeCountries: ["us"],
    // formatOnDisplay: false,
    // geoIpLookup: function(callback) {
    //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
    //     var countryCode = (resp && resp.country) ? resp.country : "";
    //     callback(countryCode);
    //   });
    // },
    // hiddenInput: "full_number",
    // initialCountry: "auto",
    // nationalMode: false,
    // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
    // placeholderNumberType: "MOBILE",
    // preferredCountries: ['cn', 'jp'],
    separateDialCode: true,
    utilsScript: "{{asset('number_masking/js/utils.js')}}"
});

$(document).ready(function() {
    $('#summernote').html('<?php echo $user->note;  ?>');
    $('#summernote').summernote({
        placeholder: 'Type Here',
        tabsize: 4,
        height: 100
    });
    $('#videofile').bind('change', function() {

        if (this.files[0].size > 5120) {

            $("#video_file_error").text("upload only 5 mb file.");
            $("#video_file_error").css("color", "red");
            return false;
        } else {
            $("#video_file_error").css("display", "none");
        }

    });



});

$(function() { // 
    $('#view_video_btn').on('click', function() {
        $('#vide_container').empty();
        $('#vide_container').append(`
                @if($user->video=="file")
                <video style="width:100%" controls>
                <source src='{{asset("video/$user->id.mp4")}}' type="video/mp4">
                <source src='{{asset("video/$user->id.mp4")}}' type="video/ogg">
                Your browser does not support HTML5 video.
                </video>
                @elseif(!$user->video) 
                @else
                <?php 
                $url = $user->video;
                // $url = "http://www.youtube.com/watch?v=7zWKm-LZWm4&feature=relate";
                    parse_str( parse_url( $url, PHP_URL_QUERY ), $url_vars );
                    //echo $url_vars['v'];  
                ?>
                <iframe style="width:100%;height:350px;"
                src="https://www.youtube.com/embed/{{$url_vars['v']}}?autoplay=1">
                </iframe>
                @endif
                `);
    });
    $('#profile').on('click', function() {
        $('#profilechange').modal('show');
    });
    $('#coverphoto').on('click', function() {
        $('#coverphotomodel').modal('show');
    });
    $('#pdf-btn').on('click', function() {
        $('#pdf_model').modal('show');
    });
    $('#profile_status').on('click', function() {
        $('#profile_status_model').modal('show');
    });
    $('#powertable').on('click', function() {
        $('#profile_status_model').modal('hide');
        $('#powertable_model').modal('show');
    });

    $('#video_btn').on('click', function() {
        $('#video_model').modal('show');
    });
    $('#updatepolicy').on('click', function() {
        $('#policy_model').modal('show');
    });
    $('#powertable').on('click', function() {
        $('#profileverification_model').modal('hide');
        $('#powertable_model').modal('show');
    });
    $('#power_table_in_pd').on('click', function() {
        $('#profile_status_model').modal('hide');
        $('#powertable_model').modal('show');
    });
    $('#profileverification').on('click', function() {
        $('#profileverification_model').modal('show');
    });
    $('#edit-page').on('click', function() {
        window.location.href = '{{route("update.profile")}}';
    });
    $('#edit_map').on('click', function() {
        $('#edit_map_model').modal('show');
    });

});
$('#freevisible').on('click', function() {
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: "{{route('changestatustofree')}}",
        type: 'POST',
        data: {
            _token: _token,
            id: '<?php echo Auth::user()->id ?>',
            status: 1
        },
        success: function(data) {
            if (data == "ok") {
                window.location.assign(window.location.href)
            }

        }
    });
});
$('#busyvisible').on('click', function() {
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: "{{route('changestatustofree')}}",
        type: 'POST',
        data: {
            _token: _token,
            id: '<?php echo Auth::user()->id ?>',
            status: 2
        },
        success: function(data) {
            if (data == "ok") {
                window.location.assign(window.location.href)
            }

        }
    });

});
$('#invisiblevisible').on('click', function() {
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: "{{route('changestatustofree')}}",
        type: 'POST',
        data: {
            _token: _token,
            id: '<?php echo Auth::user()->id ?>',
            status: 3
        },
        success: function(data) {
            if (data == "ok") {
                window.location.assign(window.location.href)
            }

        }
    });

});




$(function() {
    $('#profile').on('click', function() {
        $('#profilechange').modal('show');
    });
    $('#coverphoto').on('click', function() {
        $('#coverphotomodel').modal('show');
    });
    $('#edit-page').on('click', function() {
        window.location.href = '{{route("update.profile")}}';
    });
});
$('#freevisible').on('click', function() {
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: '{{route("changestatustofree")}}',
        type: 'POST',
        data: {
            _token: _token,
            id: '<?php echo Auth::user()->id ?>',
            status: 1
        },
        success: function(data) {
            if (data == "ok") {
                window.location.assign(window.location.href)
            }

        }
    });

});
$('#busyvisible').on('click', function() {
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: '{{route("changestatustofree")}}',
        type: 'POST',
        data: {
            _token: _token,
            id: '<?php echo Auth::user()->id ?>',
            status: 2
        },
        success: function(data) {
            if (data == "ok") {
                window.location.assign(window.location.href)
            }

        }
    });
});
$('#invisiblevisible').on('click', function() {
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: '{{route("changestatustofree")}}',
        type: 'POST',
        data: {
            _token: _token,
            id: '<?php echo Auth::user()->id ?>',
            status: 3
        },
        success: function(data) {
            if (data == "ok") {
                window.location.assign(window.location.href)
            }

        }
    });

});

var o = '<?php echo Auth::user()->visiblity ?>';
$(document).ready(function() {
    if (o == 1) {
        $('#status-icon').css('background', 'green')
        $('#dp_status-icon').css('background', 'green')

    }
    if (o == 2) {
        $('#status-icon').css('background', 'red')
        $('#dp_status-icon').css('background', 'red')
    }
    if (o == 3) {
        $('#status-icon').css('display', 'none')
        $('#dp_status-icon').css('background', 'none')
    }

});
$('#menu_link').click(function() {
    $('#menu_box').toggle();
});


$("#profile-edit-icon").hover(function() {
    $('#profile').css('display', 'block')
}, function() {
    $('#profile').css('display', 'none')
})
$("#cover-photo").hover(function() {
    $('#coverphoto').css('display', 'block')
}, function() {

    //  $('#coverphoto').css('display','none')
})
$("#cover-photo").mouseleave(function() {
    // $('#coverphoto').css('display','none')
})



$(document).ready(function() {
    $("#dropdownlist").empty()
    $('#searchbox').keyup(function(e) {
        $("#dropdownlist").empty()
        var _token = $('input[name="_token"]').val();

        input = $(this).val();

        if (input.length >= 2) {
            $.ajax({
                url: '{{route("searching")}}',
                type: 'POST',
                data: {
                    _token: _token,
                    data: input
                },
                success: function(data) {

                    if (data) {

                        data.forEach(function(item) {
                            $("#dropdownlist").css("display", "block");
                            $("#dropdownlist").append('<li class="list-group-item" style="height: 47px !important;"><img class="imgimg-responsive"  style="width:50px;height:30px;float:left" id="searchbox_icon' + item['id'] + '" src="/demo/man01.png"><a style="padding:0;" href="profile/' + item["id"] + '">' + item['name'] + '</a><i   style="margin-right: 12px;float:right;color:green; background: #02ffe1;border: 1px solid rgba(0, 0, 0, .1);border-radius: 50%;box-sizing: border-box;height: 12px;width: 12px;" class="glyphicon glyphicon-football"><sub>.</sub></i></li>');

                            $.ajax({
                                url: 'profileimages/images/img_' + item['id'] + '.png',
                                type: 'HEAD',
                                error: function() {
                                    $('#searchbox_icon' + item['id']).attr("src", "{{asset('/profile/nocover.png')}}");
                                },
                                success: function() {
                                    $('#searchbox_icon' + item['id']).attr("src", '/profileimages/images/img_' + item['id'] + '.png');

                                }
                            })

                        });
                    }
                }
            });
        }


    });
});


$(document).ready(function() {
    $(".left-first-section").click(function() {
        $('.main-section').toggleClass("open-more");
    });
});
$(document).ready(function() {
    $(".fa-minus").click(function() {
        $('.main-section').toggleClass("open-more");
    });
});
$(document).ready(function() {
    //data.namewindow.setInterval(function(){
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: '{{route("getcontactlist")}}',
        type: 'POST',
        data: {
            _token: _token,
            id: '<?php echo Auth::user()->id ?>'
        },
        success: function(data) {
            $("#contact_list").empty();

            data.forEach(function(item) {
                $("#contact_list").append('<li><div class="left-chat"><img class="imgimg-responsive"  id="contactbox_icon' + item.my_contact_id + '" src="/demo/man01.png"><p><a href="/profile/' + item.my_contact_id + '">' + item.name + '</a> <i style="float:right;"><a href="/removecontact/' + item.my_contact_id + '">x</a> </i><i   style="margin-right: 12px;float:right;color:green; background: #02ffe1;border: 1px solid rgba(0, 0, 0, .1);border-radius: 50%;box-sizing: border-box;height: 12px;width: 12px;" class="glyphicon glyphicon-football"><sub>.</sub></i></p></div></li>')

                $.ajax({
                    url: 'profileimages/images/img_' + item.my_contact_id + '.png',
                    type: 'HEAD',
                    error: function() {
                        $('#contactbox_icon' + item.my_contact_id).attr("src", "{{asset('/profile/nocover.png')}}");
                    },
                    success: function() {
                        $('#contactbox_icon' + item.my_contact_id).attr("src", '/profileimages/images/img_' + item.my_contact_id + '.png');

                    }
                })


            });
        }
    });
    // },1000);
});
$(document).ready(function() {
    $("#btn-contacts").click(function() {
        $('#box_lable').text("Contact List")
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url: '{{route("getcontactlist")}}',
            type: 'POST',
            data: {
                _token: _token,
                id: '<?php echo Auth::user()->id ?>'
            },
            success: function(data) {
                $("#contact_list").empty();

                data.forEach(function(item) {
                    $("#contact_list").append('<li><div class="left-chat"><img class="imgimg-responsive"  id="contactbox_icon' + item.my_contact_id + '" src="/demo/man01.png"><p><a href="/profile/' + item.my_contact_id + '">' + item.name + '</a> <i style="float:right;"><a href="/removecontact/' + item.my_contact_id + '">x</a> </i><i   style="margin-right: 12px;float:right;color:green; background: #02ffe1;border: 1px solid rgba(0, 0, 0, .1);border-radius: 50%;box-sizing: border-box;height: 12px;width: 12px;" class="glyphicon glyphicon-football"><sub>.</sub></i></p></div></li>')

                    $.ajax({
                        url: '/profileimages/images/img_' + item.my_contact_id + '.png',
                        type: 'HEAD',
                        error: function() {
                            $('#contactbox_icon' + item.my_contact_id).attr("src", "{{asset('/profile/nocover.png')}}");
                        },
                        success: function() {
                            $('#contactbox_icon' + item.my_contact_id).attr("src", '/profileimages/images/img_' + item.my_contact_id + '.png');

                        }
                    })


                });
            }
        });

    });
});
$("#btn-blocks").click(function() {
    $('#box_lable').text("Block List")
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: '{{route("getblockcontactlist")}}',
        type: 'POST',
        data: {
            _token: _token,
            id: '<?php echo Auth::user()->id ?>'
        },
        success: function(data) {
            $("#contact_list").empty();

            data.forEach(function(item) {
                $("#contact_list").append('<li><div class="left-chat"><img class="imgimg-responsive"  id="contactbox_icon' + item.my_contact_id + '" src="/demo/man01.png"><p><a href="/profile/' + item.my_contact_id + '">' + item.name + '</a> <i style="float:right;"><a href="/removecontact/' + item.my_contact_id + '">x</a> </i><i   style="margin-right: 12px;float:right;color:green; background: #02ffe1;border: 1px solid rgba(0, 0, 0, .1);border-radius: 50%;box-sizing: border-box;height: 12px;width: 12px;" class="glyphicon glyphicon-football"><sub>.</sub></i></p></div></li>')

                $.ajax({
                    url: '/profileimages/images/img_' + item.my_contact_id + '.png',
                    type: 'HEAD',
                    error: function() {
                        $('#contactbox_icon' + item.my_contact_id).attr("src", "{{asset('/profile/nocover.png')}}");
                    },
                    success: function() {
                        $('#contactbox_icon' + item.my_contact_id).attr("src", '/profileimages/images/img_' + item.my_contact_id + '.png');

                    }
                })


            });
        }
    });
});
/*     });
}); */


var place;
var lat;
var lng;

function initMap() {

    var map = new google.maps.Map(document.getElementById('map'), {
        center: {
            /* lat: 30.157458, 
            lng: 71.52491540000005 */
            @if(Auth::user() - > lat)
            lat: {
                { Auth::user() - > lat }
            },
            lng: {
                { Auth::user() - > lng }
            }
            @else
            lat: 30.157458,
            lng: 71.52491540000005
            @endif
        },
        zoom: 15
    });
    var image = 'https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi-dotless2.png';
    var beachMarker = new google.maps.Marker({
        @if(Auth::user() - > lat)
        position: {
            lat: {
                { Auth::user() - > lat }
            },
            lng: {
                { Auth::user() - > lng }
            }
        },
        @else
        position: { lat: 30.157458, lng: 71.52491540000005 },
        @endif

        map: map,
        icon: image
    });
    var card = document.getElementById('pac-card');
    var input = document.getElementById('pac-input');
    var types = document.getElementById('type-selector');
    var strictBounds = document.getElementById('strict-bounds-selector');

    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

    var autocomplete = new google.maps.places.Autocomplete(input);

    // Bind the map's bounds (viewport) property to the autocomplete object,
    // so that the autocomplete requests use the current map bounds for the
    // bounds option in the request.
    autocomplete.bindTo('bounds', map);

    var infowindow = new google.maps.InfoWindow();
    var infowindowContent = document.getElementById('infowindow-content');
    infowindow.setContent(infowindowContent);
    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        place = autocomplete.getPlace();
        console.log(place.geometry.location.lat(), place.geometry.location.lng());
        lat = place.geometry.location.lat();
        lng = place.geometry.location.lng();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17); // Why 17? Because it looks good.
        }
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        infowindowContent.children['place-icon'].src = place.icon;
        infowindowContent.children['place-name'].textContent = place.name;
        infowindowContent.children['place-address'].textContent = address;
        infowindow.open(map, marker);
    });

    // Sets a listener on a radio button to change the filter type on Places
    // Autocomplete.
    function setupClickListener(id, types) {
        var radioButton = document.getElementById(id);
        radioButton.addEventListener('click', function() {
            autocomplete.setTypes(types);
        });
    }

    setupClickListener('changetype-all', []);
    setupClickListener('changetype-address', ['address']);
    setupClickListener('changetype-establishment', ['establishment']);
    setupClickListener('changetype-geocode', ['geocode']);

    document.getElementById('use-strict-bounds')
        .addEventListener('click', function() {
            console.log('Checkbox clicked! New state=' + this.checked);
            autocomplete.setOptions({
                strictBounds: this.checked
            });
        });
}

function DoSubmit() {
    document.propetyform.lng.value = place.geometry.location.lng();
    document.propetyform.lat.value = place.geometry.location.lat();
    return true;
}


$("#edit_map_submit").click(function() {
    console.log(lat, lng)
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: '{{route("savelatlng")}}',
        type: 'POST',
        data: {
            _token: _token,
            lat: lat,
            lng: lng
        },
        success: function(data) {
            if (data == "ok") {
                window.location.assign(window.location.href)
            }
        }
    });
})
$(function() {
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: '{{route("getlatlng")}}',
        type: 'POST',
        data: {
            _token: _token,
        },
        success: function(data) {
            lat = data.lat;
            lng = data.lng;
        }
    });
})





var x;

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    /*   x.innerHTML = "Latitude: " + position.coords.latitude + 
      "<br>Longitude: " + position.coords.longitude; */
    console.log(position.coords.latitude, position.coords.longitude);
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: '{{route("setloginlocation")}}',
        type: 'POST',
        data: {
            _token: _token,
            lat: position.coords.latitude,
            lng: position.coords.longitude
        },
        success: function(data) {
            console.log(data)
        }
    });

}
getLocation()

$.ajax({
    url: '{{asset("/profileimages/cover/img_$id.png")}}',
    type: 'HEAD',
    error: function() {
        //inner ajax
        $.ajax({
            url: '{{asset("/profileimages/cover/img_$id.jpg")}}',
            type: 'HEAD',
            error: function() {
                //inner ajax
                $.ajax({
                    url: '{{asset("/profileimages/cover/img_$id.gif")}}',
                    type: 'HEAD',
                    error: function() {
                        //inner ajax
                        $.ajax({
                            url: '{{asset("/profileimages/cover/img_$id.jpeg")}}',
                            type: 'HEAD',
                            error: function() {
                                $("#cover-photo").attr("src", "{{asset('/profile/nocover.png')}}");
                            },
                            success: function() {
                                $("#cover-photo").attr("src", '{{asset("/profileimages/cover/img_$id.jpeg")}}');
                            }
                        })
                    },
                    success: function() {
                        $("#cover-photo").attr("src", '{{asset("/profileimages/cover/img_$id.gif")}}');
                    }
                })
            },
            success: function() {
                $("#cover-photo").attr("src", '{{asset("/profileimages/cover/img_$id.jpg")}}');
            }
        })
    },
    success: function() {
        $("#cover-photo").attr("src", '{{asset("/profileimages/cover/img_$id.png")}}');
    }
})

$.ajax({
    url: '{{asset("/profileimages/images/img_$id.png")}}',
    type: 'HEAD',
    error: function() {
        //inner ajax
        $.ajax({
            url: '{{asset("/profileimages/images/img_$id.jpg")}}',
            type: 'HEAD',
            error: function() {
                //inner ajax
                $.ajax({
                    url: '{{asset("/profileimages/images/img_$id.jpeg")}}',
                    type: 'HEAD',
                    error: function() {
                        //inner ajax
                        $.ajax({
                            url: '{{asset("/profileimages/images/img_$id.gif")}}',
                            type: 'HEAD',
                            error: function() {
                                $("#menu_link").attr("src", "{{asset('/profile/nodp.png')}}");
                                $("#profilephoto").attr("src", "{{asset('/profile/nodp.png')}}");
                            },
                            success: function() {
                                $("#menu_link").attr("src", '{{asset("/profileimages/images/img_$id.gif")}}');
                                $("#profilephoto").attr("src", '{{asset("/profileimages/images/img_$id.gif")}}');
                            }
                        })
                    },
                    success: function() {
                        $("#menu_link").attr("src", '{{asset("/profileimages/images/img_$id.jpeg")}}');
                        $("#profilephoto").attr("src", '{{asset("/profileimages/images/img_$id.jpeg")}}');
                    }
                })
            },
            success: function() {
                $("#menu_link").attr("src", '{{asset("/profileimages/images/img_$id.jpg")}}');
                $("#profilephoto").attr("src", '{{asset("/profileimages/images/img_$id.jpg")}}');
            }
        })
    },
    success: function() {
        $("#menu_link").attr("src", '{{asset("/profileimages/images/img_$id.png")}}');
        $("#profilephoto").attr("src", '{{asset("/profileimages/images/img_$id.png")}}');
    }
})

function video_verify() {
    alert($('#videofile').files[0].size);
    /* e.preventDefault();
    var len = $('#username').val().length;
    if (len < 6 && len > 1) {
        this.submit();
    } */
}