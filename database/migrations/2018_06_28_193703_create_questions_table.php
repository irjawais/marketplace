<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string("category_id")->references('id')->on('category');
            $table->longText('youtube_url');
            $table->longText('question');
            $table->longText('opt_1');
            $table->longText('opt_2');
            $table->longText('opt_3');
            $table->longText('opt_4');
            $table->longText('msg_wrong')->nullable();
            $table->longText('msg_correct')->nullable();
            $table->string('answer')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
