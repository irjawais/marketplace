

<html>

<head>

    <title>NewsyLetter</title>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="icon" href='{{asset("/profile/small_logo.png")}}'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <style>
            .body1 {
            margin-top: 2%;
        }
        
        .profile {
            width: 100%;
            position: relative;
            background: #FFF;
            padding-bottom: 5px;
            margin-bottom: 20px;
        }
        
        .profile .image {
            display: block;
            position: relative;
            z-index: 1;
            overflow: hidden;
            text-align: center;
            border: 5% solid #FFF;
        }
        
        .profile .user {
            position: relative;
            padding: 0% 2% 2%;
        }
        
        .profile .user .avatar {
            position: absolute;
            left: 2%;
            top: -60%;
            z-index: 2;
        }
        
        .profile .user h2 {
            font-size: 100%;
            line-height: 25%;
            display: block;
            float: left;
            margin: 4% 0% 0% 10%;
            font-weight: bold;
        }
        
        .profile .user .actions .btn {
            margin-bottom: 0%;
        }
        
        .profile .info {
            float: left;
            margin-left: 5%;
        }
        
        .img-profile {
            position: relative;
            left: 5%;
            top: 10%;
            width: 20%;
        }
        
        .img-cover {
            width: 100%;
        }
        
        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            background-color: silver;
            color: white;
            text-align: center;
        }
        .topnav {
  overflow: hidden;
  
}

.topnav a {
  float: left;
  display: block;
  color: black;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #2196F3;
  color: white;
}

.topnav .search-container {
  float: left;
}

.topnav input[type=text] {
  padding: 6px;
  margin-top: 8px;
  font-size: 16px;
  border: none;
}

.topnav .search-container button {
  float: right;
  padding: 6px 10px;
  margin-top: 8px;
  margin-right: 16px;
  background: #ddd;
  font-size: 24px;
  border: none;
  cursor: pointer;
}

.topnav .search-container button:hover {
  background: #ccc;
}

@media screen and (max-width: 600px) {
  .topnav .search-container {
    float: none;
  }
  .topnav a, .topnav input[type=text], .topnav .search-container button {
    float: none;
    display: block;
    text-align: left;
    width: 100%;
    margin: 0;
    padding: 14px;
  }
  .topnav input[type=text] {
    border: 1px solid #ccc;  
  }
}
    </style>
    <style>
    #menu_box {
        position: absolute;
    z-index: 1000;
    background-color: white;
    color: #797171;
    display: none;
   width: 12%;
    float: right;
    left: 0;
    margin-left: 87%;
    padding: 12px;
 
    
}
/* #profile-edit-icon:hover li {
  display:block;
} */
#menu_link {
 /* background-color: blue; */
    color: white;
  height: 25px;
  
}
    </style>
   
</head>

<body>

<?php 
if($user){
    $id =  $user->id;
    $src = "/profileimages/images/img_".$id."";
    $cover = "/profileimages/cover/img_".$id."";
    $my_id =  Auth::user()->id;
    $my_src = "/profileimages/images/img_".$my_id."";
}
?>
<script>
    
        $.ajax({
    url:'{{asset("/profileimages/cover/img_$id.png")}}',
    type:'HEAD',
    error: function(){
        $("#cover-photo").attr("src", "{{asset('/profile/nocover.png')}}");
    },
    success: function(){
    
    }})
</script>
<script>
   
    $.ajax({
    url:'{{asset("/profileimages/images/img_$id.png")}}',
    type:'HEAD',
    error: function(){
        //$("#menu_link").attr("src", "{{asset('/profile/nodp.png')}}");
        $("#profilephoto").attr("src", "{{asset('/profile/nodp.png')}}");
        
    },
    success: function(){
    
    }})
    $.ajax({
    url:'{{asset("/profileimages/images/img_$my_id.png")}}',
    type:'HEAD',
    error: function(){
        $("#menu_link").attr("src", "{{asset('/profile/nodp.png')}}");
    },
    success: function(){
    
    }})
</script>
<nav class="navbar navbar-default" style="margin-bottom:  0;">
<div class="container-fluid">  
    <div class="navbar-header">
      <a class="navbar-brand" href="{{route('home')}}"><img class="imgimg-responsive"  style="width: 34%;height: 45px;" src='{{asset("main/Home_files/logo.png")}}' id="img3" /></a>
    </div>
    <ul class="nav navbar-nav" style="float:right;">
      
      
      <li><a href="#"><img class="imgimg-responsive"   src='{{asset("/profile/noti.png")}}' height="30" /></a></li>
      <li><a ><div id="setting" class="header1"> 
        <i id="status-icon"  style="float:right;buttom:0;position:absolute;color:green; background: #02ffe1;border: 1px solid rgba(0, 0, 0, .1);border-radius: 50%;box-sizing: border-box;height: 12px;margin: 28px 1px 0 15px;width: 12px;" class="glyphicon glyphicon-football"><sub>.</sub></i>
    
   
<img class="imgimg-responsive"  id="menu_link" src='{{asset("$my_src.png")}}' height="30"  /></div></a></li>
   </ul>
  </div>
  <div id="menu_box">
    <button type="button" id="freevisible" class="btn btn-success btn-xs">Free</button>
    <button type="button" id="busyvisible" class="btn btn-danger btn-xs">Busy</button>
    <button type="button" id="invisiblevisible" class="btn  btn-xs">Invisible</button>
    <br><br>
       <a  href="{{route('home')}}"> Profile</a><br>
        Contact<br>
        Wallet<br>
        Make a post<br>
        Contact<br>
        Wallet<br>
        <hr>    
        <a href="{{ url('/logout') }}"> Logout </a>
    </div>
    </div>
</nav>

<div class="topnav">
 
  <div class="search-container ">
    <form action="#">
      <input id="searchbox" type="text" placeholder="Search.." name="search">
      <button type="submit"><i class="fa fa-search"></i></button>
    </form>
    <ul id="dropdownlist" style="display: block;height: 200px;width: 20%;left: 12px;position: absolute;z-index: 30;top: 17%;"class="list-group">
	
	</ul>
  </div>
  <img  class="imgimg-responsive"  class="imgimg-responsive" style="float:right;width: 7pc;height: 88px;"src='{{asset("/profile/power_table/owner_icon.png")}}' />

</div>

        <div class="body1">
            <div class="col-md-12">
                    <div class="profile clearfix">
                        <div class="image">
                            <img id="cover-photo" style="    height: 400px;" src='{{asset("$cover.png")}}'class="img-cover img-thumbnail img-responsive">
                        </div>

                        <div class="user clearfix">
                        
                            <!-- profile picture -->
                            <div id="profile-edit-icon" class="avatar" style="top: -17%;">
                                <img id="profilephoto" style="width: 138px;border:70%;border-color: white;border:  1px solid white;background: #fffcfc;border-radius:  3px;" src='{{asset("$src.png")}}' class=" img-responsive img-profile">
                                <i id="dp_status-icon"  style="float:right;buttom:0;position:absolute;color:green; background: #02ffe1;border: 1px solid rgba(0, 0, 0, .1);border-radius: 50%;box-sizing: border-box;height: 12px;margin: 0px 1px 0 131px;width: 12px;" class="glyphicon glyphicon-football"><sub>.</sub></i>
                            </div>
                            <div style="width: 26%;position: absolute;float:left;left: 11% !important;top: -10%;z-index: 6;">
                            <h2 style="color:white;font-size: 254%;">{{$user->name}} </h2>
                            </div>
                            <div style="position: absolute;float:right;right:  2% !important;top: -17%;z-index: 2;">
                                <div style="position:" class="btn-group dropdown">
                                <button  class="btn btn-default btn-sm tip btn-responsive" title="" data-toggle="dropdown"><span class="glyphicon glyphicon-heart glyphicon glyphicon-white"></span> Save <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                    <li><a href="/addcontact/{{$user->id}}">Add Contact</a></li>
                                    <li><a href="#">Remove Contact</a></li>
                                    <li><a href="#" id="note_btn" >Add Note</a></li>
                                    <li><a href="/blockcontact/{{$user->id}}">Block</a></li>
                                    <li><a href="/unblockcontact/{{$user->id}}">Unblock</a></li>
                                </ul>
                                
                                <div class="btn-group">
                                 <button class="btn btn-default btn-sm tip btn-responsive" title="" data-original-title="Send message"><span class="glyphicon glyphicon-envelope glyphicon glyphicon-white"></span> Message</button>

                                 <button data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default btn-sm tip btn-responsive" title="" data-original-title="Recommend"><span  class="glyphicon glyphicon-share-alt glyphicon glyphicon-white"></span> Recommend</button>
                                 <button data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default btn-sm tip btn-responsive" title="" data-original-title="Recommend"><span  class="glyphicon glyphicon-share-alt glyphicon glyphicon-white"></span> Report</button>

                                 {{-- <ul class="dropdown-menu">
                                        <li><span  class="glyphicon glyphicon-share-alt glyphicon glyphicon-white"></span> Recommend</li>
                                        <li><span  class="glyphicon glyphicon-share-alt glyphicon glyphicon-white"></span> Report</li>
                                    </ul> --}}
                                </div>
                             </div>
                            </div>

                            <nav style=" margin-left:-2%; font-size: 150%; width:104%;" class="navbar navbar-default">
                                <div class="container">
                                    <ul style="padding-left:12%" class="nav navbar-nav">
                                        <li ><a href="{{route('userTimeline',$user->id)}}">Timeline</a></li>
                                        <li ><a href="#">About</a></li>
                                        <li><a href="#">Buy</a></li>
                                        <li><a href="#">Sell</a></li>
                                        <li><a href="#">Articale</a></li>
                                        <li><a href="#"> <img src='{{asset("profile/promotion.png")}}' style="float:right;" id="img3" /></a></li>
                                        <li><a  id="updatepolicy" title="Policy" data-toggle="dropdown"> <img src='{{asset("profile/policy.png")}}' style="float:right;" id="img3" /></a>
                                        


                                        </li>
                                        <li><a href="/download/{{$user->id}}" ><img src='{{asset("profile/pdf.png")}}' style="float:right;" id="img3" /></a>
                                            <li><a href="{{route('quizView',$user->id)}}"><img class="imgimg-responsive"  src='{{asset("profile/quiz.png")}}' style="float:right;width:  38px;height:  32px;" id="img3" /></a></li>

                                        </li>
                                    </ul>
                                </div>
                            </nav>
                            <div class="container-fluid">
                       
 <div class="row">
    <div class="col-sm-3" ><h5>${{$user->hr or "0"}} USD/hr <i class="fa fa-address-book"></i></h5><h5>Hoursly Rate </h5></div>

    <div class="col-sm-3" style="line-height: 6pc;" > Language : {{$user->language or "English"}} <i class="fa fa-address-book" aria-hidden="true"></i></div>
    <div class="col-sm-3" style="line-height: 6pc;">Website :<a href="https://{{$user->website}}"> {{$user->website or "default"}}</a> <i class="fa fa-address-book" aria-hidden="true"></i></div>
    <div class="col-sm-3"  style="line-height: 6pc;"><a href="https://www.google.com/maps?saddr=My+Location&daddr={{$user->lat}},{{$user->lng}}" target="_blank">Location  <i class="fa fa-map-marker"></i><img style="width:40px;height:40px;" src="https://yt3.ggpht.com/a-/AJLlDp2OpNuRPHKO3kc7lFNPeA0pbX1GtL4HAzKqUA=s900-mo-c-c0xffffffff-rj-k-no">
        
</a>
@if($user->visiblity!=3)
    <a href="https://www.google.com/maps?saddr=My+Location&daddr={{$user->live_lat}},{{$user->live_lng}}" target="_blank"><img style="width:40px;height:30px;" src='{{asset("profile/live.png")}}'/></a>
    @endif
    </div>
</div>
</div>
<div class="container-fluid">
<div class="col-sm-11" >
        <h3>{{ !$user->paragraph_title ? "Title Not Available.!" : $user->paragraph_title  }} </h3>    
<p>{{ !$user->paragraph ? "Detail Not Available.!" : $user->paragraph }}</p></div>
</div>
<div class="container-fluid" style="border: 1px solid gray; padding: 4;">
    <button id="video_btn" style="display:block;margin:auto;border:1px solid green" type="button" class="btn"><i class="fa fa-play-circle"></i> Watch Video <i class="fa fa-address-book" aria-hidden="true"></i></button>
</div>
<div class="form-group">
  <label for="comment">Test:</label>
  <textarea class="form-control" rows="5" id="comment"></textarea>
</div>            

                            <!-- button -->
                            
                        </div>

                        <!-- info -->
                        
                    </div>
            </div>





            </div>
            <div class="modal fade" id="coverphotomodel" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                
                <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <form method="post"  action="{{ route('changecover') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                        <div class="form-group">
                            <label for="usr">Please Choose only png Photo:</label>
                            <input type="file" accept="image/x-png"   name="profileimg" class="form-control" id="usr">
                        </div>
                        <button type="submit" class="btn btn-default">Upload</button>
                    </form>
                </div>
                <!-- <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div> -->
            </div>
        </div>
    </div>
    <div class="modal fade" id="profilechange" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <!-- <div class="modal-header">
                    
                    
                </div> -->
                <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <form method="post"  action="{{ route('changeprofile') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                        <div class="form-group">
                            <label for="usr">Please Choose only png Photo:</label>
                            <input type="file" accept="image/x-png"   name="profileimg" class="form-control" id="usr">
                        </div>
                        <button type="submit" class="btn btn-default">Upload</button>
                    </form>
                </div>
                <!-- <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div> -->
            </div>
        </div>
    </div>
     <!-- note model -->
 <div class="modal fade" id="note_model" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
        <form action="{{route('addnote')}}" method="post">
        {{ csrf_field() }}
            <input type="hidden" name="id" value="{{$user->id}}">
        <div class="form-group">
        <label for="comment">Note:</label>
        <textarea name="notes" class="form-control" rows="5" id="comment"></textarea>
        </div>
        <button type="submit" class="btn btn-success">Add</button>
    </form>
        </div>
        
      </div>
    </div>
</div>
     <!-- Policy model -->
     <div class="modal fade" id="policy_model" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <div class="border border-primary" id="summernote">
                
            </div>
       
    </div>
        
      </div>
    </div>
</div>
<div class="modal fade" id="video_model" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
        <div class="modal-body">
            @if($user->video=="file")
            <video style="width:100%" controls>
            <source src='{{asset("video/$user->id.mp4")}}' type="video/mp4">
            <source src='{{asset("video/$user->id.mp4")}}' type="video/ogg">
            Your browser does not support HTML5 video.
            </video>
            @elseif(!$user->video) 
            @else
            <?php 
               $url = $user->video;
               // $url = "http://www.youtube.com/watch?v=7zWKm-LZWm4&feature=relate";
                parse_str( parse_url( $url, PHP_URL_QUERY ), $url_vars );
                //echo $url_vars['v'];  
            ?>
            <iframe style="width:100%;height:350px;"
            src="https://www.youtube.com/embed/{{$url_vars['v']}}?autoplay=1">
            </iframe>
            @endif
        </div>
        
      </div>
    </div>
</div>

<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
            <script>
                      var o = <?php echo $user->visiblity ?>;
        $(document).ready(function() {
            if(o==1){
                $('#status-icon').css('background', 'green')
                $('#dp_status-icon').css('background', 'green')
                
            }
            if(o==2){
                $('#status-icon').css('background', 'red')
                $('#dp_status-icon').css('background', 'red')
            }
            if(o==3){
                $('#status-icon').css('display', 'none')
                $('#dp_status-icon').css('background', 'none')
            }
            
        });
        $(function() {
            $('#summernote').html('<?php echo $user->note;  ?>');
                //$('#summernote').summernote();
            $('#profile').on('click', function() {
                $('#profilechange').modal('show');
            });
            $('#coverphoto').on('click', function() {
                $('#coverphotomodel').modal('show');
            });
            
            $('#video_btn').on('click', function() {
                $('#video_model').modal('show');
            });
            $('#edit-page').on('click', function() {
                window.location.href = '{{route("update.profile")}}';
            });
            
            
        });
        $('#freevisible').on('click',function() {
            var _token = $('input[name="_token"]').val();
           $.ajax({
            url: "{{route('changestatustofree')}}",
               type: 'POST',
               data: {
                   _token : _token,
                   id:<?php echo $user->id ?>,
                   status:1
               },
               success: function (data) {
                   if(data=="ok"){
                    window.location.assign(window.location.href)
                   }
                
               }
           });
          
        });
        $('#busyvisible').on('click',function() {
            var _token = $('input[name="_token"]').val();
           $.ajax({
            url: "{{route('changestatustofree')}}",
               type: 'POST',
               data: {
                   _token : _token,
                   id:<?php echo $user->id ?>,
                   status:2
               },
               success: function (data) {
                   if(data=="ok"){
                    window.location.assign(window.location.href)
                   }
                
               }
           });
          
        });
        $('#invisiblevisible').on('click',function() {
            var _token = $('input[name="_token"]').val();
           $.ajax({
            url: "{{route('changestatustofree')}}",
               type: 'POST',
               data: {
                   _token : _token,
                   id:<?php echo $user->id ?>,
                   status:3
               },
               success: function (data) {
                   if(data=="ok"){
                    window.location.assign(window.location.href)
                   }
                
               }
           });
          
        });
        
       
    </script>
            <script>
                $('#note_btn').on('click', function() {
                $('#note_model').modal('show');
            });
                 $(function() {
            $('#profile').on('click', function() {
                $('#profilechange').modal('show');
            });
            $('#updatepolicy').on('click', function() {
                $('#policy_model').modal('show');
            });
            $('#profileverification').on('click', function() {
                $('#profileverification_model').modal('show');
            });
            
            $('#coverphoto').on('click', function() {
                $('#coverphotomodel').modal('show');
            });
            $('#edit-page').on('click', function() {
                window.location.href = '{{route("update.profile")}}';
            });
        });
        $('#freevisible').on('click',function() {
            var _token = $('input[name="_token"]').val();
           $.ajax({
            url: "{{route('changestatustofree')}}",
               type: 'POST',
               data: {
                   _token : _token,
                   id:<?php echo $user->id ?>,
                   status:1
               },
               success: function (data) {
                   if(data=="ok"){
                    window.location.assign(window.location.href)
                   }
                
               }
           });
          
          });
        $('#busyvisible').on('click',function() {
            var _token = $('input[name="_token"]').val();
           $.ajax({
            url: "{{route('changestatustofree')}}",
               type: 'POST',
               data: {
                   _token : _token,
                   id:<?php echo $user->id ?>,
                   status:2
               },
               success: function (data) {
                   if(data=="ok"){
                    window.location.assign(window.location.href)
                   }
                
               }
           });
      });
        $('#invisiblevisible').on('click',function() {
            var _token = $('input[name="_token"]').val();
           $.ajax({
            url: "{{route('changestatustofree')}}",
               type: 'POST',
               data: {
                   _token : _token,
                   id:<?php echo $user->id ?>,
                   status:3
               },
               success: function (data) {
                   if(data=="ok"){
                    window.location.assign(window.location.href)
                   }
                
               }
           });
          
        });

                 var o = <?php echo Auth::user()->visiblity ?>;
        $(document).ready(function() {
            if(o==1){
                $('#status-icon').css('background', 'green')
            }
            if(o==2){
                $('#status-icon').css('background', 'red')
            }
            if(o==3){
                $('#status-icon').css('display', 'none')
            }
            
        });
        $('#menu_link').click(function() {
        $('#menu_box').toggle();
        });
                    </script>
             <script>
                $("#profile-edit-icon").hover(function(){
                    $('#profile').css('display','block')
                }, function(){
                    $('#profile').css('display','none')
                })
                $("#cover-photo").hover(function(){
                    $('#coverphoto').css('display','block')
                }, function(){
                    
                  //  $('#coverphoto').css('display','none')
                })
                $("#cover-photo").mouseleave(function(){
                   // $('#coverphoto').css('display','none')
                })
                
            </script>
            <script>
                $(document).ready(function() {
                
                   $('#searchbox').keyup(function(e) {
                    var _token = $('input[name="_token"]').val();

                   input = $(this).val();
                   $("#dropdownlist").empty()
                   if(input.length>2){
                    $.ajax({
                        url: '{{route("searching")}}',
               type: 'POST',
               data: {
                   _token : _token,
                    data:input
               },
               success: function (data) {
               
                if(data){
                    
                    data.forEach(function(item) {
                        $("#dropdownlist").css("display", "block");
                        $("#dropdownlist").append('<li class="list-group-item" style="height: 47px !important;"><img class="imgimg-responsive"  style="width:50px;height:30px;float:left" id="searchbox_icon'+item['id']+'" src="/demo/man01.png"><a style="padding:0;" href="/profile/'+item["id"]+'">'+item['name']+'</a><i   style="margin-right: 12px;float:right;color:green; background: #02ffe1;border: 1px solid rgba(0, 0, 0, .1);border-radius: 50%;box-sizing: border-box;height: 12px;width: 12px;" class="glyphicon glyphicon-football"><sub>.</sub></i></li>');
                        
                        $.ajax({
                        url:'profileimages/images/img_'+item['id']+'.png',
                        type:'HEAD',
                        error: function(){
                            $('#searchbox_icon'+item['id']).attr("src", "{{asset('/profile/nocover.png')}}");
                        },
                        success: function(){
                            $('#searchbox_icon'+item['id']).attr("src",'/profileimages/images/img_'+item['id']+'.png');

                        }})

                    });
                    
                    
                }
               }
                });
                   }
             

                  }); });
                
            </script>



</body>

</html>