

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="application-name" content="">
    <meta name="description" content="">
    <title>NewsyLetter</title>
    <link rel="icon" href='{{asset("/profile/small_logo.png")}}'>
    <link rel="stylesheet" href="{{ asset('main/Home_files/style.css') }}">
    <link rel="stylesheet" href="{{ asset('main/Home_files/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('main/Home_files/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('main/Home_files/icofont.css') }}">
    <script src="{{ asset('main/Home_files/jquery.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('main/Home_files/settings.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
   
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    
    {{-- <link rel="stylesheet" href="{{ asset('main/Home_files/responsive.css') }}"> --}}
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    
<style>
      .fas.fa-star  {color: #37cea6;}
      /* .btn-group {
            float: left;
        } */
        .note-editor.note-frame.panel > * div.btn-group {
            float: none !important;
        }
        
</style>
</head>

<body>
    <!-- Header Start -->
    <header class="navigation-bar">
        <div class="container-fluid">
            <div class="row">
                
                @guest
                <div class="col-sm-12 col-md-8 col-lg-7 order-sm-1 order-2 primary-menu-tm">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#"><img style="width: 34%;height: 45px;" src='{{asset("main/Home_files/logo.png")}}' id="img3" /></a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                     <span class="navbar-toggler-icon"></span>
                     </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <!-- <ul class="navbar-nav">
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="{{url('/')}}" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Home</a>
                                    
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Hire Freelancer<span class="sr-only">(current)</span></a>
                                   
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Find Work</a>
                                   
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Blog</a>
                                    
                                </li>
                            </ul> -->
                            
                        </div>
                    </nav>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-5 order-sm-2 order-1 header-notification">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <ul class="navbar-nav ml-auto ymp-right">
                           <!--  <li class="nav-item menu-button">
                                <a class="nav-link" href="#">Post A Project</a>
                            </li> -->
                            <li class="nav-item">
                            <a href="#" style="line-height:  3pc;"><img  src='{{asset("/profile/noti.png")}}' height="20" /></a>
                                <!-- <a class="nav-link" href="#" data-toggle="modal" data-target="#sign-in-page">Sign Up</a> -->
                            </li>
                            <li class="nav-item menu-bar">
                                <a class="nav-link" href="#" data-toggle="modal" data-target="#signin_model"><span class="glyphicon glyphicon-user"></span></a>
                            </li>
                            
                        </ul>
                    </nav>
                </div> 
                @else
                <!-- Header Start -->
      
               <div class="col-sm-12 col-md-6 col-lg-7 order-sm-1 order-2 primary-menu-tm">
                  <nav class="navbar navbar-expand-lg navbar-light bg-light">
                     <a class="navbar-brand" href="{{url('/')}}"><img style="width: 34%;height: 45px;" src='{{asset("main/Home_files/logo.png")}}' alt="logo"></a>
                     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                     <span class="navbar-toggler-icon"></span>
                     </button>
                    
                  </nav>
               </div>
               <div class="col-sm-12 col-md-6 col-lg-5 order-sm-2 order-1 header-notification">
               <nav class="navbar navbar-expand-lg navbar-light bg-light">
                     <ul class="navbar-nav ml-auto ymp-right-menu">
                        
                        <li class="menu-bar dropdown dropdown-user">
                            <a href="{{ url('/home') }}"> Home &nbsp; &nbsp; </a>
                           @if(Auth::user()->email_verify1=1)
                           <a href="{{ url('/logout') }}"> logout </a>
                           @else
                           <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           <img style="width:20px;height:30px;" src='{{asset("profile/noimage.png")}}'  alt="">
                           <span>{{ Auth::user()->name}}</span>
                           </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ url('/logout') }}"> logout </a></li>
                            </ul>
                           @endif
                        </li>
                     </ul>
                  </nav>
               </div>
           
      <!-- Header End -->
                @endguest
            </div>
        </div>
    </header>
    <div class="container">
       @foreach ($posts as $post)
       @if($post->is_archive==1)
            @continue; 
       @endif
           <div class="row">
                    <div class="col-sm-2" >
                            <img style="height: 45px;" src='{{asset('/profile/nodp.png')}}' id="img3" />
                    </div>
                    <div class="col-sm-8" >
                            <h3  style="text-decoration: underline;"><a class="postBtn" value="{{$post->id}}" href="javascript:void(0);">{{$post->subject}}</a></h3>
                    </div>
                    <div class="col-sm-2" >
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                    </div>
                    
            </div>
            <div class="row">
                <div class="col-sm-12" >
                          {{$post->name}}
                </div>
            </div>
            <div class="row">
                    <div class="col-sm-12" >
                             {{strip_tags($post->notes)}} -- {{$post->user_id}}
                    </div>
            </div>
            <div class="row">
                    <div class="col-sm-3" >
                     {{$post->created_at}}
                    </div>
                    <div class="col-sm-3" >
                            <i class="fas fa-share-square"></i>
                    </div>
                    <div class="col-sm-3" >
                           
                    </div>
                    <div class="col-sm-3" >
                    </div>
            </div>
            <br><br>
        @endforeach

       </div>
    </div>
    <!-- Header End -->
    <!-- Start Modals -->
   

    <section id="modal">
            
        <div class="container">
               
            <!-- Modal One -->
            <div class="modal fade" id="signup_model" role="dialog" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content sign-up">
                        <!-- <div class="modal-header">
                      
                            <button data-dismiss="modal" class="close">×</button>
                        </div> -->
                        <div class="modal-body" style="padding-left: 0px;padding-right: 0px;padding-top: 0px;">
                        <button data-dismiss="modal" class="close" style="cursor: pointer;color: white;border: 1px solid #AEAEAE;background: #605F61;display: block;line-height: 0px;padding: 11px 3px;background-color:red;">×</button><br><br>
                        <div style="padding:10px;height:50px; margin-bottom:-2px;  padding-bottom:5px; padding-left:15px; margin-left:20px; background-color:#e6f2ff; color:black; width:450px">
                        <font size="3" style="float:left">Sign Up </font>
                        <!-- <a style="float:right;margin-left: 15px;" style="color:blue " href=""><b>Forget Password</b></a> -->
                        </div>

        <div >
            <form method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
            <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <input  type="text" class="form-control"  name="name"  style="margin-bottom: 0px;" placeholder="Name" required>
                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                    <input id="email" type="text" class="form-control" name="email"  placeholder="Email" style="margin-bottom: 0px;" required>
                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
                
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input id="password" type="password" class="form-control" name="password" style="margin-bottom: 0px;" placeholder="Password" required>
                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif 
                </div>
                
                <!-- <div class="checkbox">
                    <label style="margin-left: 15px;"><input type="checkbox" name="remember"> <b>Remember me</b></label>
                </div> -->
                <br>
                <button style="margin-left: 15px;" type="submit" class="btn btn-success"><b>Register</b></button>
               <!--  <button style="margin-left: 5px;" type="submit" class="btn btn-primary"><b>LOGIN With Facebook</b></button><br><br> -->
                <p>_________________________________________________________</p>

                <label style="margin-left: 15px; ">  Already have an account ?<a style="margin-left: 15px;" id="signin_btn" style="color:blue " href="#">Sign in Here</a></label>
            </form>
        </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- Modal Two -->
            <div class="modal fade" id="signin_model" role="dialog" tabindex="-1" aria-hidden="true">
                
                <div class="modal-dialog">
                    <div class="modal-content sign-in">
                        <!-- <div class="modal-header">
                            
                            
                        </div> -->
                        <div class="modal-body" style="
    padding-left: 0px;
    padding-right: 0px;
    padding-top: 0px;
">
                        <button data-dismiss="modal" class="close" style="cursor: pointer;
color: white;
border: 1px solid #AEAEAE;
background: #605F61;

display: block;
line-height: 0px;
padding: 11px 3px;
background-color:red;">×</button><br><br>
                        <div style="padding:10px;height:50px; margin-bottom:-2px; border-radius: 5px 5px; padding-bottom:5px; padding-left:15px; margin-left:15px; background-color:#e6f2ff; color:black; width:450px">
                        <font size="3" style="float:left">Sign In </font>
                        <a style="float:right;margin-left: 15px;" style="color:blue " href="{{ route('password.request') }}"><b>Forget Password</b></a>
                        </div>
                            <form method="post" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                      
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                    <input type="text" class="form-control" name="email"  placeholder="Email" style="margin-bottom: 0px;" required>
                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input  type="password" class="form-control" name="password" style="margin-bottom: 0px;" placeholder="Password" required>
                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif 
                </div>
                
                <div class="checkbox">
                    <label style="margin-left: 15px;"><input type="checkbox" name="remember"> <b>Remember me</b></label>
                </div>
                <br>
                <button style="margin-left: 15px;" type="submit" class="btn btn-success"><b>Login</b></button>
                <a style="margin-left: 5px;" href="{{url('/redirect')}}" class="btn btn-primary"><b>LOGIN With Facebook</b></a><br><br>
                <p>_________________________________________________________</p>

                <label style="margin-left: 15px; ">  Don't have an account ?<a id="signup_btn" style="margin-left: 15px;" style="color:blue " href="#">Sign up</a></label>
            </form>

                           
                        </div>
                       
                    </div>
                </div>
            </div>
            
        </div>
    </section>
  
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> --}}

<div id="postModel" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header" style="padding-left: 0px;padding-right: 0;">
                    <button type="button" style="float:right;" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <div class="modal-body">
                        <div class="row">
                                <div class="col-sm-3">
                                  <p>PostId : <small id="post_id"></small></p>
                                </div>
                                <div class="col-sm-3" >
                                    <p id="post_date"></p>
                                      </div>
                                <div class="col-sm-6" >
                                  <p id="post_amount"></p>
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-sm-3">
                                  <p>Post for Zone : </p>
                                </div>
                                <div class="col-sm-9" >
                                    <i class="fas fa-map-marker-alt"></i> <small id="post_zone"></small>
                        </div>
                        </div>
                        <div class="row">
                                <div class="col-sm-9">
                                  <h3 id="post_subject">Subject</h3>
                                </div>
                                <div class="col-sm-3" >
                                  
                        </div>
                        </div>
                        <div class="row" style="    border: 1px solid magenta; margin: 1px;padding: 3px;">
                                
                                <div id="post_notes" class="col-sm-12" >
                                  
                                </div>
                        </div>
                        <div class="row">
                                        <div class="col-sm-3">
                                        
                                        </div>
                                        <div class="col-sm-9" >
                                                <br>
                                                <button style="float: right;" type="button" id="submit_proposal" class="btn btn-success">Submit Proposal</button>
                            </div>
                        </div>
                      
                  {{-- <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div> --}}
                </div>
            
              </div>
      </div>
</div>

<div class="modal fade" id="submitProposalModel" role="dialog">
        <div class="modal-dialog modal-lg">
        
          <!-- Modal content-->
          <div class="modal-content">
                <div class="modal-header" style="padding-left: 0px;padding-right: 0;">
                        <button type="button" style="float:right;" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                        <form method="post"  action="{{route('submitProposal')}}">
                            {{ csrf_field() }}
                        
                            <input type="hidden" id="proposal_post_id" name="post_id" value="">
                        
                            <div class="row"  style="all: none !important;">
                                    
                                    <div  class="col-sm-12">
                                            <textarea id="submit_proposal_notes" name="notes" style="height:200px;" name="editordata" required></textarea>
                                    </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-3">
                                <h5 id="edit_post_subject">Amount :</h5>

                                </div>
                                <div class="col-sm-3" >
                                <input required type="number" step=".01" name="amount" class="form-control form-control-sm validate ml-0">
                                    
                                </div>
                                <div class="col-sm-6" >
                                </div>
                            </div>
                            <div class="row">
                                    <div  class="col-sm-12" >
                                <button style="margin: auto; display: block;" type="submit" class="btn btn-default">Save</button>
                                    </div>
                            </div>
                            </form>
                    </div>
            
          </div>
          
        </div>
      </div>
</body>

<script>
    $('.postBtn').on('click', function() {
            var id = $(this).attr('value');
            var _token = $('input[name="_token"]').val();
            $.ajax({
               url: '{{route("getPostData")}}',
               type: 'POST',
               data: {
                   _token : _token,
                   id : id
                },
               success: function (data) {
                 console.log(data)
                 $('#post_id').text(data.id)
                 
                 $('#proposal_post_id').val(data.id)
                 $('#post_subject').text(data.subject)
                 $('#post_zone').text(data.zone)
                 $('#post_notes').html(data.notes)
                 $('#post_date').html(data.created_at)
                 if(data.amount)
                 $('#post_amount').text("$ "+data.amount)
                 else 
                 $('#post_amount').text(" ")
               }
           });
            $('#postModel').modal('toggle');
    });
    $('#signup_btn').on('click', function() {
        $('#signin_model').modal('hide');
                $('#signup_model').modal('show');
                
    });
    $('#signin_btn').on('click', function() {
        $('#signin_model').modal('show');
                $('#signup_model').modal('hide');
                
    });
    @if (Session::get('registeration_attempt') == 'reg' && $errors->has('email') ||  $errors->first('password') || $errors->first('phoneno'))
        $( document ).ready(function() {
            $('#signup_model').modal('show');
        });
    @elseif(!Session::get('registeration_attempt') && $errors->has('email') || $errors->has('password'))
        $( document ).ready(function() {
            $('#signin_model').modal('show');
        });
    @endif
    $(document).ready(function(){
        $("#submit_proposal").click(function(){
        @if(isset(Auth::user()->id))
        
            console.log('d')
            $('#postModel').modal('toggle');
            
            $('#submitProposalModel').modal('toggle');

        
        @else
            $('#postModel').modal('toggle');
            $('#signin_model').modal('show');
        
        @endif
        });
    });
    $(document).ready(function() {
        
        $('#submit_proposal_notes').summernote({
                placeholder: 'Type Here',
                tabsize: 4,
                height: 300
            });
    });
</script>
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> --}}
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
</html>