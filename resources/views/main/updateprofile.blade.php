<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('profile/ust.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


</head>

<body>
<?php 
    $id =  Auth::user()->id;
    $src = "/profileimages/images/img_".$id."";
    $cover = "/profileimages/cover/img_".$id."";
?>

    <div class="header1">
        <div id="img3" class="header1"><img style="width:0%" src='{{asset("main/Home_files/logo.png")}}' id="img3" /></div>
        <div id="searcharea" class="header1"><input placeholder="search here..." type="text" id="searchbox" /></div>
     
        <div id="setting" class="header1">    <i  style="float:right;buttom:0;position:absolute;color:green;background: #fa3e3e;
    border: 1px solid rgba(0, 0, 0, .1);
    border-radius: 50%;
    box-sizing: border-box;
    height: 12px;
    margin: 2px 1px 0 0;
    width: 12px;" class="glyphicon glyphicon-football"><sub>.</sub></i>
<img src='{{asset("$src.png")}}' height="30"  /></div>
        <div id="logout" class="header1"><img src='{{asset("/profile/noti.png")}}' height="30" /></div>
    </div>

    <div class="header0001">
    </div>
    <div class="container" style="width:60%">
        <br>
        <br>
        <br>
        <br>
  <h2>Edit Profile Information</h2>
  <form method="post" action="{{route('update.profilePost')}}">
  {{ csrf_field() }}
  <div class="form-group">
      <label for="comment">About Title:</label>
      <input type = "text" value="{{$user->abouttitle}}" name="abouttitle" class="form-control"/>
    </div>
    <div class="form-group">
      <label for="comment">About Profile:</label>
      <textarea class="form-control"  name="aboutdescr" rows="5" id="comment">{{$user->aboutdescr}}</textarea>
    </div>
    <div class="form-group">
      <label for="comment">Username:</label>
      <input type = "text" name="username" value="{{$user->username}}" class="form-control"/>
    </div>
    <div class="form-group">
      <label for="comment">Phone No:</label>
      <input type = "text" name="phoneno" value="{{$user->phoneno}}" class="form-control"/>
    </div>
    <div class="form-group">
      <label for="comment">Address:</label>
      <input type = "text" name="address" value="{{$user->address}}" class="form-control"/>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
  </form>
</div>
</body>

</html>