

<html>

    <head>
    
        <title>NewsyLetter</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href='{{asset("/profile/small_logo.png")}}'>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @yield('style')
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    </head>
    
    <body>
        <nav class="navbar navbar-default" style="margin-bottom:  0;">
            <div class="container-fluid">  
                <div class="navbar-header">
                  <a class="navbar-brand" href="/home"><img class="imgimg-responsive"  style="width: 34%;height: 45px;" src='{{asset("main/Home_files/logo.png")}}' id="img3" /></a>
                </div>
                <ul class="nav navbar-nav" style="float:right;">
                  
                  
                  <li><a href="#"><img class="imgimg-responsive"   src='{{asset("/profile/noti.png")}}' height="30" /></a></li>
                  <li><a ><div id="setting" class="header1"> 
                           <i id="status-icon"  style="float:right;buttom:0;position:absolute;color:green; background: #02ffe1;border: 1px solid rgba(0, 0, 0, .1);border-radius: 50%;box-sizing: border-box;height: 12px;margin: 28px 1px 0 15px;width: 12px;" class="glyphicon glyphicon-football"><sub>.</sub></i>
                
               
            <img class="imgimg-responsive"  id="menu_link" src='{{asset("$src.png")}}' height="30"  /></div></a></li>
               </ul>
              </div>
              <div id="menu_box">
                <button type="button" id="freevisible" class="btn btn-success btn-xs">Free</button>
                <button type="button" id="busyvisible" class="btn btn-danger btn-xs">Busy</button>
                <button type="button" id="invisiblevisible" class="btn  btn-xs">Invisible</button>
                    <br> <br>    
                    <a href="/home">Profile</a><br>
                    Contact<br>
                    Wallet<br>
                    Make a post<br>
                    Contact<br>
                    Wallet<br>
                    <a href="javascript:void(0)" id="settings">Settings</a>
                    <hr>    
                    <a href="{{ url('/logout') }}"> Logout </a>
                </div>
                </div>
            </nav>
            
            
    @yield('content')
    @yield('model')         
    @yield('script')
    </body>
    
    </html>