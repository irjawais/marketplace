@extends('agent.layouts.apps')

@section('content')
<div class="main">

			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					
					<div class="row">
						
						<div class="col-md-12">
						<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">New Property</h3>
								</div>
								<div class="panel-body">
                                    <div style="background-color: #008000;
                                                border-radius: 3px;
                                                clear: both;
                                                color: #ffffff;
                                                float: left;
                                                margin-bottom: 7px;
                                                margin-top: 8px;
                                                padding: 7px 0;
                                                text-indent: 9px;
                                                text-transform: uppercase;
                                                width: 99.6%;">
                                    <h6 >PROPERTY TYPE AND LOCATION</h6>
                                    </div>
                                    
                                
                                <div class="col-md-3">
                                    <label>Purpose: * </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="label label-info"> Sale
                                    <input type="radio"  name="purpose">
                                    <span class="checkmark"></span>
                                    </label>
                                
                                    <label class="label label-info">Buy
                                    <input type="radio" name="purpose">
                                    <span class="checkmark"></span>
                                    </label>
                                    <label class="label label-info">Rent
                                    <input type="radio" name="purpose">
                                    <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div style="clear: both;"></div>
                                
                                <div class="col-md-3">
                                    <label>Property Type: * </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="label label-info"> Home
                                    <input type="radio"  name="purpose">
                                    <span class="checkmark"></span>
                                    </label>
                                
                                    <label class="label label-info">Plots
                                    <input type="radio" name="purpose">
                                    <span class="checkmark"></span>
                                    </label>
                                    <label class="label label-info"> Commercial
                                    <input type="radio" name="purpose">
                                    <span class="checkmark"></span>
                                    </label>
                                </div>
                               




									<input type="text" id="country">
									<br>
									<input type="password" class="form-control" value="asecret">
									<br>
									<textarea class="form-control" placeholder="textarea" rows="4"></textarea>
									<br>
									<select class="form-control">
										<option value="cheese">Cheese</option>
										<option value="tomatoes">Tomatoes</option>
										<option value="mozarella">Mozzarella</option>
										<option value="mushrooms">Mushrooms</option>
										<option value="pepperoni">Pepperoni</option>
										<option value="onions">Onions</option>
									</select>
									<br>
									<label class="fancy-checkbox">
										<input type="checkbox">
										<span>Fancy Checkbox 1</span>
									</label>
									<label class="fancy-checkbox">
										<input type="checkbox">
										<span>Fancy Checkbox 2</span>
									</label>
									<label class="fancy-checkbox">
										<input type="checkbox">
										<span>Fancy Checkbox 3</span>
									</label>
									<br>
									<label class="fancy-radio">
										<input name="gender" value="male" type="radio">
										<span><i></i>Male</span>
									</label>
									<label class="fancy-radio">
										<input name="gender" value="female" type="radio">
										<span><i></i>Female</span>
									</label>

									
								</div>
							</div>
						</div>
					</div>
					


				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="{{ asset('countrySelect.min.js') }}""></script>
<script>
  $("#country").countrySelect();
</script>

@endsection
