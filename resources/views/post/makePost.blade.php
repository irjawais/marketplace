<!DOCTYPE html>
<html lang="en">

<head>
        <title>NewsyLetter</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href='{{asset("/profile/small_logo.png")}}'>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDqaB1GlRDNUy9CJ29SzwedZ8Ew11OIiXA&libraries=places&callback=initMap" async defer></script> --}}
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDqaB1GlRDNUy9CJ29SzwedZ8Ew11OIiXA&libraries=places&callback=initAutocomplete" async defer></script>
    
    <style>
        body {
            margin-top: 40px;
        }
        
        .stepwizard-step p {
            margin-top: 10px;
        }
        
        .stepwizard-row {
            display: table-row;
        }
        
        .stepwizard {
            display: table;
            width: 50%;
            position: relative;
        }
        
        .stepwizard-step button[disabled] {
            opacity: 1 !important;
            filter: alpha(opacity=100) !important;
        }
        
        .stepwizard-row:before {
            top: 14px;
            bottom: 0;
            position: absolute;
            content: " ";
            width: 100%;
            height: 1px;
            background-color: #ccc;
            z-order: 0;
        }
        
        .stepwizard-step {
            display: table-cell;
            text-align: center;
            position: relative;
        }
        
        .btn-circle {
            width: 30px;
            height: 30px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px;
        }
    </style>
    <script>
        $(document).ready(function() {
            var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn');

            allWells.hide();

            navListItems.click(function(e) {
                e.preventDefault();
                var $target = $($(this).attr('href')),
                    $item = $(this);

                if (!$item.hasClass('disabled')) {
                    navListItems.removeClass('btn-primary').addClass('btn-default');
                    $item.addClass('btn-primary');
                    allWells.hide();
                    $target.show();
                    $target.find('input:eq(0)').focus();
                }
            });

            allNextBtn.click(function() {
                var typepost = $("input[name=typepost]:checked").val();
                if(!typepost)
                    return false;
                if (typepost == 1) {
                    $("#amount_field").css("display", "block");
                    // $("#aritcle_value").text("Buy Post")  
                    var curStep = $(this).closest(".setup-content"),
                        curStepBtn = curStep.attr("id"),
                        nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                        curInputs = curStep.find("input[type='text'],input[type='url']"),
                        isValid = true;

                    $(".form-group").removeClass("has-error");
                    for (var i = 0; i < curInputs.length; i++) {
                        if (!curInputs[i].validity.valid) {
                            isValid = false;
                            $(curInputs[i]).closest(".form-group").addClass("has-error");
                        }
                    }

                    if (isValid)
                        nextStepWizard.removeAttr('disabled').trigger('click');
                    //setting end

                    $("#aritcle_value").text("Buy Post")
                    console.log("d");
                } else if (typepost == 2) {

                    //  $("#aritcle_value").text("Sell Post")
                    $("#amount_field").css("display", "block");
                    // $("#aritcle_value").text("Buy Post")  
                    var curStep = $(this).closest(".setup-content"),
                        curStepBtn = curStep.attr("id"),
                        nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                        curInputs = curStep.find("input[type='text'],input[type='url']"),
                        isValid = true;

                    $(".form-group").removeClass("has-error");
                    for (var i = 0; i < curInputs.length; i++) {
                        if (!curInputs[i].validity.valid) {
                            isValid = false;
                            $(curInputs[i]).closest(".form-group").addClass("has-error");
                        }
                    }

                    if (isValid)
                        nextStepWizard.removeAttr('disabled').trigger('click');
                    //setting end

                    $("#aritcle_value").text("Sell Post")
                } else {
                    //settings
                    $('#amount_field').prop('required',false);
                    var curStep = $(this).closest(".setup-content"),
                        curStepBtn = curStep.attr("id"),
                        nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                        curInputs = curStep.find("input[type='text'],input[type='url']"),
                        isValid = true;

                    $(".form-group").removeClass("has-error");
                    for (var i = 0; i < curInputs.length; i++) {
                        if (!curInputs[i].validity.valid) {
                            isValid = false;
                            $(curInputs[i]).closest(".form-group").addClass("has-error");
                        }
                    }

                    if (isValid)
                        nextStepWizard.removeAttr('disabled').trigger('click');
                    //setting end

                    $("#aritcle_value").text("Post Article")
                }
                var subject = $("input[name=subject]").val();
                $("#subject_view").text(subject)
                console.log(subject)

            });

            $('div.setup-panel div a.btn-primary').trigger('click');
        });
    </script>
    <script>
        $(document).ready(function(){
        load_category(0);
        //$('#category_model').modal('show');
        
          $('#summernote').summernote({
                placeholder: 'Type Here',
                tabsize: 4,
                height: 100
            });
     
    });
    
    function load_category(parent_id){
        var _token = $('input[name="_token"]').val();
        
        $.ajax({
            headers: 
            {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{route('ajaxGetCategory')}}",
            type: 'POST',
            data:{
                _token:_token,parent_id:parent_id
            },
            success: function (data) {
                $("#category_list").empty();
                data.forEach(function(v, i, a){
                        $("#category_list").append("<li class='glyphicon glyphicon-chevron-right list-group-item ulli ui-state-default' id='list"+v.id+"' value='"+v.id+"'>"+v.name.toUpperCase()+" <input style='float:right;' type='radio' name='category_id' value='"+v.id+"' required></li>");
                })
                }
    })}
    var parent_id=0;
    var category_id= 0;
    function load_category_after(parent_id){
        var _token = $('input[name="_token"]').val();
        $.ajax({
            headers: 
            {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{route('ajaxGetCategory')}}",
            type: 'POST',
            data:{
                _token:_token,parent_id:parent_id
            },
            success: function (data) {
                 data.forEach(function(v, i, a){
                       
                        $("#list"+parent_id).append("<li class='list-group-item ulli ' id='list"+v.id+"' value='"+v.id+"'>"+v.name.toUpperCase()+" <input style='float:right;' type='radio' name='category_id' value='"+v.id+"' required></li>");
                          
                })
            }
    })}
    $(document).ready(function(){
        $('body').on('click', '.ulli', function(e) {
            //$(".ulli").click(function(e){
                console.log(this)
                if(e.target !== e.currentTarget) return;

                if ( $("#list"+this.value).children().length > 2 ) {
                    $("#list"+this.value).children().remove()
                    return;
                }
                load_category_after(this.value);
        });
        $('input[name="category_id"]').change(function(){
            console.log(this.value)
        });
        
        $('#btnPreview').on('click', function(e) {
                e.preventDefault();
                //$('#post_id').text(data.id)
                 $('#post_subject').text( $('input[name="subject"]').val())
                 $('#post_zone').text($('input[name="zone"]').val())
                 $('#post_notes').html($('.note-editable').html())
                // $('#post_date').html($('input[name="created_at"]').val()data.)
                $('#postModel').modal('show');
        });
    });
    </script>
    <script>
       function initAutocomplete() {
          var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -33.8688, lng: 151.2195},
            zoom: 13,
            mapTypeId: 'roadmap'
          });
  
          // Create the search box and link it to the UI element.
          var input = document.getElementById('pac-input');
          var searchBox = new google.maps.places.SearchBox(input);
          map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
  
          // Bias the SearchBox results towards current map's viewport.
          map.addListener('bounds_changed', function() {
            searchBox.setBounds(map.getBounds());
          });
  
          var markers = [];
          // Listen for the event fired when the user selects a prediction and retrieve
          // more details for that place.
          searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();
  
            if (places.length == 0) {
              return;
            }
  
            // Clear out the old markers.
            markers.forEach(function(marker) {
              marker.setMap(null);
            });
            markers = [];
  
            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function(place) {
              if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
              }
              var icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
              };
  
              // Create a marker for each place.
              markers.push(new google.maps.Marker({
                map: map,
                icon: icon,
                title: place.name,
                position: place.geometry.location
              }));
  
              if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
              } else {
                bounds.extend(place.geometry.location);
              }
            });
            map.fitBounds(bounds);
          });
        }
  
      </script>
      
</head>

<body>

    <div class="container">
        <div class="container">

            <div class="stepwizard col-md-offset-3">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step">
                        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                        <p>Step 1</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                        <p>Step 2</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                        <p>Step 3</p>
                    </div>
                </div>
            </div>

            <form role="form" action="{{route('submitPost')}}" method="post">
                    {{ csrf_field() }}
                <div class="row setup-content" id="step-1">
                    <div class="col-xs-6 col-md-offset-3">
                        <div class="col-md-12">
                            <h3> What You want to post?</h3>
                            <div class="form-group">
                                <div class="radio">
                                    <label><input type="radio" name="typepost" value="1" required>Buy Post</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="radio">
                                    <label><input type="radio" name="typepost" value="2" required>Sell Post</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="radio">
                                    <label><input type="radio" name="typepost" value="3" required>Article Post</label>
                                </div>
                            </div>
                            <button id="get_article_name" class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
                        </div>
                    </div>
                </div>
                <div class="row setup-content" id="step-2">
                    <div class="col-xs-6 col-md-offset-3">
                        <div class="col-md-12">
                            <h3 id="aritcle_value"> </h3>
                            <div class="form-group">
                                <input maxlength="200" name="subject" type="text" required="required" class="form-control" placeholder="Subject" required/>
                            </div>
                            <button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
                        </div>
                    </div>
                </div>
                <div class="row setup-content" id="step-3">
                    <div class="col-xs-6 col-md-offset-3">
                        <div class="col-md-12">
                            <h3> Post Article <small id="subject_view"> </small></h3>
                            <div class="row">
                                <div class="col-sm-4" >
                                  <p>Select Category</p>
                                </div>
                                <div class="col-sm-8" >
                                        <ul   class="list-group connectedSortable" id="category_list" style="    padding-bottom: 100px;padding-bottom: 100px;display:  block;">
                                        </ul>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-4" >
                                  <p>Posting Zone</p>
                                </div>
                                <div class="col-sm-8" >
                                    <input id="pac-input" class="form-control" name="zone" type="text" placeholder="Search Box" required>
                                    
                                    <div id="map" style=" display:none"></div>
                                
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-sm-12" >
                                    <br>
                                    <form method="post">
                                    <textarea id="summernote" name="notes" style="height:200px;    width: 100%;" name="editordata"></textarea>
                                    <!-- <textarea name="notes" class="form-control" rows="5" id="comment"></textarea> -->
                                   {{--  <button type="submit" class="btn btn-success">Save</button> --}}
                                    </form>
                                </div>
                              </div>
                              <div class="row">
                              <div class="form-group" >
                                <input style="display:none" id="amount_field" name="amount_field" type="number" step=".01" class="form-control" placeholder="Amount" required/>
                                </div>
                            </div>
                            <button class="btn btn-success btn-lg pull-right" type="submit"> Submit </button>
                             <button id="btnPreview" class="btn btn-info btn-lg pull-right"> Preview </button>
                             <a  href="{{route('timeline')}}" class="btn btn-danger btn-lg pull-right"> Cancel </a>
                        </div>
                    </div>
                </div>
                
            </form>

        </div>
    </div>

    <div id="postModel" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header" style="padding-left: 0px;padding-right: 0;">
                        <button type="button" style="float:right;background: red;margin-right:  10px;" class="close" data-dismiss="modal">&times;</button>
                      </div>
                  <div class="modal-body">
                        <div class="row">
                                <div class="col-sm-3">
                                  <p>PostId : <small id="post_id">#</small></p>
                                </div>
                                <div class="col-sm-3" >
                                    <p id="post_date"></p>
                                      </div>
                                <div class="col-sm-6" >
                                  <p></p>
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-sm-3">
                                  <p>Post for Zone : </p>
                                </div>
                                <div class="col-sm-9" >
                                    <i class="fas fa-map-marker-alt"></i> <small id="post_zone"></small>
                        </div>
                        </div>
                        <div class="row">
                                <div class="col-sm-9">
                                  <h3 id="post_subject">Subject</h3>
                                </div>
                                <div class="col-sm-3" >
                                  
                        </div>
                        </div>
                        <div class="row" style="    border: 1px solid magenta; margin: 1px;padding: 3px;">
                                
                                <div id="post_notes" class="col-sm-12" >
                                  
                                </div>
                        </div>
                  </div>
                  {{-- <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div> --}}
                </div>
            
              </div>
      </div>
</body>

</html>