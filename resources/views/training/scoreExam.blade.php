
@extends('layouts.apps1')
@section('style')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="{{asset('number_masking/css/intlTelInput.css')}}" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<link href="{{asset('choosen/chosen.css')}}" rel="stylesheet">

<style>
    *{
        margin: 0;
        padding: 0;
    }
        .body1 {
        margin-top: 2%;
    }
    
    .profile {
        width: 100%;
        position: relative;
        background: #FFF;
        padding-bottom: 5px;
        margin-bottom: 20px;
    }
    
    .profile .image {
        display: block;
        position: relative;
        z-index: 1;
        overflow: hidden;
        text-align: center;
        border: 5% solid #FFF;
    }
    
    .profile .user {
        position: relative;
        padding: 0% 2% 2%;
    }
    
    .profile .user .avatar {
        position: absolute;
        left: 2%;
        top: -60%;
        z-index: 2;
    }
    
    .profile .user h2 {
        font-size: 100%;
        line-height: 25%;
        display: block;
        float: left;
        margin: 4% 0% 0% 10%;
        font-weight: bold;
    }
    
    .profile .user .actions .btn {
        margin-bottom: 0%;
    }
    
    .profile .info {
        float: left;
        margin-left: 5%;
    }
    
    .img-profile {
        position: relative;
        left: 5%;
        top: 10%;
        width: 20%;
    }
    
    .img-cover {
        width: 100%;
    }
    
    .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        background-color: silver;
        color: white;
        text-align: center;
    }
    .topnav {
overflow: hidden;

}

.topnav a {
float: left;
display: block;
color: black;
text-align: center;
padding: 14px 16px;
text-decoration: none;
font-size: 17px;
}

.topnav a:hover {
background-color: #ddd;
color: black;
}

.topnav a.active {
background-color: #2196F3;
color: white;
}

.topnav .search-container {
float: left;
}

.topnav input[type=text] {
padding: 6px;
margin-top: 8px;
font-size: 16px;
border: none;
}

.topnav .search-container button {
float: right;
padding: 6px 10px;
margin-top: 8px;
margin-right: 16px;
background: #ddd;
font-size: 24px;
border: none;
cursor: pointer;
}

.topnav .search-container button:hover {
background: #ccc;
}

@media screen and (max-width: 600px) {
.topnav .search-container {
float: none;
}
.topnav a, .topnav input[type=text], .topnav .search-container button {
float: none;
display: block;
text-align: left;
width: 100%;
margin: 0;
padding: 14px;
}
.topnav input[type=text] {
border: 1px solid #ccc;  
}
}
</style>
<style>
#menu_box {
    position: absolute;
z-index: 1000;
background-color: white;
color: #797171;
display: none;
width: 170px;
float: right;
left: 0;
margin-left: 87%;
padding: 12px;
}
/* #profile-edit-icon:hover li {
display:block;
} */
#menu_link {
/* background-color: blue; */
color: white;
height: 25px;

}
</style>
<style>
.main-section{
width: 300px;
position: fixed;
right:50px;
bottom:-350px;
z-index: 3000000;
}
.first-section:hover{
cursor: pointer;
}
.open-more{
bottom:0px;
transition:2s;
}
.border-chat{
border:1px solid #FD8468;
margin: 0px;
}
.first-section{
background-color:#FD8468;
}
.first-section p{
color:#fff;
margin:0px;
padding: 10px 0px;
}
.first-section p:hover{
color:#fff;
cursor: pointer;
}
.right-first-section{
text-align: right;
}
.right-first-section i{
color:#fff;
font-size: 15px;
padding: 12px 3px;
}
.right-first-section i:hover{
color:#fff;
}
.chat-section ul li{
list-style: none;
margin-top:10px;
position: relative;
}
.chat-section{
overflow-y:scroll;
height:300px;
}
.chat-section ul{
padding: 0px;
}
.left-chat img,.right-chat img{
width:50px;
height:50px;
float:left;
margin:0px 10px;
}
.right-chat img{
float:right;
}
.second-section{
padding: 0px;
margin: 0px;
background-color: #F3F3F3;
height: 300px;
}
.left-chat,.right-chat{
overflow: hidden;
}
.left-chat p,.right-chat p{
background-color:#FD8468;
padding: 10px;
color:#fff;
border-radius: 5px; 
float:left;
width:60%;
margin-bottom:20px;
}
.left-chat span,.right-chat span{
position: absolute;
left:70px;
top:60px;
color:#B7BCC5;
}
.right-chat span{
left:45px;
}
.right-chat p{
float:right;
background-color: #FFFFFF;
color:#FD8468;
}
.third-section{
border-top: 1px solid #EEEEEE;
}
.text-bar input{
width:90%;
margin-left:-15px;
padding:10px 10px;
border:1px solid #fff;
}
.text-bar a i{
background-color:#FD8468;
color:#fff;
width:30px;
height:30px;
padding:7px 0px;
border-radius: 50%;
text-align: center;
}
.left-chat:before{
content: " ";
position:absolute;
top:0px;
left:55px;
bottom:150px;
border:15px solid transparent;

}
.right-chat:before{
content: " ";
position:absolute;
top:0px;
right:55px;
bottom:150px;
border:15px solid transparent;
border-top-color:#fff; 
}
</style>
<style>
/* Always set the map height explicitly to define the size of the div
   * element that contains the map. */

#map {
    height: 100%;
}
/* Optional: Makes the sample page fill the window. */

html,
body {
    height: 100%;
    margin: 0;
    padding: 0;
}

#description {
    font-family: Roboto;
    font-size: 15px;
    font-weight: 300;
}

#infowindow-content .title {
    font-weight: bold;
}

#infowindow-content {
    display: none;
}

#map #infowindow-content {
    display: inline;
}

.pac-card {
    margin: 10px 10px 0 0;
    border-radius: 2px 0 0 2px;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    outline: none;
    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    background-color: #fff;
    font-family: Roboto;
}

#pac-container {
    padding-bottom: 12px;
    margin-right: 12px;
}

.pac-controls {
    display: inline-block;
    padding: 5px 11px;
}

.pac-controls label {
    font-family: Roboto;
    font-size: 13px;
    font-weight: 300;
}

#pac-input {
    background-color: #fff;
    font-family: Roboto;
    font-size: 15px;
    font-weight: 300;
    margin-left: 12px;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;
    width: 400px;
    width: 88%;
display: inline-block;
}

#pac-input:focus {
    border-color: #4d90fe;
}

#title {
    color: #fff;
    background-color: #4d90fe;
    font-size: 25px;
    font-weight: 500;
    padding: 6px 12px;
}
div.pac-container.pac-logo{
    z-index: 2000 !important;
}
div.intl-tel-input.allow-dropdown{
    float: right;
    width: 100%;
}

</style>
<link href="{{asset('treeview/easyTree.css')}}" rel="stylesheet">
@endsection
@section('content')

<?php 
if(Auth::user()){
$id =  Auth::user()->id;
$src = "/profileimages/images/img_".$id."";
$cover = "/profileimages/cover/img_".$id."";
}
?>
<div class="container-fluid">
    <br><br><br>
    <div class="row">
      <div class="col-sm-2" >
          
        <div id="category_container" >
            <ul   class="list-group connectedSortable" id="category_list" style="    padding-bottom: 100px;padding-bottom: 100px;display:  block;">
            </ul>
    
        </div>  
    </div>
    <div class="col-sm-10" >
        <center>
        <h1 style="color:#2196F3;">Score : {{$quiz_result->score}}</h1>
        <a href="{{route('showAnswer',$quiz_result->id)}}" type="button" class="btn btn-info btn-lg">Show Answer</a>
        </center>
        <div class="row">
                <br><br><br>
                <div class="col-sm-4" >
                <a href="{{route('initExamMode')}}" type="button" class="btn btn-danger btn-lg">Again Exam</a>
                </div>
               
                <div class="col-sm-4" >
                        <a href="{{route('publishExam',$quiz_result->id)}}" type="button" class="btn btn-info btn-lg">Publish</a>
                </div>
                
                <div class="col-sm-4" >
                                <a href="{{route('initTrainingMode')}}"  type="button" class="btn btn-success btn-lg">Back To Study</a>
                </div>
        </div>
    </div>

   
    </div>
  </div>
@endsection

@section('model')

<div class="modal fade" id="coverphotomodel" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <!-- <div class="modal-header">
                
                
            </div> -->
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
                <form method="post"  action="{{ route('changecover') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                    <div class="form-group">
                        <label for="usr">Please Choose only png Photo:</label>
                        <input type="file" accept="image/*"   name="profileimg" class="form-control" id="usr">
                    </div>
                    <button type="submit" class="btn btn-default">Upload</button>
                </form>
            </div>
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> -->
        </div>
    </div>
</div>
<div class="modal fade" id="profilechange" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <!-- <div class="modal-header">
                
                
            </div> -->
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
                <form method="post"  action="{{ route('changeprofile') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                    <div class="form-group">
                        <label for="usr">Please Choose only png Photo:</label>
                        <input type="file" accept="image/*"   name="profileimg" class="form-control" id="usr">
                    </div>
                    <button type="submit" class="btn btn-default">Upload</button>
                </form>
            </div>
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> -->
        </div>
    </div>
</div>
<div class="main-section">
<div class="row border-chat">
    <div class="col-md-12 col-sm-12 col-xs-12 first-section">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-6 left-first-section">
                <p id="box_lable">Contacts</p>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-6 right-first-section">
                <a href="#"><i class="fa fa-minus" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</div>
<div class="row border-chat" >
    <div class="col-md-12 col-sm-12 col-xs-12 second-section">
        <div class="chat-section">
            <ul id="contact_list">
                
                
            </ul>
        </div>
    </div>
</div>
<div class="row border-chat" style="background: #f3f3f3;">
    <div class="col-md-12 col-sm-12 col-xs-12 third-section">
        <div class="text-bar">
             <span id="btn-contacts" style="padding-left: 21%;" class="fa fa-address-book fa-3x"></span>
            <span id="btn-blocks" style="padding-left: 21%;" class="fa fa-ban fa-3x"></span>
            <!-- <input type="text" placeholder="Write messege"><a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a> -->
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="powertable_model" role="dialog">
<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3>Power Table<img class="imgimg-responsive"  src='{{asset("profile/powertable2.png")}}' style="float:left;" id="img3" /><h3>
    </div>
    <div class="modal-body">
    
    <form>
    <!-- <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-3"></div>
        <div class="col-sm-3"></div>
        <div class="col-sm-3"></div>
    </div> -->
    <div class="row">
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Super User
            </label>
        </div>
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Edit post
            </label>
        </div>
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Set Report Permission
            </label>
        </div>
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Approve Category
            </label>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Company employee 
            </label>
        </div>
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Dispute Manage
            </label>
        </div>
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Transfer Other Balance for Dispute Resolvesions
            </label>
        </div>
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Approve Question
            </label>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Can Change power  
            </label>
        </div>
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Need 2nd Verifyer
            </label>
        </div>
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">View Audit Data
            </label>
        </div>
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Can View Power
            </label>
        </div>
    </div>    
    <div class="row">
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Change Service Queue
            </label>
        </div>
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Can Create Customer Service Agent
            </label>
        </div>
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Profile Embedded Code Verifyer
            </label>
        </div>
        <div class="col-sm-3"></div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Verify Profile Confidential Documents
            </label>
        </div>
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Can Create Customer Service Agent
            </label>
        </div>
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Profile Embedded Code Update
            </label>
        </div>
        <div class="col-sm-3"></div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Customer Service Agent
            </label>
        </div>
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Approve Training
            </label>
        </div>
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Add Category
            </label>
        </div>
        <div class="col-sm-3"></div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Need Verify Post
            </label>
        </div>
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Admin promotion setup
            </label>
        </div>
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Add Training
            </label>
        </div>
        <div class="col-sm-3"></div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Block Post
            </label>
        </div>
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Run add/edit SQL
            </label>
        </div>
        <div class="col-sm-3">
            <label class="checkbox-inline">
            <input type="checkbox" value="">Add Exam Question
            </label>
        </div>
        <div class="col-sm-3"></div>
    </div>
    <br>
    <center>
    <button type="button" class="btn btn-success">Add Edit</button>
    <button type="button" class="btn btn-success">Verify</button>
    </center>
    </form>
    </div>
    <div class="row">
        <div class="col-sm-3">
            
        </div>
        <div class="col-sm-3">
            <label class="checkbox-inline">
            Editted By:
            </label>
        </div>
        <div class="col-sm-3">
            <label class="checkbox-inline">
            Approve By:
            </label>
        </div>
        <div class="col-sm-3">
            <label class="checkbox-inline">
            Verify By:
            </label>
        </div>
    </div>
  </div>
</div>
</div>

<div class="modal fade" id="profileverification_model" role="dialog">
<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      
<span><strong>Profile Status</strong></span> 
      <div class="col-sm-2" style="float:right;">
        <img class="imgimg-responsive" id="powertable" src='{{asset("profile/powertable2.png")}}' style="float:right;" id="img3" />
        </div>
        <div class="col-sm-7" style="float:right;" ><div  class="progress" style="background: #c8e8cc;">
        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{{$profile_ratio}}%"aria-valuemin="0" aria-valuemax="100" style="width:{{$profile_ratio}}%">{{$profile_ratio}}%
        </div>
        </div>
        </div>
    </div>
    <div class="modal-body">
            <h4><img class="imgimg-responsive"  src='{{asset("profile/powertable.png")}}' style="float:left;" id="img3" /> Personal Data</h4>
        <br>
    <form>
            <div class="row">
                    <div class="col-sm-4">
                            <label for="phone">Phone No</label>
                    </div>
                    <div class="col-sm-4">
                            <input style="display: inline;" class="form-control" id="phone" type="tel">
                    </div>
                    <div class="col-sm-4">
                            {{-- <button type="button" class="btn btn-info">View</button> --}}
                            <button type="button" class="btn btn-success">Verify</button>
                            {{-- <button type="button" class="btn btn-primary">Review</button> --}}
                    </div>
            </div>
            <div class="row">
                    <div class="col-sm-4">
                            <label for="usr" >User Photo Id Attechment:</label>
                    </div>
                    <div class="col-sm-4">
                            <input style="display: inline; " type="file" class="form-control" id="usr">
                    </div>
                    <div class="col-sm-4">
                            <button type="button" class="btn btn-info">View</button>
                            <button type="button" class="btn btn-success">Verify</button>
                            <button type="button" class="btn btn-primary">Review</button>
                    </div>
            </div>
            <div class="row">
                    <div class="col-sm-4">
                            <label for="pwd">Other Attachments:</label>
                    </div>
                    <div class="col-sm-4">
                            <input style="display: inline;" type="file" class="form-control" id="pwd">
                    </div>
                    <div class="col-sm-4">
                            <button type="button" class="btn btn-info">View</button>
                            <button type="button" class="btn btn-success">Verify</button>
                            <button type="button" class="btn btn-primary">Review</button>
                    </div>
            </div>
        
    </form>
    <div >
    <!-- <button type="button" class="btn btn-default">Verify</button>
    <button type="button" class="btn btn-default">Unverify</button> -->
    <br><br>
    <center>
    <button type="button" class="btn btn-default">Save</button>
    </center>
    </div>
    </div>
    
  </div>
</div>
</div>
<div class="modal fade" id="pdf_model" role="dialog">
<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4><img class="imgimg-responsive"  src='{{asset("profile/pdf.png")}}' style="float:left;" id="img3" /> Profile Detail Attachments </h4>

    </div>
    <div class="modal-body">
    <a style="float:right" href="{{route('download.profile.detail.attachments',[$user->id])}}" class="btn btn-default">View</a>
    <form enctype="multipart/form-data" id="demo-form2" method="post" action="{{route('profileattachment')}}">
    {{ csrf_field() }}    
    <div class="form-group">
       <center>
        <input style="display: inline;width:  50%; " name="documentfile" type="file" class="form-control" id="usr">
       </center>    
    </div>
    <br>
    <center>
    <button type="submit" class="btn btn-default">Save</button>
    </center>
    </form>
    <div >
   
    </div>
    </div>
    
  </div>
</div>
</div>
<div class="modal fade" id="video_model" role="dialog">
<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4><img class="imgimg-responsive"   style="float:left;" id="img3" /> Like Video Presentation </h4>

    </div>
    <div class="modal-body" id="vide_container">
    <button id="view_video_btn" style="float:right;" type="button" class="btn btn-default">View</button>
    <br><br>
    <form enctype="multipart/form-data" id="profile_video"  onsubmit="video_verify()"   method="post" action="{{route('upload.video')}}">
    {{ csrf_field() }}    
    <div class="row">
            <div class="col-sm-2" >
                    <input type="radio" name="videocheck" value="file">
            </div>
            <div class="col-sm-3" >
                    Upload Video
            </div>
            <div class="col-sm-7" >
                    <input accept="video/mp4" id="videofile" name="videofile" type="file" class="form-control" id="usr">
                    <span id="video_file_error"></span>
            </div>
    </div>
    <div class="row">
        <div class="col-sm-2" >
            <input type="radio" name="videocheck" value="link">
        </div>
        <div class="col-sm-3" >
            Youtube Link
        </div>
        <div class="col-sm-7" >
            <input  name="videolink" type="text" class="form-control" id="usr">
        </div>
</div>
    <div class="form-group">
      
       {{--  <center>
       <label class="radio-inline">
        <input type="radio" name="videocheck" value="link">Youtube Link
    </label>  <input style="display: inline;width:  50%; " name="videolink" type="text" class="form-control" id="usr">
       </center>  --}}   
    </div>
    <br>
    <center>
    <button type="submit" class="btn btn-default">Upload</button>
    </center>
    </form>
    
    <div >
   
    </div>
    </div>
    
  </div>
</div>
</div>
 <!-- Policy model -->
 <div class="modal fade" id="policy_model" role="dialog">
<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
    
    <form action="{{route('updatepolicy')}}" method="post">
    {{ csrf_field() }}
        <input type="hidden" name="id" value="{{$user->id}}">
    <div class="form-group">
    <label for="comment">Policy:</label>
    <form method="post">
    <textarea id="summernote" name="notes" style="height:200px;" name="editordata"></textarea>
    <!-- <textarea name="notes" class="form-control" rows="5" id="comment"></textarea> -->
    <button type="submit" class="btn btn-success">Save</button>
    </form>

    </div>
    
</form>
    </div>
    
  </div>
</div>
</div>
<div class="modal fade" id="edit_map_model" role="dialog">
<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <input id="pac-input" type="text" class="form-control" name="lo_address" placeholder="Place name">
      <button type="button" id="edit_map_submit" class="btn btn-success">Save</button>
    </div>
    <div class="modal-body">
    <div style="clear: both;"></div>
                        

                        <div id="map" style="width:100%;height:300px;"></div>
                        <div id="infowindow-content">
                            <img class="imgimg-responsive"  src="" width="16" height="16" id="place-icon">
                            <span id="place-name" class="title"></span><br>
                            <span id="place-address"></span>
                        </div>
    </div>
    
  </div>
</div>
</div>
<div class="modal fade" id="profile_status_model" role="dialog">
<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-header">
        
    <button data-dismiss="modal" class="close" style="cursor: pointer;color: white;border: 1px solid #AEAEAE;background: #605F61;display: block;line-height: 0px;padding: 11px 3px;background-color:red;">×</button>
      <span><strong>Profile Status</strong></span> 
      <div class="col-sm-2" style="float:right;">
        <img class="imgimg-responsive" id="powertable" src='{{asset("profile/powertable2.png")}}' style="float:right;" id="img3" />
        </div>
        <div class="col-sm-7" style="float:right;" ><div  class="progress" style="background: #c8e8cc;">
        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{{$profile_ratio}}%"aria-valuemin="0" aria-valuemax="100" style="width:{{$profile_ratio}}%">{{$profile_ratio}}%
        </div>
        </div>
        </div>
        
    </div>
    <div class="modal-body" >
    <div class="row">
        <div class="col-sm-4" >
        <img id="power_table_in_pd" class="imgimg-responsive"  src='{{asset("profile/powertable.png")}}' style="float:left;" id="img3" />
        <h4 style="display:inline-block">Personal Data</h4>
        </div>
        <div class="col-sm-5" >
        <button type="button" class="btn btn-success">Verify</button>
        <button type="button" class="btn btn-warning">Review</button>
        </div>
        <div class="col-sm-3" >
    
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4" >
        
        </div>
        <div class="col-sm-5" >
            <textarea class="form-control" rows="3" id="comment"></textarea>
        </div>
        <div class="col-sm-3" >
        <div class="checkbox">
        <label><input type="checkbox" value="">Reqire Reply</label>
        </div>
        <button type="button" class="btn btn-primary">Reply</button>
        </div>
    </div>
    <br>
    <div class="row">
        
        <div class="col-sm-4">
            <label class="checkbox-inline">
            Editted By:
            </label>
        </div>
        <div class="col-sm-4">
            <label class="checkbox-inline">
            Approve By:
            </label>
        </div>
        <div class="col-sm-4">
            <label class="checkbox-inline">
            Verify By:
            </label>
        </div>
    </div>
    </div>
    
  </div>
</div>
</div>
<div class="modal fade" id="settings_model" role="dialog">
<div class="modal-dialog modal-sm">
  <div class="modal-content">
    <div class="modal-header">
        
    <button data-dismiss="modal" class="close" style="cursor: pointer;color: white;border: 1px solid #AEAEAE;background: #605F61;display: block;line-height: 0px;padding: 11px 3px;background-color:red;">×</button>
      <span><strong>Settings</strong></span> 
      
        
    </div>
    <div class="modal-body" >
    <div class="row">
        <div class="col-sm-4" >
                <button type="button" style="width:150px" class="btn btn-primary">Attachment Size</button>
        </div>
    </div>
    <br>
    <div class="row">
            <div class="col-sm-4" >
                    <button id="language_btn"  type="button" style="width:150px" class="btn btn-primary">Language</button>
            </div>
    </div>
    <br>
    <div class="row">
            <div class="col-sm-4" >
                    <button id="category_btn"  type="button" style="width:150px" class="btn btn-primary">Category</button>
            </div>
    </div>
    <br>
    <div class="row">
            <div class="col-sm-4" >
                    <a href="{{route('addTraining')}}" style="width:150px" class="btn btn-primary">Add Training</a>
            </div>
    </div>
    <br>
    {{-- <div class="row">
            <div class="col-sm-4" >
                    <button type="button" style="width:150px" class="btn btn-primary">Add Exam</button>
            </div>
     </div> --}}
  </div>
</div>
</div>
</div>
<div class="modal fade" id="language_model_box" role="dialog" >
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
                <div class="row">
                    <form id="language_form">
                        <div class="col-sm-12" >
                                <div class="form-group"style="display:inline-block">
                                        <input type="text" required placeholder="Name" id="settings_language_input" class="form-control" style="display:inline-block"  id="usr">
                                </div>
                                <button type="submit" style="width:150px;display:inline-block" class="btn btn-primary">Add</button>
                        </div>
                    </form>    
                 </div>
                 <div id="language_cantainer" style="overflow: scroll;height:  250px;border: 1px solid #3996da;padding:  16px;">
                      
                  
                <div class="row">
                        <div class="col-sm-2" >
                            <strong>Language: List</strong>
                        </div>
                </div>
                <?php

                $multi =[];
               
                $i = 0; 
                $j = 0; 
                if(!empty($languages)){

                
                foreach($languages as $key=> $language){
                    //if($language['user_id']==Auth::user()->id){
                        if($i>=6){
                        $j++;
                        $i = 0;
                    }
                        $multi[$j][$i]["lng"]=$language['language'];
                        $multi[$j][$i]["id"]=$language['id'];
                        //$multi[$j][$key]["id"]=$language['id'];
                    
                    
                     $i++;
                   
                   // }
                }
                /* print "<pre>";
                print_r($multi);
                print "</pre>"; */
                $i = 0; 
                foreach($multi as $row => $value) {
                  $s = sizeof($multi[$i]);
                       print "<div class='row'>";
                         for($j=0;   $j<$s;$j++){
                            $value = $multi[$i][$j]['lng'];
                            $id = $multi[$i][$j]['id'];
                            print "<div class='col-sm-2 child_e' id='$id' >" .  $value . " </div>"; 
                        } 
                       print "</div>";
                       $i++;
                } 
            }
                
              ?>
        </div>
        
      </div>
    </div>
</div>
</div>


<div class="modal fade" id="select_language_model" role="dialog">
<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Choose Language</h4>
      
</div>
    <div class="modal-body" style="height:250px;">
        <div style="overflow: scroll;height:  220px;border: 1px solid #3996da;padding:  16px;">
                <div class='row'>

                        <div class="col-sm-10 col-md-offset-2" >
                        <select class="chosen" id="save_value_language"  multiple="true" >
                            @if($selected_language)
                            @foreach($selected_language as $key=> $lng)
                                <option value="{{$lng->id}}" selected="selected">{{ucfirst($lng->language)}}</option>
                            @endforeach
                            @endif
                            @foreach($languages as $key=> $total_language)
                                <option value="{{$total_language['id']}}">{{ucfirst($total_language['language'])}}</option>
                            @endforeach
                        </select>
                        </div>
                               
                        </div>
                        <br>
                        <div class='row'>
                        <div class="col-sm-10 col-md-offset-2" >
                                <button type="button" id="save_language" class="btn btn-success">Save</button>
                        </div>
                    </div> 
        </div>
    </div>
    
  </div>
</div>
</div>
<div class="modal fade" id="category_model" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Category </h4>
        </div>
        <div class="modal-body" style="overflow-y: scroll; height: 400px;">
                <button id="add_category_btn" style="float:right;" type="button" ><span class="glyphicon glyphicon-plus"></span></button>
                <br><br>
                <div id="category_container" >
                        <ul   class="list-group connectedSortable" id="category_list" style="    padding-bottom: 100px;padding-bottom: 100px;display:  block;">
                </ul>
                {{-- @foreach ($main_categories as $key=> $main_category)
                <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key}}">
                              {{ucfirst($main_category->name)}} <img id="power_table_in_pd" class="imgimg-responsive" style="width:20px;height:20px;" src='{{asset("/category/main_category/category_$main_category->id.png")}}' style="float:left;" id="img3" /></a>
                              <button title="Add Sub Category" value="{{$main_category->id}}"  type="button" class="btn btn-info btn-xs add_sub_category_btn">
                                    Add <span class="glyphicon glyphicon-plus"></span> 
                                </button>
                            </h4>
                          </div>
                          
                        <div id="collapse{{$key}}" class="panel-collapse collapse {{ $key === 0 ? "in" : "" }}">
                            <div class="panel-body">
                                    <ul class="list-group">
                                    @foreach ($main_category->sub_category as $sub_main_category)
                                    <li class="list-group-item">{{ucfirst($sub_main_category['name'])}} <img id="power_table_in_pd" class="imgimg-responsive" style="width:20px;height:20px;" src='{{asset("/category/sub_category/sub_category_".$sub_main_category['id'].".png")}}' style="float:left;" id="img3" /></li>
                                     @endforeach
                                    </ul>
                        </div>
                          </div>
                        </div>
                </div>
                @endforeach --}}
                </div>
        </div>
        
      </div>
      
    </div>
  </div>
  <div class="modal fade" id="category_form_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog cascading-modal modal-avatar modal-sm" role="document" style="margin-top:  23vh;">
            <!--Content-->
            <div class="modal-content">
    
                <div class="modal-body text-center mb-1">
                    <form id="new_category_data" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <h5 class="mb-2" style="text-align:left;">Name</h5>
                    <div class="md-form ml-0 mr-0">
                        <input required  type="text" name="category_name" class="form-control form-control-sm validate ml-0">
                    </div>
                    <h5 class="mb-2" style="text-align:left;">Image <small>Only png</small></h5>
                    <div class="md-form ml-0 mr-0">
                            <input required  type="file" accept="image/x-png" name="category_image" class="form-control form-control-sm validate ml-0">
                        </div>
                        <br>          
                    <div class="text-center mt-4">
                        <button type="submit" class="btn btn-cyan mt-1 btn-xs" style = "    color: #fff;background-color: #9124a3;border-color: #701c7e;" id="category_submit"> Save</button>
                    </div>
                    </form>
                </div>
    
            </div>
            <!--/.Content-->
        </div>
    </div>
    <div class="modal fade" id="sub_category_form_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog cascading-modal modal-avatar modal-sm" role="document" style="margin-top:  23vh;">
                <!--Content-->
                <div class="modal-content">
        
                    <div class="modal-body text-center mb-1">
                        <form id="new_sub_category_data" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <h5 style="text-align:left;" class="mb-2">Name</h5>
                        <div class="md-form ml-0 mr-0">
                            <input required type="text" name="category_name" class="form-control form-control-sm validate ml-0">
                        </div>
                        <h5 style="text-align:left;" class="mb-2">Image <small>Only png</small></h5>
                        <div class="md-form ml-0 mr-0">
                                <input required type="file" accept="image/x-png" name="category_image" class="form-control form-control-sm validate ml-0">
                            </div>
                        <br>
                        <div class="text-center mt-4">
                            <button type="submit" class="btn btn-cyan mt-1 btn-xs" style = "    color: #fff;background-color: #9124a3;border-color: #701c7e;"id="sub_category_submit">Save </button>
                        </div>
                        </form>
                    </div>
        
                </div>
                <!--/.Content-->
            </div>
        </div>
        <div class="modal fade" id="sub_category_edit_form_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog cascading-modal modal-avatar modal-sm" role="document" style="margin-top:  23vh;">
                    <!--Content-->
                    <div class="modal-content">
                            <div class="modal-body text-center mb-1">
                            <form id="edit_sub_category_data" enctype="multipart/form-data" method="post">
                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            <h5 style="text-align:left;" class="mb-2">Name</h5>
                            <div class="md-form ml-0 mr-0">
                                <input required type="text" id="category_name_field" name="category_name" class="form-control form-control-sm validate ml-0">
                            </div>
                            <h5 style="text-align:left;" class="mb-2">Image <small>Only png</small></h5>
                            <div class="md-form ml-0 mr-0">
                                    <input  type="file" accept="image/x-png" name="category_image" class="form-control form-control-sm validate ml-0">
                                </div>
                            <br>
                            <div class="text-center mt-4">
                                <button type="submit" class="btn btn-cyan mt-1 btn-xs" style = "    color: #fff;background-color: #9124a3;border-color: #701c7e;"id="sub_category_submit">Save </button>
                            </div>
                            </form>
                        </div>
            
                    </div>
                    <!--/.Content-->
                </div>
            </div>
            <div class="modal fade" id="sub_category_form_model_for_empty" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog cascading-modal modal-avatar modal-sm" role="document" style="margin-top:  23vh;">
                        <!--Content-->
                        <div class="modal-content">
                
                            <div class="modal-body text-center mb-1">
                                <form id="new_sub_category_data_for_empty" enctype="multipart/form-data" method="post">
                                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                <h5 style="text-align:left;" class="mb-2">Name</h5>
                                <div class="md-form ml-0 mr-0">
                                    <input required type="text" name="category_name" class="form-control form-control-sm validate ml-0">
                                </div>
                                <h5 style="text-align:left;" class="mb-2">Image <small>Only png</small></h5>
                                <div class="md-form ml-0 mr-0">
                                        <input required type="file" accept="image/x-png" name="category_image" class="form-control form-control-sm validate ml-0">
                                    </div>
                                <br>
                                <div class="text-center mt-4">
                                    <button type="submit" class="btn btn-cyan mt-1 btn-xs" style = "    color: #fff;background-color: #9124a3;border-color: #701c7e;"id="sub_category_submit">Save </button>
                                </div>
                                </form>
                            </div>
                
                        </div>
                        <!--/.Content-->
                    </div>
                </div>
@endsection
@section('script')
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script src="{{asset('number_masking/js/intlTelInput.js')}}"></script>
<script src="{{asset('choosen/chosen.jquery.js')}}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
jQuery(document).ready(function(){
    jQuery(".chosen").data("placeholder","Select Frameworks...").chosen();
});
</script>
<script>
    
   
    $('#youtube_url').keyup(function(){
       var video_id =  "https://www.youtube.com/embed/"+youtube_parser($('#youtube_url').val())

       $('#youtube_viewer').attr('src',video_id);
    });
    function youtube_parser(url){
        var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        var match = url.match(regExp);
        return (match&&match[7].length==11)? match[7] : false;
    }
var easyTree ;
$(document).ready(function(){
    load_category(0);
    //$('#category_model').modal('show');
});
$('#category_btn').on('click', function() {
    $('#category_model').modal('show');//
});
function load_category(parent_id){
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: "{{route('ajaxGetCategory')}}",
        type: 'POST',
        data:{
            _token:_token,parent_id:parent_id
        },
        success: function (data) {
            $("#category_list").empty();
            data.forEach(function(v, i, a){
                    $("#category_list").append("<li class='glyphicon glyphicon-chevron-right list-group-item ulli ui-state-default' id='list"+v.id+"' value='"+v.id+"'><a href='/exam/"+v.id+"/"+v.question+"'>"+v.name.toUpperCase()+" <img id='category_icon"+v.id+"' class='imgimg-responsive' style='width:20px;height:20px;' src='' style='float:left;' id='img3' /></a></li>");
            
                    $.ajax({
                    url:'/category/sub_category/sub_category_'+v.id+'.png',
                    type:'HEAD',
                    success: function(){
                        $('#category_icon'+v.id).attr("src",'/category/sub_category/sub_category_'+v.id+'.png');
                    }
                    })
                    
            })
            
          
         }
})}
var parent_id=0;
var category_id= 0;
function load_category_after(parent_id){
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url: "{{route('ajaxGetCategory')}}",
        type: 'POST',
        data:{
            _token:_token,parent_id:parent_id
        },
        success: function (data) {
             data.forEach(function(v, i, a){
                   
                    $("#list"+parent_id).append("<li class='list-group-item ulli ' id='list"+v.id+"' value='"+v.id+"'><a href='/add-training/"+v.id+"/"+v.question+"'>"+v.name.toUpperCase()+" <img id='category_icon"+v.id+"' class='imgimg-responsive' style='width:20px;height:20px;' src='' style='float:left;' id='img3' /></a></li>");
                    $.ajax({
                    url:'/category/sub_category/sub_category_'+v.id+'.png',
                    type:'HEAD',
                    success: function(){
                        $('#category_icon'+v.id).attr("src",'/category/sub_category/sub_category_'+v.id+'.png');
                    }
                    })    
            })
        }
})}
/* $('body').on('mouseenter', '.ulli', function() {
    
   $(this).prepend('<span style="float:right;"><span class="glyphicon glyphicon-plus add_sub_category_btn"></span><span   class="glyphicon glyphicon-pencil sub_category_control_edit"></span> <span class="glyphicon glyphicon-trash sub_category_control_delete"></span></span>');
}); */
/* $('body').on('mouseleave', '.ulli', function() {
   $(this).find("span").remove();
}); */
$('body').on('click', '.ulli', function(e) {

    if(e.target !== e.currentTarget) return;

    if ( $("#list"+this.value).children().length > 2 ) {
        $("#list"+this.value).children().remove()
        load_category(0);
        return;
    }
    load_category_after(this.value);
});

$('#add_category_btn').on('click', function() {
    $('#category_form_model').modal('show');
});
$("#new_category_data").on('submit',(function(e) {
        var formData = new FormData(this)
        var _token = $('input[name="_token"]').val();
        var category_image = $('input[name="category_image"]').val();
        var category_name = $('input[name="category_name"]').val();
        formData.append("_token", _token);
        formData.append("parent", parent_id);
    
    
        $.ajax({
        url: "{{route('ajaxAddCategory')}}",
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        
        data: formData,
        success: function (data) {
            if(data == "already_created"){
                swal("Sorry!", "This Category Name already exist!", "error");   
            }
            else{
            swal("Good job!", "Category created successfully!", "success");
                load_category(0);
            }
            }
        });
    e.preventDefault();
 
}));
$('body').on('click', '.add_sub_category_btn', function() { 
    //category_id = this.value;
    parent_id = $(this).parent().parent().attr('value');
    $('#category_form_model').modal('show');
    console.log(parent_id);
});
$('body').on('click', '.sub_category_control_edit', function() { 
    category_id = $(this).parent().parent().attr('value');
    $('#sub_category_edit_form_model').modal('show');
    var value = $(this).parent().parent().text();//
    $('#category_name_field').val(value); 
});
$("#edit_sub_category_data").on('submit',(function(e) {
        var formData = new FormData(this)
        formData.append("category_id",category_id);
        var _token = $('input[name="_token"]').val();
        var category_image = $('input[name="category_image"]').val();
        var category_name = $('input[name="category_name"]').val();
    formData.append("_token", _token);
    
        $.ajax({
        url: "{{route('ajaxSubCategoryEdit')}}",
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        success: function (data) {
            if(data == "already_created"){
                swal("Sorry!", "This Category Name already exist!", "error");   
            }else{ 
            swal("Good Job!", "Sub Category edit successfully!", "success");
               load_category(0);
            }
            }
           
        });
    e.preventDefault();
 
}));
$('body').on('click', '.sub_category_control_delete', function() { 
    var id = $(this).parent().parent().attr('value');
    swal({
    title: "Are you sure?",
    icon: "warning",
    buttons: true,
    dangerMode: true,
    })
    .then((willDelete) => {
    if (willDelete) {
        
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url: "{{route('ajaxSubCategoryDelete')}}",
            type: 'POST',
            data: {
                _token:_token,id:id
            },
            success: function (data) {
            if(data == "can_not_delete"){
                swal("Sorry!", "Sorry This category can not delete!", "error"); 
            }else{
                swal("Good job!", "Sub Category Deleted successfully!", "success");
                
            }
            load_category(0);
            }
            
            });
        swal("Poof! Sub Category has been deleted!", {
        icon: "success",
        });
    } else {
        swal("Your sub category is safe now!");
    }
    });
    
});


var loc = window.location.href,
index = loc.indexOf('#');

if (index > 0) {
window.location = loc.substring(0, index);
}
$(document).ready(function(){
    ajaxGetUserlanguage()
});
function ajaxGetUserlanguage(){
    $("#languag_list").empty()
    var _token = $('input[name="_token"]').val();
    $.ajax({
           url: "{{route('ajaxGetUserlanguage')}}",
           type: 'POST',
           data: {
               _token : _token
           },
           success: function (data) {
            
                console.log(data)
                data.forEach(function(v, i, a){
                    $("#languag_list").append("<li>"+v.language.toUpperCase()+"</li>");
                    
                })
                
                
            }
    });
}
$('body').on('click', '#save_language', function() { 
    
    
    var language = $('#save_value_language').val();
    var _token = $('input[name="_token"]').val();
       $.ajax({
           url: "{{route('ajaxSaveUserlanguage')}}",
           type: 'POST',
           data: {
               _token : _token,id:language
           },
           success: function (data) {
            ajaxGetUserlanguage()
            $('#select_language_model').modal('hide');
        }
    });
});

$('body').on('mouseenter', '.child_e', function() {
    console.log(this.id);
    $(this).append("<span   class='glyphicon glyphicon-trash trash_e'></span>")
});

    $('body').on('mouseleave', '.child_e', function() {
    console.log(this.id);
    $(this).find("span").remove();
});
$('body').on('click', '.child_e', function() {

    console.log(this.id);
    var element =this;
    var _token = $('input[name="_token"]').val();
       $.ajax({
           url: "{{route('ajaxRemovelanguage')}}",
           type: 'POST',
           data: {
               _token : _token,id:this.id
           },
           success: function (data) {
            $(element).remove();
                //$(element).parent().remove();
                localStorage.setItem('popupFlag', 1);
                window.location.reload();
            }
    });
});

$(document).ready(function(){
    if(localStorage.getItem('popupFlag') == 1)
    {
        $('#language_model_box').modal('show');    // Do something after 1 second 
        localStorage.removeItem('popupFlag')
    }
});

$("#language_form").submit(function(e) {
    var _token = $('input[name="_token"]').val();
       $.ajax({
           url: "{{route('ajaxAddlanguage')}}",
           type: 'POST',
           data: {
               _token : _token,language:$('#settings_language_input').val()
           },
           success: function (data) {
            
               console.log();
           if(data != "already_created"){
                console.log($("#language_cantainer:last-child div:last-child div").length)
              if($("#language_cantainer:last-child div:last-child div").length<6){ 
                $("#language_cantainer:last-child div:last-child div:last-child").after('<div class="col-sm-2 child_e" id='+data.id+'>'+data.language+'</div>')
              
            }else{
                $("#language_cantainer:last-child").append('<div class="row"><div class="col-sm-2 child_e" id='+data.id+'>'+data.language+'</div></div>')
            }
            $('#settings_language_input').empty()
           }
             
           }
    });
    e.preventDefault();
});
$('#settings').on('click', function() {
    $('#settings_model').modal('show');
});
$('#language_btn').on('click', function() {
    $('#settings_model').modal('hide');
    
},function(){
    
    $('#language_model_box').modal('show');
});
        
$("#hour_rate_edit_box").hover(function(){
    $('#hour_rate_edit').show();
},function(){
    $('#hour_rate_edit').hide();
});
$('body').on('click', '#hour_rate_edit', function() {

    $("#hour_rate_edit_box").empty();
    $("#hour_rate_edit_box").append(`
        <input type="text" id="input_hr" placeholder="HR-USD">
        <button type="button" id="saave_input_hr" class="btn">save</button>
    `);
    $('#input_hr').focus();
});
$('body').on('click', '#saave_input_hr', function() {
    var _token = $('input[name="_token"]').val();
       $.ajax({
           url: "{{route('ajaxUpdateHoursRate')}}",
           type: 'POST',
           data: {
               _token : _token,hr:$('#input_hr').val()
           },
           success: function (data) {
            $("#hour_rate_edit_box").empty();
            $("#hour_rate_edit_box").append(`
                <h5 id="value_hr">$`+data+` USD/hr <span id="hour_rate_edit" style="display:none" class="glyphicon glyphicon-pencil"></span></h5><h5>Hoursly Rate </h5>
            `);
            console.log(data)    
           }
       });
})
$("#language_value_box").hover(function(){
    $('#language_value_icon').show();
},function(){
    $('#language_value_icon').hide();
});

$('body').on('click', '#language_value_icon', function() {
    $('#select_language_model').modal('show');
});
   
$('body').on('click', '#saave_input_language', function() {
    var _token = $('input[name="_token"]').val();
       $.ajax({
           url: "{{route('ajaxUpdatelanguage')}}",
           type: 'POST',
           data: {
               _token : _token,language:$('#input_language').val()
           },
           success: function (data) {
            $("#language_value_box").empty();
            $("#language_value_box").append(`
           
                Language : <span>`+data+`</span> <span id="language_value_icon" style="display:none" class="glyphicon glyphicon-pencil">
            `);
            console.log(data)    
           }
       });
})
$("#website_box").hover(function(){
    $('#website_icon').show();
},function(){
    $('#website_icon').hide();
});
$('body').on('click', '#website_icon', function() {
//$('#hour_rate_edit').click(function() {
    $("#website_box").empty();
    $("#website_box").append(`
        <input type="text" id="input_website" placeholder="Website">
        <button type="button" id="saave_input_website" class="btn">save</button>
    `);
    $('#saave_input_website').focus();
});
$('body').on('click', '#saave_input_website', function() {
    var _token = $('input[name="_token"]').val();
       $.ajax({
           url: "{{route('ajaxUpdateWebsite')}}",
           type: 'POST',
           data: {
               _token : _token,website:$('#input_website').val()
           },
           success: function (data) {
            $("#website_box").empty();
            $("#website_box").append(`
            Website : <a href="https://`+data+`"> `+data+` </a><span id="website_icon" style="display:none" class="glyphicon glyphicon-pencil">
            `);
            console.log(data)    
           }
       });
})
$("#paragraph_title_box").hover(function(){
    $('#paragraph_title_icon').show();
},function(){
    $('#paragraph_title_icon').hide();
});
$('body').on('click', '#paragraph_title_icon', function() {
//$('#hour_rate_edit').click(function() {
    $("#paragraph_title_box").empty();
    $("#paragraph_title_box").append(`
        <input type="text" id="input_input_title" placeholder="Title">
        <button type="button" id="saave_input_btn" class="btn">save</button>
    `);
    $('#input_input_title').focus();
});
$('body').on('click', '#saave_input_btn', function() {
    var _token = $('input[name="_token"]').val();
       $.ajax({
           url: "{{route('ajaxUpdateParagraphTitle')}}",
           type: 'POST',
           data: {
               _token : _token,paragraph_title:$('#input_input_title').val()
           },
           success: function (data) {
            $("#paragraph_title_box").empty();
            $("#paragraph_title_box").append(`
                <h3  >`+data+` <span id="paragraph_title_icon" style="display:none" class="glyphicon glyphicon-pencil"></h3>    
            `);
            console.log(data)    
           }
       });
})
$("#paragraph_box").hover(function(){
    $('#paragraph_icon').show();
},function(){
    $('#paragraph_icon').hide();
});
$('body').on('click', '#paragraph_icon', function() {
//$('#hour_rate_edit').click(function() {
    $("#paragraph_box").empty();
    $("#paragraph_box").append(`
        <textarea type="text" class="form-control" rows="5" id="input_paragraph" placeholder="Title"></textarea>
        <button type="button" id="input_paragraph_btn" class="btn">save</button>
    `);
    $('#input_paragraph').focus();
});
$('body').on('click', '#input_paragraph_btn', function() {
    var _token = $('input[name="_token"]').val();
       $.ajax({
           url: "{{route('ajaxUpdateParagraph')}}",
           type: 'POST',
           data: {
               _token : _token,paragraph_title:$('#input_paragraph').val()
           },
           success: function (data) {
            $("#paragraph_box").empty();
            $("#paragraph_box").append(`
                <p>`+data+` <span id="paragraph_icon" style="display:none" class="glyphicon glyphicon-pencil"></span></p>
            `);
            console.log(data)    
           }
       });
})
//
    $("#phone").intlTelInput({
 
   separateDialCode: true,
  utilsScript: "{{asset('number_masking/js/utils.js')}}"
});

$(document).ready(function() {
    $('#summernote').html('<?php echo $user->note;  ?>');
    $('#summernote').summernote({
            placeholder: 'Type Here',
            tabsize: 4,
            height: 100
        });
        $('#videofile').bind('change', function() {

            if(this.files[0].size>5120){
                
                $("#video_file_error").text("upload only 5 mb file.");
                $("#video_file_error").css("color", "red");
                return false;
            }else{
                $("#video_file_error").css("display", "none");
            }
            
        });
        
        
        
});

    $(function() {// 
        $('#view_video_btn').on('click', function() {
            $('#vide_container').empty();
            $('#vide_container').append(`
            @if($user->video=="file")
            <video style="width:100%" controls>
            <source src='{{asset("video/$user->id.mp4")}}' type="video/mp4">
            <source src='{{asset("video/$user->id.mp4")}}' type="video/ogg">
            Your browser does not support HTML5 video.
            </video>
            @elseif(!$user->video) 
            @else
            <?php 
            $url = $user->video;
            // $url = "http://www.youtube.com/watch?v=7zWKm-LZWm4&feature=relate";
                parse_str( parse_url( $url, PHP_URL_QUERY ), $url_vars );
                //echo $url_vars['v'];  
            ?>
            <iframe style="width:100%;height:350px;"
            src="https://www.youtube.com/embed/{{$url_vars['v']}}?autoplay=1">
            </iframe>
            @endif
            `);
        });
        $('#profile').on('click', function() {
            $('#profilechange').modal('show');
        });
        $('#coverphoto').on('click', function() {
            $('#coverphotomodel').modal('show');
        });
        $('#pdf-btn').on('click', function() {
            $('#pdf_model').modal('show');
        });
        $('#profile_status').on('click', function() {
            $('#profile_status_model').modal('show');
        });
        $('#powertable').on('click', function() {
            $('#profile_status_model').modal('hide');
            $('#powertable_model').modal('show');
        });
        
        $('#video_btn').on('click', function() {
            $('#video_model').modal('show');
        });
        $('#updatepolicy').on('click', function() {
            $('#policy_model').modal('show');
        });
        $('#powertable').on('click', function() {
            $('#profileverification_model').modal('hide');
            $('#powertable_model').modal('show');
        });
        $('#power_table_in_pd').on('click', function() {
            $('#profile_status_model').modal('hide');
            $('#powertable_model').modal('show');
        });
        $('#profileverification').on('click', function() {
            $('#profileverification_model').modal('show');
        });
        $('#edit-page').on('click', function() {
            window.location.href = '{{route("update.profile")}}';
        });
        $('#edit_map').on('click', function() {
            $('#edit_map_model').modal('show');
        });
        
    });
    $('#freevisible').on('click',function() {
        var _token = $('input[name="_token"]').val();
       $.ajax({
           url: "{{route('changestatustofree')}}",
           type: 'POST',
           data: {
               _token : _token,
               id:<?php echo Auth::user()->id ?>,
               status:1
           },
           success: function (data) {
               if(data=="ok"){
                window.location.assign(window.location.href)
               }
            
           }
       });
    });
    $('#busyvisible').on('click',function() {
        var _token = $('input[name="_token"]').val();
       $.ajax({
        url: "{{route('changestatustofree')}}",
           type: 'POST',
           data: {
               _token : _token,
               id:<?php echo Auth::user()->id ?>,
               status:2
           },
           success: function (data) {
               if(data=="ok"){
                window.location.assign(window.location.href)
               }
            
           }
       });
      
   });
    $('#invisiblevisible').on('click',function() {
        var _token = $('input[name="_token"]').val();
       $.ajax({
        url: "{{route('changestatustofree')}}",
           type: 'POST',
           data: {
               _token : _token,
               id:<?php echo Auth::user()->id ?>,
               status:3
           },
           success: function (data) {
               if(data=="ok"){
                window.location.assign(window.location.href)
               }
            
           }
       });
      
   });
    
   
</script>
        <script>
             $(function() {
        $('#profile').on('click', function() {
            $('#profilechange').modal('show');
        });
        $('#coverphoto').on('click', function() {
            $('#coverphotomodel').modal('show');
        });
        $('#edit-page').on('click', function() {
            window.location.href = '{{route("update.profile")}}';
        });
    });
    $('#freevisible').on('click',function() {
        var _token = $('input[name="_token"]').val();
       $.ajax({
           url: '{{route("changestatustofree")}}',
           type: 'POST',
           data: {
               _token : _token,
               id:<?php echo Auth::user()->id ?>,
               status:1
           },
           success: function (data) {
               if(data=="ok"){
                window.location.assign(window.location.href)
               }
            
           }
       });
      
      });
    $('#busyvisible').on('click',function() {
        var _token = $('input[name="_token"]').val();
       $.ajax({
        url: '{{route("changestatustofree")}}',
           type: 'POST',
           data: {
               _token : _token,
               id:<?php echo Auth::user()->id ?>,
               status:2
           },
           success: function (data) {
               if(data=="ok"){
                window.location.assign(window.location.href)
               }
            
           }
       });
  });
    $('#invisiblevisible').on('click',function() {
        var _token = $('input[name="_token"]').val();
       $.ajax({
        url: '{{route("changestatustofree")}}',
           type: 'POST',
           data: {
               _token : _token,
               id:<?php echo Auth::user()->id ?>,
               status:3
           },
           success: function (data) {
               if(data=="ok"){
                window.location.assign(window.location.href)
               }
            
           }
       });
      
    });

             var o = <?php echo Auth::user()->visiblity ?>;
    $(document).ready(function() {
        if(o==1){
            $('#status-icon').css('background', 'green')
            $('#dp_status-icon').css('background', 'green')
            
        }
        if(o==2){
            $('#status-icon').css('background', 'red')
            $('#dp_status-icon').css('background', 'red')
        }
        if(o==3){
            $('#status-icon').css('display', 'none')
            $('#dp_status-icon').css('background', 'none')
        }
        
    });
    $('#menu_link').click(function() {
    $('#menu_box').toggle();
    });
                </script>
         <script>
            $("#profile-edit-icon").hover(function(){
                $('#profile').css('display','block')
            }, function(){
                $('#profile').css('display','none')
            })
            $("#cover-photo").hover(function(){
                $('#coverphoto').css('display','block')
            }, function(){
                
              //  $('#coverphoto').css('display','none')
            })
            $("#cover-photo").mouseleave(function(){
               // $('#coverphoto').css('display','none')
            })
            
        </script>
        <script>
            $(document).ready(function() {
                $("#dropdownlist").empty()
               $('#searchbox').keyup(function(e) {
                $("#dropdownlist").empty()
                var _token = $('input[name="_token"]').val();
                
               input = $(this).val();
               
               if(input.length>=2){
                $.ajax({
           url: '{{route("searching")}}',
           type: 'POST',
           data: {
               _token : _token,
                data:input
           },
           success: function (data) {
           
            if(data){
                
                data.forEach(function(item) {
                    $("#dropdownlist").css("display", "block");
                    $("#dropdownlist").append('<li class="list-group-item" style="height: 47px !important;"><img class="imgimg-responsive"  style="width:50px;height:30px;float:left" id="searchbox_icon'+item['id']+'" src="/demo/man01.png"><a style="padding:0;" href="profile/'+item["id"]+'">'+item['name']+'</a><i   style="margin-right: 12px;float:right;color:green; background: #02ffe1;border: 1px solid rgba(0, 0, 0, .1);border-radius: 50%;box-sizing: border-box;height: 12px;width: 12px;" class="glyphicon glyphicon-football"><sub>.</sub></i></li>');
                    
                    $.ajax({
                    url:'profileimages/images/img_'+item['id']+'.png',
                    type:'HEAD',
                    error: function(){
                        $('#searchbox_icon'+item['id']).attr("src", "{{asset('/profile/nocover.png')}}");
                    },
                    success: function(){
                        $('#searchbox_icon'+item['id']).attr("src",'/profileimages/images/img_'+item['id']+'.png');

                    }})

                });
            }
            }
                    });
               }
         

              }); });
        </script>
        <script>
        $(document).ready(function(){
    $(".left-first-section").click(function(){
        $('.main-section').toggleClass("open-more");
    });
});
$(document).ready(function(){
    $(".fa-minus").click(function(){
        $('.main-section').toggleClass("open-more");
    });
});
$(document).ready(function(){
    //data.namewindow.setInterval(function(){
        var _token = $('input[name="_token"]').val();
        $.ajax({
           url: '{{route("getcontactlist")}}',
           type: 'POST',
           data: {
               _token : _token,
               id:<?php echo Auth::user()->id ?>
           },
           success: function (data) {
            $("#contact_list").empty();
              
              data.forEach(function(item) {
                  $("#contact_list").append('<li><div class="left-chat"><img class="imgimg-responsive"  id="contactbox_icon'+item.my_contact_id+'" src="/demo/man01.png"><p><a href="/profile/'+item.my_contact_id+'">'+item.name+'</a> <i style="float:right;"><a href="/removecontact/'+item.my_contact_id+'">x</a> </i><i   style="margin-right: 12px;float:right;color:green; background: #02ffe1;border: 1px solid rgba(0, 0, 0, .1);border-radius: 50%;box-sizing: border-box;height: 12px;width: 12px;" class="glyphicon glyphicon-football"><sub>.</sub></i></p></div></li>')
                  
                  $.ajax({
                    url:'profileimages/images/img_'+item.my_contact_id+'.png',
                    type:'HEAD',
                    error: function(){
                        $('#contactbox_icon'+item.my_contact_id).attr("src", "{{asset('/profile/nocover.png')}}");
                    },
                    success: function(){
                        $('#contactbox_icon'+item.my_contact_id).attr("src",'/profileimages/images/img_'+item.my_contact_id+'.png');

                    }})


            });
           }
    });
   // },1000);
});
$(document).ready(function(){
    $("#btn-contacts").click(function(){
        $('#box_lable').text("Contact List")
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url: '{{route("getcontactlist")}}',
           type: 'POST',
           data: {
               _token : _token,
               id:<?php echo Auth::user()->id ?>
           },
           success: function (data) {
            $("#contact_list").empty();
              
              data.forEach(function(item) {
                  $("#contact_list").append('<li><div class="left-chat"><img class="imgimg-responsive"  id="contactbox_icon'+item.my_contact_id+'" src="/demo/man01.png"><p><a href="/profile/'+item.my_contact_id+'">'+item.name+'</a> <i style="float:right;"><a href="/removecontact/'+item.my_contact_id+'">x</a> </i><i   style="margin-right: 12px;float:right;color:green; background: #02ffe1;border: 1px solid rgba(0, 0, 0, .1);border-radius: 50%;box-sizing: border-box;height: 12px;width: 12px;" class="glyphicon glyphicon-football"><sub>.</sub></i></p></div></li>')
                  
                  $.ajax({
                    url:'/profileimages/images/img_'+item.my_contact_id+'.png',
                    type:'HEAD',
                    error: function(){
                        $('#contactbox_icon'+item.my_contact_id).attr("src", "{{asset('/profile/nocover.png')}}");
                    },
                    success: function(){
                        $('#contactbox_icon'+item.my_contact_id).attr("src",'/profileimages/images/img_'+item.my_contact_id+'.png');

                    }})


            });
           }
    });
           
           });
    });
    $("#btn-blocks").click(function(){
        $('#box_lable').text("Block List")
        var _token = $('input[name="_token"]').val();
        $.ajax({
           url: '{{route("getblockcontactlist")}}',
           type: 'POST',
           data: {
               _token : _token,
               id:<?php echo Auth::user()->id ?>
           },
           success: function (data) {
            $("#contact_list").empty();
              
              data.forEach(function(item) {
                  $("#contact_list").append('<li><div class="left-chat"><img class="imgimg-responsive"  id="contactbox_icon'+item.my_contact_id+'" src="/demo/man01.png"><p><a href="/profile/'+item.my_contact_id+'">'+item.name+'</a> <i style="float:right;"><a href="/removecontact/'+item.my_contact_id+'">x</a> </i><i   style="margin-right: 12px;float:right;color:green; background: #02ffe1;border: 1px solid rgba(0, 0, 0, .1);border-radius: 50%;box-sizing: border-box;height: 12px;width: 12px;" class="glyphicon glyphicon-football"><sub>.</sub></i></p></div></li>')
                  
                  $.ajax({
                    url:'/profileimages/images/img_'+item.my_contact_id+'.png',
                    type:'HEAD',
                    error: function(){
                        $('#contactbox_icon'+item.my_contact_id).attr("src", "{{asset('/profile/nocover.png')}}");
                    },
                    success: function(){
                        $('#contactbox_icon'+item.my_contact_id).attr("src",'/profileimages/images/img_'+item.my_contact_id+'.png');

                    }})


            });
           }
    });
            });
/*     });
}); */
        </script>
        <script>
var place ;
var lat;
var lng;
function initMap() {
   
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {
            /* lat: 30.157458, 
            lng: 71.52491540000005 */
            @if(Auth::user()->lat)
            lat: {{Auth::user()->lat}}, 
            lng: {{Auth::user()->lng}}
            @else
            lat: 30.157458, 
            lng: 71.52491540000005
            @endif
        },
        zoom: 15
    });
    var image = 'https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi-dotless2.png';
    var beachMarker = new google.maps.Marker({
        @if(Auth::user()->lat)
        position: {lat: {{Auth::user()->lat}}, lng: {{Auth::user()->lng}}},
            @else
            position: {lat: 30.157458, lng: 71.52491540000005},
            @endif
      
      map: map,
      icon: image
    });
    var card = document.getElementById('pac-card');
    var input = document.getElementById('pac-input');
    var types = document.getElementById('type-selector');
    var strictBounds = document.getElementById('strict-bounds-selector');

    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

    var autocomplete = new google.maps.places.Autocomplete(input);

    
    autocomplete.bindTo('bounds', map);

    var infowindow = new google.maps.InfoWindow();
    var infowindowContent = document.getElementById('infowindow-content');
    infowindow.setContent(infowindowContent);
    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        place = autocomplete.getPlace();
        console.log(place.geometry.location.lat(),place.geometry.location.lng());
        lat = place.geometry.location.lat();
        lng = place.geometry.location.lng();
        if (!place.geometry) {
           window.alert("No details available for input: '" + place.name + "'");
            return;
        }

        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17); // Why 17? Because it looks good.
        }
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        infowindowContent.children['place-icon'].src = place.icon;
        infowindowContent.children['place-name'].textContent = place.name;
        infowindowContent.children['place-address'].textContent = address;
        infowindow.open(map, marker);
    });

   function setupClickListener(id, types) {
        var radioButton = document.getElementById(id);
        radioButton.addEventListener('click', function() {
            autocomplete.setTypes(types);
        });
    }

    setupClickListener('changetype-all', []);
    setupClickListener('changetype-address', ['address']);
    setupClickListener('changetype-establishment', ['establishment']);
    setupClickListener('changetype-geocode', ['geocode']);

    document.getElementById('use-strict-bounds')
        .addEventListener('click', function() {
            console.log('Checkbox clicked! New state=' + this.checked);
            autocomplete.setOptions({
                strictBounds: this.checked
            });
        });
}
function DoSubmit(){
    document.propetyform.lng.value = place.geometry.location.lng();
    document.propetyform.lat.value = place.geometry.location.lat();
    return true;
}


$("#edit_map_submit").click(function(){
    console.log(lat,lng)
    var _token = $('input[name="_token"]').val();
    $.ajax({
           url: '{{route("savelatlng")}}', 
           type: 'POST',
           data: {
               _token : _token,
               lat:lat,
               lng:lng
            },
           success: function (data) {
               if(data=="ok"){
                window.location.assign(window.location.href)
               }
           }
       });
})
$(function() {
    var _token = $('input[name="_token"]').val();
    $.ajax({
           url: '{{route("getlatlng")}}',
           type: 'POST',
           data: {
               _token : _token,
            },
           success: function (data) {
              lat = data.lat;
              lng = data.lng;
           }
       });
})
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDqaB1GlRDNUy9CJ29SzwedZ8Ew11OIiXA&libraries=places&callback=initMap" async defer></script>

<!--  <script type="text/javascript"
    src="http://maps.googleapis.com/maps/api/js?libraries=geometry&sensor=false&key=AIzaSyDqaB1GlRDNUy9CJ29SzwedZ8Ew11OIiXA&callback=initMap">
</script> -->
<script>
var x;

function getLocation() {
if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
} else { 
    x.innerHTML = "Geolocation is not supported by this browser.";
}
}

function showPosition(position) {

console.log(position.coords.latitude,position.coords.longitude);
var _token = $('input[name="_token"]').val();
    $.ajax({
           url: '{{route("setloginlocation")}}',
           type: 'POST',
           data: {
               _token : _token,
               lat:position.coords.latitude,
               lng:position.coords.longitude
            },
           success: function (data) {
             console.log(data)
           }
       });

}
getLocation()
</script>
<script>
    $.ajax({
url:'{{asset("/profileimages/cover/img_$id.png")}}',
type:'HEAD',
error: function(){
   //inner ajax
   $.ajax({
    url:'{{asset("/profileimages/cover/img_$id.jpg")}}',
    type:'HEAD',
    error: function(){
        //inner ajax
        $.ajax({
            url:'{{asset("/profileimages/cover/img_$id.gif")}}',
            type:'HEAD',
            error: function(){
               //inner ajax
                $.ajax({
                    url:'{{asset("/profileimages/cover/img_$id.jpeg")}}',
                    type:'HEAD',
                    error: function(){
                        $("#cover-photo").attr("src", "{{asset('/profile/nocover.png')}}");
                    },
                    success: function(){
                        $("#cover-photo").attr("src", '{{asset("/profileimages/cover/img_$id.jpeg")}}');
                    }})
            },
            success: function(){
                $("#cover-photo").attr("src", '{{asset("/profileimages/cover/img_$id.gif")}}');
            }})
    },
    success: function(){
        $("#cover-photo").attr("src", '{{asset("/profileimages/cover/img_$id.jpg")}}');
    }})
},
success: function(){
$("#cover-photo").attr("src", '{{asset("/profileimages/cover/img_$id.png")}}');
}})
</script>
<script>
    $.ajax({
        url:'{{asset("/profileimages/images/img_$id.png")}}',
        type:'HEAD',
        error: function(){
            //inner ajax
            $.ajax({
            url:'{{asset("/profileimages/images/img_$id.jpg")}}',
            type:'HEAD',
            error: function(){
               //inner ajax
                $.ajax({
                url:'{{asset("/profileimages/images/img_$id.jpeg")}}',
                type:'HEAD',
                error: function(){
                    //inner ajax
                    $.ajax({
                    url:'{{asset("/profileimages/images/img_$id.gif")}}',
                    type:'HEAD',
                    error: function(){
                        $("#menu_link").attr("src", "{{asset('/profile/nodp.png')}}");
                        $("#profilephoto").attr("src", "{{asset('/profile/nodp.png')}}");
                    },
                    success: function(){
                        $("#menu_link").attr("src", '{{asset("/profileimages/images/img_$id.gif")}}');
                        $("#profilephoto").attr("src", '{{asset("/profileimages/images/img_$id.gif")}}');
                    }})
                },
                success: function(){
                    $("#menu_link").attr("src", '{{asset("/profileimages/images/img_$id.jpeg")}}');
                    $("#profilephoto").attr("src", '{{asset("/profileimages/images/img_$id.jpeg")}}');
                }})
            },
            success: function(){
                $("#menu_link").attr("src", '{{asset("/profileimages/images/img_$id.jpg")}}');
                $("#profilephoto").attr("src", '{{asset("/profileimages/images/img_$id.jpg")}}');
            }})
        },
        success: function(){
            $("#menu_link").attr("src", '{{asset("/profileimages/images/img_$id.png")}}');
            $("#profilephoto").attr("src", '{{asset("/profileimages/images/img_$id.png")}}');
        }})

        function video_verify(){
    alert($('#videofile').files[0].size);
    
}
</script>
@endsection

